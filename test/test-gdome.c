#include <stdio.h>
#include <gtk/gtk.h>
#include <parser.h>
#include <tree.h>
#include <gdome.h>
#include <gdome-xml.h>
#include <gdome-util.h>

void
print_gdoc_structure (GdomeNode *node)
{
  int type;
  GdomeNode *child, *next;
  GdomeDOMString *tagName;
  GdomeException exc = 0;

  type = gdome_n_nodeType (node, &exc);
  if (type == GDOME_TEXT_NODE)
    {
      GdomeDOMString *text;

      text = gdome_cd_data ((GdomeCharacterData *)node, &exc);
      fputs (text->str, stdout);
      gdome_str_unref (text);

      return;
    }
  tagName = NULL;
  if (type == GDOME_ELEMENT_NODE)
    {
      GdomeNamedNodeMap *attrs;
      int i, n_attrs;
      putchar ('<');
      tagName = gdome_el_tagName ((GdomeElement *)node, &exc);
      fputs (tagName->str, stdout);

      attrs = gdome_n_attributes (node, &exc);
      n_attrs = gdome_nnm_length (attrs, &exc);
      for (i = 0; i < n_attrs; i++)
	{
	  GdomeAttr *attr;
	  GdomeDOMString *attrName, *attrVal;
	  attr = (GdomeAttr *)gdome_nnm_item (attrs, i, &exc);
	  attrName = gdome_a_name (attr, &exc);
	  attrVal = gdome_a_value (attr, &exc);
	  gdome_n_unref ((GdomeNode *)attr, &exc);
	  printf (" %s=\"%s\"", attrName->str, attrVal->str);
	  gdome_str_unref (attrName);
	  gdome_str_unref (attrVal);
	}
      gdome_nnm_unref (attrs, &exc);
      putchar ('>');
    }
  for (child = gdome_n_firstChild (node, &exc); child != NULL; child = next)
    {
      print_gdoc_structure (child);
      next = gdome_n_nextSibling (child, &exc);
      gdome_n_unref (child, &exc);
    }
  if (type == GDOME_ELEMENT_NODE)
    {
      fputs ("</", stdout);
      fputs (tagName->str, stdout);
      gdome_str_unref (tagName);
      putchar ('>');
    }
}

static void insert_cb (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc)
{
  printf ("got event!\n");
}


static void
gdome_test_str_unref (GdomeDOMString *self)
{
  g_free (self->str);
  g_free (self);
}

static GdomeDOMString *
gdome_test_str_mk (const char *str)
{
  GdomeDOMString *result;

  result = g_new (GdomeDOMString, 1);
  result->unref = gdome_test_str_unref;
  result->str = g_strdup (str);
  return result;
}

int
main (int argc, char **argv)
{
  xmlDocPtr doc;
  GdomeDocument *gdoc;
  GdomeElement *el;
  GdomeException exc = 0;
  GdomeNode *ref;
  GdomeElement *empty;
  GdomeEventListener *listener;

  if (argc < 2)
    {
      fprintf (stderr, "usage: test-gdome test.xml\n");
      exit (1);
    }

  doc = xmlParseFile (argv[1]);
  gdoc = gdome_xml_from_document (doc);
  el = gdome_doc_documentElement (gdoc, &exc);

  /*  listener.vtab = &gdome_test_EventListener_vtab; */
  listener = gdome_xml_evntl_new (GDOME_EVNTL_CALLBACK (insert_cb), NULL);
  gdome_evntt_addEventListener (GDOME_EVNTT (el),
				gdome_test_str_mk ("nodeInsertedIntoSubtree"),
				listener, 0, &exc);

  empty = gdome_doc_createElement (gdoc, gdome_test_str_mk ("empty"), &exc);
  ref = gdome_n_firstChild ((GdomeNode *)el, &exc);
  gdome_n_unref (gdome_n_insertBefore ((GdomeNode *)el,
				       (GdomeNode *)empty, ref, &exc), &exc);
  gdome_n_unref (ref, &exc);
  gdome_n_unref ((GdomeNode *)empty, &exc);

  gdome_doc_unref (gdoc, &exc);
  print_gdoc_structure ((GdomeNode *)el);
  gdome_el_unref (el, &exc);
  putchar ('\n');

  xmlFreeDoc (doc);
  return 0;
}


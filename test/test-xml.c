#include <stdio.h>
#include "rxml.h"
#include "rdom.h"
#include "rdom-rxml.h"

RXML_Error *
test_write (RXML_ByteWriter *self, char *buf, int size)
{
  fwrite (buf, 1, size, stdout);
  return NULL;
}

RXML_Error *
test_close (RXML_ByteWriter *self)
{
  return NULL;
}

void print_node (RDOM_Node *n);

void
print_element (RDOM_Element *el)
{
  char *name;
  RDOM_Node child, *c;
  RDOM_NamedNodeMap nnm, *map;
  int n_attrs;
  int i;
  RDOM_Attr attr, *a;
  char *attr_name, *attr_val;

  putchar ('<');
  name = RDOM_get_tagName (el);
  fputs (name, stdout);
  map = RDOM_get_attributes (el, &nnm);
  n_attrs = RDOM_nnm_get_length (map);
  for (i = 0; i < n_attrs; i++)
    {
      a = RDOM_nnm_item (map, i, &attr);
      attr_name = RDOM_attr_get_name (a);
      attr_val = RDOM_attr_get_value (a);
      printf (" %s=\"%s\"", attr_name, attr_val);
      RDOM_release_string (a, attr_name);
      RDOM_release_string (a, attr_val);
    }
  putchar ('>');
  for (c = RDOM_firstChild (el, &child); c != NULL;
       c = RDOM_nextSibling (c, &child))
    {
      print_node (c);
    }
  fputs ("</", stdout);
  fputs (name, stdout);
  RDOM_release_string (el, name);
  putchar ('>');
}

void
print_text (RDOM_Text *t)
{
  char *data;

  data = RDOM_cd_get_data (t);
  fputs (data, stdout);
  RDOM_release_string (t, data);
}

void
print_document (RDOM_Document *document)
{
  RDOM_Node child, *c;

  for (c = RDOM_firstChild (document, &child); c != NULL;
       c = RDOM_nextSibling (c, &child))
    {
      print_node (c);
    }
}

void print_node (RDOM_Node *n)
{
  switch (RDOM_get_nodeType (n))
    {
    case RDOM_ELEMENT_NODE:
      print_element (n);
      break;
    case RDOM_TEXT_NODE:
      print_text (n);
      break;
    case RDOM_DOCUMENT_NODE:
      print_document (n);
      break;
    default:
      printf ("[unknown node type %d]", RDOM_get_nodeType (n));
    }
}

void
test_listener_insert (RDOM_Listener *self, RDOM_Node *node, RDOM_Node *child,
		      RDOM_When when)
{
  printf ("insert node %x child %x %s\n",
	  node->node.id, child->node.id,
	  when == RDOM_Before ? "before" : "after");
}

void
test_listener_delete (RDOM_Listener *self, RDOM_Node *node, RDOM_When when)
{
  printf ("delete node %x %s\n",
	  node->node.id,
	  when == RDOM_Before ? "before" : "after");
}

const RDOM_Listener test_listener = {
  test_listener_insert,
  test_listener_delete
};

void
make_big_tree (RDOM_Document *doc, RDOM_Node *node, int width, int height)
{
  RDOM_Element el;
  int i;

  if (height == 0)
    {
      RDOM_createElement (doc, "leaf", &el);
      RDOM_appendChild (node, &el);
    }
  else
    {
      RDOM_createElement (doc, "tree", &el);
      RDOM_appendChild (node, &el);
      for (i = 0; i < width; i++)
	make_big_tree (doc, &el, width, height - 1);
    }
}

void
test_dom (RXML_Context *context)
{
  RDOM_Document document;
  RDOM_Element root;
  RDOM_Element child;
  RDOM_Text text;

  RDOM_create (context, &document);
#if 0
  RDOM_createElement (&document, "test", &root);
  RDOM_createTextNode (&document, "text", &text);
  RDOM_createElement (&document, "child", &child);
  RDOM_add_listener (&root, (RDOM_Listener *)&test_listener);
  RDOM_appendChild (&root, &text);
  RDOM_remove_listener (&root, (RDOM_Listener *)&test_listener);
  RDOM_appendChild (&root, &child);
#else
  make_big_tree (&document, &document, 10, 2);
#endif

  print_document (&document);
  putchar ('\n');
}

int
main (int argc, char **argv)
{
  RXML_Context *context;
  RXML_Document *document;
  RXML_ByteWriter my_bw = { test_write, test_close };
  RXML_XbitWriter *xw;
  RXML_ByteWriter *bw;
  char *str = "<?xml?><funky a='b' c='&foo;&apos;&quot;&amp;&lt;&gt;&#x3c;&#60;&#12345;\343\200\271'>chicken&quot;&#60;<blarg/></funky>";
  RDOM_Document rdoc;

  context = RXML_context_default ();

  document = RXML_new (context, RXML_Document, 1);
  document->prolog = NULL;

  document->element = RXML_new (context, RXML_Element, 1);
  document->element->tag.name = "xml";
  document->element->tag.n_attrs = 1;
  document->element->tag.attrs = RXML_new (context, RXML_Attr, 1);
  document->element->tag.attrs[0].name = "a";
  document->element->tag.attrs[0].value = ">b<";
  document->element->children.n_bits = 1;
  document->element->children.bits = RXML_new (context, RXML_ContentBit, 1);
  document->element->children.bits[0].type = RXML_CHARDATA;
  document->element->children.bits[0].content.CharData = "<![CDATA[yahoo!]]>";

  xw = RXML_bytes_from_xbit (context, &my_bw);
  RXML_write_document (document, xw);

  bw = RXML_parse_xml (context, xw, "iso-8859-1");
  bw->write (bw, str, strlen (str));
  bw->close (bw);

  bw = RXML_parse_xml_utf8 (context, xw);
  {
    int len;
    int i;

    len = strlen (str);
    for (i = 0; i < len; i++)
      bw->write (bw, str + i, 1);
  }
  bw->close (bw);

  test_dom (context);
  RDOM_create (context, &rdoc);
  xw = RDOM_RXML_create_document (context, &rdoc);

  bw = RXML_parse_xml (context, xw, "iso-8859-1");
  bw->write (bw, str, strlen (str));
  bw->close (bw);

  print_document (&rdoc);
  putchar ('\n');

  context->free_context (context);

  return 0;
}

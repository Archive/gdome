eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}' && eval 'exec perl -S $0 $
argv:q'
  if 0;
#use strict;

# Usage: ifgen.pl < DOM.idl

# generates gdome.c, gdome.h, gdome-xml.c


# a q&d idl parser just for parsing DOM.idl
sub parse_idl {
    my ($idl_fn) = @_;
    my $cur_if;
    my $if_attr;

    open IDL, $idl_fn;
    while (<IDL>) {
	if (!$cur_if && /^\s*interface\s+(\w+)\s*(:\s*(.*\S))?\s*\{\s*$/) {
	    $cur_if = $1;
	    $if_attr = {};
	    $if_methods = [];
	    if ($3) {
		$$if_attr{'super'} = $3;
	    }
        } elsif ($cur_if && /^\s*\}\s*\;\s*$/) {
	    $$if_attr{'methods'} = $if_methods;
	    $ifs{$cur_if} = $if_attr;
	    undef $cur_if;
	} elsif ($cur_if) {
	    $line = $_;
	    $line =~ s/\/\/.*//;
	    if ($line =~ /;/) {
		$method = $prefix.' '.$line;
		push @$if_methods, $method;
		$prefix = '';
	    } else {
		chomp $line;
		$prefix .= $line;
	    }
	}
    }
    close IDL;
}


sub dump_ifs {
foreach $if (keys %ifs) {
    print "$if: {";
	%attr = %{$ifs{$if}};
$sep = "";
foreach $key (keys %attr) {
    print "$sep$key => $attr{$key}";
    $sep = ", ";
}
print "}\n";
}
}

sub toposort {
    my (@ifs) = @_;
    my %visited;
    my @result;
    my @queue;

    @queue = sort @ifs;

    while (@queue) {
	my $node = shift @queue;
	next if $visited{$node};
	my $supers = ${$ifs{$node}}{'super'};
    my $queued = 0;
    foreach my $super (split /,\s*/, $supers) {
	if ($super && !$visited{$super}) {
	    if (!$queued) {
		unshift @queue, $node;
	    }
	    $queued = 1;
	    unshift @queue, $super;
	}
    }
    if (!$queued) {
	push @result, $node;
	$visited{$node} = 1;
    }
}

    return @result;
}

sub is_const {
    my ($method) = @_;

    return ($method =~ /^\s*const\b/);
}

sub is_attribute {
    my ($method) = @_;

    return ($method =~ /\battribute\b/);
}

sub is_readonly {
    my ($method) = @_;

    return ($method =~ /\breadonly\b/);
}

sub attr_name {
    my ($method) = @_;

    if ($method =~ /\s+(\w+)\s*;/) {
	return $1;
    }
}

sub attr_type {
    my ($method) = @_;

    if ($method =~ /\battribute\s+(.*\S)\s+\w+\s*;/) {
	return $1;
    }
}

sub meth_return_type {
    my ($method) = @_;
    if ($method =~ /^\s*(.*\S)\s+\w+\s*\(/) {
	return $1;
    }
}

sub meth_name {
    my ($method) = @_;
    if ($method =~ /\b(\w+)\s*\(/) {
	return $1;
    }
}

sub meth_args {
    my ($method) = @_;
    if ($method =~ /\((.*)\)/) {
	return $1;
    }
}

sub veryshort {
    my ($ifname) = @_;
    my %vstab = (
	Node => 'n',
	Attr=> 'a',
	Text=> 't',
	Notation => 'not',
	Element => 'el',
	Entity => 'ent',
	EntityReference => 'er',
	CDATASection => 'cds',
	Document => 'doc',
	ProcessingInstruction => 'pi',
	CharacterData => 'cd',
	Comment => 'c',
	DocumentType => 'dt',
	DocumentFragment => 'df',
	EventListener => 'evntl',
	Event => 'evnt',
	EventTarget => 'evntt',
	MutationEvent => 'mevnt',
	UIEvent => 'uievnt',
	NamedNodeMap => 'nnm',
	NodeList => 'nl'
    );

    if ($vstab{$ifname}) {
	return $vstab{$ifname};
    } else {
	return $ifname;
    }
}

sub gdome_name {
    my ($name) = @_;

    return "Gdome$name";
}

# return value includes trailing blank if appropriate
sub dom_type_to_c {
    my ($domtype) = @_;

    if ($domtype eq 'unsigned long') {
	return 'unsigned long ';
    } elsif ($domtype eq 'long') {
	return 'long ';
    } elsif ($domtype eq 'void') {
	return 'void ';
    } elsif ($domtype eq 'unsigned short') {
	return 'unsigned short ';
    } elsif ($domtype eq 'boolean') {
	return 'GdomeBoolean ';
    }
    return (gdome_name ($domtype)." *");
}

sub need_result_arg {
    my ($domtype) = @_;
    my $ctype = dom_type_to_c ($domtype);

    return 0 if ($ctype =~ / $/);
    return 0 if ($ctype eq 'GdomeDOMString *');
    return 1;
}

sub gen_typedefs {
    foreach $if (toposort (keys %ifs)) {
#	print CURFILE "/* definition for $if */\n";
	my $gname = gdome_name ($if);
	print CURFILE "typedef struct _$gname $gname;\n";
    }
    print CURFILE "\n";
    foreach $if (toposort (keys %ifs)) {
	my $gname = gdome_name ($if);
	print CURFILE "typedef struct _${gname}Vtab ${gname}Vtab;\n";
    }
    print CURFILE "\n";
}

sub gen_objs {
    foreach my $if (toposort (keys %ifs)) {
	my $gname = gdome_name ($if);
	print CURFILE "struct _$gname {\n";
	print CURFILE "  const ${gname}Vtab *vtab;\n";
#	print CURFILE "  GdomeId h1, h2, h3;\n";
	print CURFILE "};\n";
	print CURFILE "\n";
    }
    print CURFILE "\n";
}

sub gen_consts {
    my $blank;

    foreach my $if (toposort (keys %ifs)) {
	%if = %{$ifs{$if}};
	@methods = @{$if{'methods'}};
	foreach my $meth (@methods) {
	    if (is_const ($meth)) {
		if ($meth =~ /\b(\w+)\s*\=\s*(.*);/) {
		    print CURFILE "#define GDOME_$1 $2\n";
		    $blank = "\n";
		}
	    }
	}
	print CURFILE $blank;
	$blank = '';
    }
}

sub gen_method {
    my ($method) = @_;
    my ($rettype, $methname, $args) = @{$method};

    print CURFILE "  $rettype(*$methname) ($args, GdomeException *exc);\n";
}

sub get_cmethods {
    foreach my $if (toposort (keys %ifs)) {
	%if = %{$ifs{$if}};
	@methods = @{$if{'methods'}};
	$cmeths = [];
	my $gname = gdome_name ($if);
	if (!$if{'super'}) {
	    push @{$cmeths}, ['void ', 'ref', "$gname *self"];
	    push @{$cmeths}, ['void ', 'unref', "$gname *self"];
	    push @{$cmeths}, ['void * ', 'query_interface', "$gname *self, const char *interface"];
	}
	foreach my $meth (@methods) {
	    if (is_attribute ($meth)) {
		$attrname = attr_name ($meth);
		$ctype = dom_type_to_c (attr_type ($meth));
		$args = "$gname *self";
#		if (need_result_arg (attr_type ($meth))) {
#		    $args .= ", ${ctype}result";
#		}
		$cmethod = [$ctype, $attrname, $args];
		push @{$cmeths}, $cmethod;
		if (!is_readonly ($meth)) {
		    $cmethod = ['void ', "set_$attrname", "$gname *self, $ctype$attrname"];
		    push @{$cmeths}, $cmethod;
		}
	    } elsif (!is_const ($meth)) {
		# it's a method, not an attribute
		$meth =~ s/\braises\s*\([^\)]*\)//;
#		print "/* $meth */\n";
		$methname = meth_name ($meth);
		$crtype = dom_type_to_c (meth_return_type ($meth));
		$args = "$gname *self";
		foreach my $arg (split (/\s*,\s*/, meth_args ($meth))) {
		    $arg =~ s/in\s+//;
		    if ($arg =~ /^(.*\S)\s+(\w+)$/) {
			$argname = $2;
			$catype = dom_type_to_c ($1);
			$args .= ", $catype$argname";
		    }
		}
#		if (need_result_arg (meth_return_type ($meth))) {
#		    $args .= ", ${crtype}result";
#		}
		$cmethod = [$crtype, $methname, $args];
		push @{$cmeths}, $cmethod;
	    }
	}
	${$ifs{$if}}{'cmethods'} = $cmeths;
    }
}

# Return the number of methods along the single-inheritance spine
sub compute_single_size {
    my ($if) = @_;
    my $size = 0;

    while ($if) {
	my %if = %{$ifs{$if}};
	my @cmethods = @{$if{'cmethods'}};
	$size += @cmethods;
	my @supers = split /,\s*/, $if{'super'};
	$if = $supers[0];
    }
    return $size;
}

# After this routine is called, $base{$if} contains a "base location"
# for each interface.

# This routine sucks for all but the simplest MI layouts. Fortunately,
# that's not a problem right now. In particular, if any of the MI
# invoked interfaces is itself a subclass, we're in trouble.
sub layout_mi {
    my %mi_invoked = ();
    my $max_size = 0;

    foreach my $if (keys %ifs) {
	my %if = %{$ifs{$if}};
	my @supers = split /,\s*/, $if{'super'};
	if (@supers > 1) {
	    for my $i (1..$#supers) {
		my $klass = @supers[$i];
		while ($klass) {
		    $mi_invoked{$klass} = 1;
		    my %kif = %{$ifs{$klass}};
		    my @ksupers = split /,\s*/, $kif{'super'};
		    $klass = $ksupers[0];
		}
	    }
	}
    }
    foreach my $if (keys %ifs) {
	if (!$mi_invoked{$if}) {
	    my $size = compute_single_size ($if);
#	    print "size of $if = $size\n";
	    if ($size > $max_size) {
		$max_size = $size;
	    }
	    $base{$if} = 0;
	}
    }
    print "max size = $max_size\n";
    foreach my $if (keys %ifs) {
	if ($mi_invoked{$if}) {
	    $base{$if} = $max_size;
	    my $size = compute_single_size ($if);
	    $max_size += $size - 1;
	}
#	print "base of $if = $base{$if}\n";
    }
}

sub object_base_method {
    my ($method_name) = @_;

    return ($method_name eq 'ref' ||
	    $method_name eq 'unref' ||
	    $method_name eq 'query_interface');
}

sub gen_vtabs {
    layout_mi ();
    foreach my $if (toposort (keys %ifs)) {
	my $posn = 0;
	%if = %{$ifs{$if}};
	my $gname = gdome_name ($if);
	if ($base{$if}) {
	    print CURFILE "struct _${gname}Vtab$base{$if} {\n";
	    @cmethods = @{$if{'cmethods'}};
	    foreach my $cmethod (@cmethods) {
		my ($rettype, $methname, $args) = @{$cmethod};
		if (!object_base_method ($methname)) {
		    gen_method ($cmethod);
		}
	    }
	    print CURFILE "};\n";
	    print CURFILE "\n";
	    print CURFILE "struct _${gname}Vtab {\n";
	    my $posn = 0;
	    foreach my $cmethod (@cmethods) {
		my ($rettype, $methname, $args) = @{$cmethod};
		if (object_base_method ($methname)) {
		    gen_method ($cmethod);
		    $posn++;
		}
	    }
	    $padding = $base{$if} - $posn; 
	    if ($padding > 0) {
		print CURFILE "  void *dummy[$padding];\n";
		$posn += $padding;
	    }
	    print CURFILE "  struct _${gname}Vtab$posn impl;\n";
	    print CURFILE "};\n";
	} else {
	    print CURFILE "struct _${gname}Vtab {\n";
	    my @supers = split (/,\s*/, $if{'super'});
	    if (@supers >= 1) {
		$posn += compute_single_size ($supers[0]);
		my $gsuper = gdome_name ($supers[0]);
		my $supername = "super";
		if (@supers > 1) {
		    $supername = "super0";
		}
		print CURFILE "  ${gsuper}Vtab $supername;\n";
	    }
	    @cmethods = @{$if{'cmethods'}};
	    foreach my $cmethod (@cmethods) {
		gen_method ($cmethod);
		$posn++;
	    }
	    if (@supers > 1) {
		for my $i (1..$#supers) {
		    $padding = $base{$supers[$i]} - $posn; 
		    if ($padding > 0) {
			print CURFILE "  void *dummy[$padding];\n";
			$posn += $padding;
		    }
		    my $gsuper = gdome_name ($supers[$i]);
		    print CURFILE "  struct _${gsuper}Vtab$posn super$i;\n";
		    $posn += compute_single_size ($supers[$i]);
		}
	    }
	    print CURFILE "};\n";
	}
	print CURFILE "\n";
    }
}

sub gen_invoker_h_super {
    my ($selfname, $short, $if) = @_;

    my $supers = ${$ifs{$if}}{'super'};
    foreach my $super (split (/,\s*/, $supers)) {
	%if = %{$ifs{$super}};
	@cmethods = @{$if{'cmethods'}};
	foreach my $cmethod (@cmethods) {
	    my ($rettype, $methname, $args) = @{$cmethod};
	    $args =~ s/^\w+//;
	    $args = $selfname.$args;
#	    my $short = veryshort ($if);
	    print CURFILE "${rettype}gdome_${short}_$methname ($args, GdomeException *exc);\n";
	    $blank = "\n";
	}

	gen_invoker_h_super ($selfname, $short, $super);
	
    }
}

sub gen_invoker_headers {
    my $blank;
    foreach my $if (toposort (keys %ifs)) {
	%if = %{$ifs{$if}};
	@cmethods = @{$if{'cmethods'}};
	my $short = veryshort ($if);
	foreach my $cmethod (@cmethods) {
	    my ($rettype, $methname, $args) = @{$cmethod};
	    print CURFILE "${rettype}gdome_${short}_$methname ($args, GdomeException *exc);\n";
	    $blank = "\n";
	}

	gen_invoker_h_super (gdome_name ($if), $short, $if);

	print CURFILE $blank;
	$blank = '';
    }
}

sub gen_invoker_super {
    my ($selfname, $short, $if, $superpath) = @_;

    my $supers = ${$ifs{$if}}{'super'};
    my @supers = split (/,\s*/, $supers);
    my $ix;
    if (@supers == 1) {
	$ix = '';
    } else {
	$ix = 0;
    }
    foreach my $super (@supers) {
	print CURFILE "/* gen_invoker_super $selfname $short $if: $super ($superpath)*/\n";
	my %if = %{$ifs{$super}};
	my @cmethods = @{$if{'cmethods'}};
#	my $short = veryshort ($if);
        my $gsuper = gdome_name ($super);
	my $this_sp = $superpath."super$ix.";
	foreach my $cmethod (@cmethods) {
	    my ($rettype, $methname, $args) = @{$cmethod};
	    next if (object_base_method ($methname) && $ix > 0); # hack!
	    $args =~ s/^\w+//;
	    $args = $selfname.$args;
	    $rettype =~ s/ $//;
	    print CURFILE "$rettype\n";
#	    print " $if: $methname\n";
	    print CURFILE "gdome_${short}_$methname ($args, GdomeException *exc)\n";
	    print CURFILE "{\n";
	    print CURFILE "  *exc = 0;\n";
	    if ($rettype eq 'void') {
		$return = '';
	    } else {
		$return = 'return ';
	    }
	    print CURFILE "  ${return}self->vtab->$this_sp$methname (";
	    $sep = '';
	    foreach my $arg (split (/\s*,\s*/, $args)) {
		if ($arg =~ /\W(\w+)$/) {
		    my $stripped = $1;
		    if ($stripped eq 'self') {
			$stripped = "($gsuper *)self";
		    }
		    print CURFILE "$sep$stripped";
		    $sep = ', ';
		}
	    }
	    print CURFILE ", exc);\n";
	    print CURFILE "}\n";
	    print CURFILE "\n";
	}
	gen_invoker_super ($selfname, $short, $super, $this_sp);
	$ix++;
    }
}

sub gen_invokers {
    foreach my $if (toposort (keys %ifs)) {
#	print "gen_invokers $if\n";
	%if = %{$ifs{$if}};
	@cmethods = @{$if{'cmethods'}};
	my $short = veryshort ($if);
	foreach my $cmethod (@cmethods) {
	    my ($rettype, $methname, $args) = @{$cmethod};
	    $rettype =~ s/ $//;
	    print CURFILE "$rettype\n";
#	    print " $if: $methname\n";
	    print CURFILE "gdome_${short}_$methname ($args, GdomeException *exc)\n";
	    print CURFILE "{\n";
	    print CURFILE "  *exc = 0;\n";
	    if ($rettype eq 'void') {
		$return = '';
	    } else {
		$return = 'return ';
	    }
	    if ($base{$if} && !object_base_method ($methname)) {
		print CURFILE "  ${return}self->vtab->impl.$methname (";
	    } else {
		print CURFILE "  ${return}self->vtab->$methname (";
	    }
	    $sep = '';
	    foreach my $arg (split (/\s*,\s*/, $args)) {
		if ($arg =~ /\W(\w+)$/) {
		    print CURFILE "$sep$1";
		    $sep = ', ';
		}
	    }
	    print CURFILE ", exc);\n";
	    print CURFILE "}\n";
	    print CURFILE "\n";
	}
	gen_invoker_super (gdome_name ($if), $short, $if);
    }
}

# Get the classes from the .c.in file.
# class Attr {
#    xmlAttr *a;
#    xmlDoc *d;
# }
# becomes:
# $classes{'Attr'} = ['xmlAttr *a;', 'xmlDoc *d'];
sub get_classes {
    my ($impl) = @_;

    open (GXML, "gdome-$impl.c.in");

    my $funcs = "";

    while (<GXML>) {
	my $line = $_;
	if ($line =~ /^class\s+(\w+)\s*(:\s*(\w+))?\s*\{/) {
	    my $class = $1;
	    my $super = $3;
	    $classes{$class} = [];
	    if ($super) {
		$impl_super{$class} = $super;
	    }
	    while (<GXML>) {
		last if /^\}/;
		s/^\s*//; # strip trailing blanks
		chomp;    # strip trailing newline
	    	push @{$classes{$class}}, $_;
	    }
	}
    }
    
    close (GXML);
}

# This should be coming from the .c.in file

#%libxml_name = (
#    Node => 'xmlNode',
#    Notation => 'xmlNotation',
#    Attr => 'xmlAttr',
#    Document => 'xmlDoc',
#    DocumentType => 'xmlDtd'
#		);


sub gen_priv_typedefs {
    my ($impl) = @_;

    foreach $if (toposort (keys %ifs)) {
	my $gname = gdome_name ($if);
	$gname =~ s/^Gdome//;
	print CURFILE "typedef struct _Gdome_${impl}_$gname Gdome_${impl}_$gname;\n";
    }
    print CURFILE "\n";
}

# returns ('xmlNode', 'n');
#sub find_privates {
#    my ($if) = @_;
#
#    for (my $klass = $if; $klass; $klass = $super) {
#	if ($libxml_name{$klass}) {
#	    $ksh = veryshort ($klass);
#	    return ($libxml_name{$klass}, $ksh);
#	} else {
#	    %kif = %{$ifs{$klass}};
#	    $super = $kif{'super'};
#	}
#    }
#    return ();
#}

# returns ('xmlAttr *a;', 'xmlDoc *doc;')
sub find_impl {
    my ($if) = @_;
    my @result;

    if ($classes{$if}) {
	@result = find_impl ($impl_super{$if});
	push @result, @{$classes{$if}};
	return @result;
    } else {
	return ();
    }
}

sub gen_priv_objs {
    my ($impl) = @_;
    my ($itype, $iname);

    foreach $if (toposort (keys %ifs)) {
	my $gname = gdome_name ($if);
	my $gname_without_the_gdome = $gname;
	$gname_without_the_gdome =~ s/^Gdome//;
	print CURFILE "struct _Gdome_${impl}_$gname_without_the_gdome {\n";
	my $short = veryshort ($if);
	print CURFILE "  $gname super;\n";
	foreach my $priv (find_impl($if)) {
	    print CURFILE "  $priv\n";
	}
	print CURFILE "};\n";
	print CURFILE "\n";
    }
}

# Return a list of all cmethods for an interface, including superclasses,
# with no concern for duplication
sub all_cmethods {
    my ($if) = @_;
    my @result;

    my %if = %{$ifs{$if}};
    if ($if{'super'}) {
	my @supers = split (/,\s*/, $if{'super'});
	foreach my $super (@supers) {
	    push @result, all_cmethods ($super);
	}
    }
    push @result, @{$if{'cmethods'}};
    return @result;
}

sub gen_class_skel {
    my ($if, $impl, $sub) = @_;

    if (defined $sub) {
	print CURFILE "/* implementations of methods in interface $if:$sub */\n";
    } else {
	print CURFILE "/* implementations of methods in interface $if */\n";
    }
    print CURFILE "\n";
    %if = %{$ifs{$if}};
    my $short = veryshort ($if);
    my @cmethods = all_cmethods ($if);

#    print "$if:";
#    foreach my $cmethod (@cmethods) {
#	my ($rettype, $methname, $args) = @{$cmethod};
#	print " $methname";
#    }
#    print ";\n";

    foreach my $cmethod (@cmethods) {
	my ($rettype, $methname, $args) = @{$cmethod};
#	print " $methname";
	# get and check full method name
	my $fullmeth = dispatch ($impl, $if, $methname, $sub);
	next if $have_done{$fullmeth} || $fullmeth eq 'NULL';
	
	if (!$implemented{$fullmeth} ne "") {
	    print CURFILE "/* Not in the .c.in file, this is a stub! */\n";
	}
	
	$rettype =~ s/ $//;
	print CURFILE "static $rettype\n";
	$short = veryshort ($if);
	
	print CURFILE "$fullmeth ($args, GdomeException *exc)\n";
	
	# Check against implemented methods
	if ($implemented{$fullmeth} ne "") {
	    # uncomment this to generate stubs for methods:
#	    print "method $fullmeth ($args)\n";
	    print CURFILE $implemented{$fullmeth};
	    $used{$fullmeth} = 1;
	} else {
	    print CURFILE "{\n";
	    my $gname = gdome_name ($if);
	    $gname =~ s/^Gdome//;
	    print CURFILE "  Gdome_${impl}_$gname *priv = (Gdome_${impl}_$gname *)self;\n";
	    foreach my $priv (find_impl ($if)) {
		if ($priv =~ /^(.*\W)(\w+);$/) {
		    print CURFILE "  $1$2 = priv->$2;\n";
		}
	    }
#	    my ($itype, $iname) = find_privates ($if);
#	    if ($itype) {
#		print CURFILE "  $itype *$iname = priv->$iname;\n";
#	    }
	    %kif = %{$ifs{$klass}};
	    print CURFILE "\n";
#	        if ($libxml_name{$if}) {
#		    $lxn = $libxml_name{$if};
#		    print CURFILE "  $lxn *$short = ($lxn *)self->h1.p;\n";
#		    print CURFILE "\n";
#	        }
	    if ($rettype =~ /\*/) {
		print CURFILE "  return NULL;\n";
	    } elsif ($rettype ne 'void') {
		print CURFILE "  return 0;\n";
	    }
	    print CURFILE "}\n";
	}
	$have_done{$fullmeth} = 1;
	print CURFILE "\n";
    }
#    print "\n";
}

sub gen_skeletons {
    my ($impl) = @_;
    my ($last_was_blank, $is_blank);

    # Parse gdome-xml-impl.c and look for implemented methods.

    open (GXML, "gdome-$impl.c.in");

    my $funcs = "";

    while (<GXML>) {
	my $line = $_;
	if ($line =~ /^subclass\s+(\w+)\s*\:\s*(\w+)\s*\{/) {
	    my $superclass = $1;
	    my $sub = $2;
	    my $body = "";
	    while (<GXML>) {
		last if /^\}/;
	    	$body .= $_;
	    }
	    my $gsuper = gdome_name ($superclass);
	    $gsuper =~ s/^Gdome//;
	    print CURFILE "typedef struct _Gdome_${impl}_${sub}_$gsuper Gdome_${impl}_${sub}_$gsuper;\n";
	    print CURFILE "\n";
	    print CURFILE "struct _Gdome_${impl}_${sub}_$superclass {\n";
	    print CURFILE "  Gdome$gsuper super;\n";
	    print CURFILE $body;
	    print CURFILE "};\n";
	    print CURFILE "\n";
	    my $short = veryshort ($superclass);
	    print CURFILE "const Gdome${gsuper}Vtab gdome_${impl}_${sub}_${short}_vtab;\n";
	    print CURFILE "\n";
	    push @subclasses, [$superclass, $sub];
	} elsif ($line =~ /^class\s+(\w+)/) {
	    while (<GXML>) {
		last if /^\}/;
	    }
	} elsif ($line =~ /^method\s+(\w+)/) {
	    my $methname = $1;

	    # we have method name, now find body
	    print CURFILE "/* method $methname defined here */\n";
	    my $body = "";
	    while (<GXML>) {
	    	$body .= $_;
		last if /^\}/;
	    }
	    $implemented{$methname} = $body;
	} else {
	    s/\/\/[ \t]*([^\n]*)/\/\* $1 \*\//; # convert C++ comments to C
	    $is_blank = (/^\s*$/);
	    push @funcs, $_ if (!$is_blank || !$last_was_blank);
	    $last_was_blank = $is_blank;
	}
    }
    
    close (GXML);

    print CURFILE "/* Begin statically implemented functions. Note: there
  may be isolated comments here from the .c.in file. */\n";
    print CURFILE "\n";
    print CURFILE @funcs;
    print CURFILE "\n" unless $last_was_blank;
    print CURFILE "/* End statically implemented functions. */\n";
    print CURFILE "\n";
    @funcs = ();

    # Generate skeletons - checking for implemented methods.
    foreach my $if (toposort (keys %ifs)) {
	if ($classes{$if}) {
	    gen_class_skel ($if, $impl);
	}
    }
    foreach my $sc (@subclasses) {
	my ($super, $sub) = @$sc;
	gen_class_skel ($super, $impl, $sub);
    }
}

# Return the implementing function for a given method in a given interface
sub dispatch {
    my ($impl, $if, $methname, $sub) = @_;
    my $result;
    my $short = veryshort ($if);

#    print "dispatch: $impl $if $methname (super = $impl_super{$if})\n";
    my $fimpl = $impl;
    if ($sub) {
	$fimpl .= "_$sub";
    }
    $result = "gdome_${fimpl}_${short}_$methname";

    if ($implemented{$result}) {
	return $result;
    }

    if ($impl_super{$if}) {
	$result = dispatch ($impl, $impl_super{$if}, $methname, $sub);
	return $result;
    }

    if (!$classes{$if}) {
	return "NULL";
    }

    my %if = %{$ifs{$if}};
    foreach my $cmethod (@{$if{'cmethods'}}) {
	my ($rettype, $the_methname, $args) = @{$cmethod};
	if ($the_methname eq $methname) {
	    return "gdome_${fimpl}_${short}_$methname";
	}
    }

    return "NULL";
}

sub gen_generic_vtab_contents {
    my ($if, $this_if, $indent, $impl, $sub) = @_;
    my %if = %{$ifs{$this_if}};
    my $nl = '';
    my $child_short;

    my @supers = ();
    my @cmethods = @{$if{'cmethods'}};
    if ($base{$if}) {
	foreach my $cmethod (@cmethods) {
	    my ($rettype, $methname, $args) = @{$cmethod};
	    print "$methname\n";
	    if (object_base_method ($methname)) {
		print CURFILE ($nl.("  " x $indent).dispatch ($impl, $if, $methname, $sub));
		$posn++;
		$nl = ",\n";
	    }
	}
	if ($base{$if} > $posn) {
	    print CURFILE ($nl.("  " x $indent)."{ NULL }");
	    $posn = $base{$supers[$i]};
	    $nl = ",\n";
	}
	print CURFILE ($nl.("  " x $indent)."{\n");
	$nl = '';
	foreach my $cmethod (@cmethods) {
	    my ($rettype, $methname, $args) = @{$cmethod};
	    if (!object_base_method ($methname)) {
		print CURFILE ($nl.("  " x (1 + $indent)).dispatch ($impl, $if, $methname, $sub));
		$posn++;
		$nl = ",\n";
	    }
	}
	print CURFILE "\n";
	print CURFILE (("  " x $indent)."}");
	return $nl;
    }
    if ($if{'super'}) {
	@supers = split /,\s*/, $if{'super'};
	print CURFILE ($nl.("  " x $indent)."{\n");
	$nl = gen_generic_vtab_contents ($if, $supers[0], $indent + 1, $impl);
	print CURFILE "\n";
	print CURFILE (("  " x $indent)."}");
	$nl = ",\n";
    }
    foreach my $cmethod (@cmethods) {
	my ($rettype, $methname, $args) = @{$cmethod};
	if (!object_base_method ($methname) || $posn < 3) {
	    print CURFILE ($nl.("  " x $indent).dispatch ($impl, $if, $methname, $sub));
	    $posn++;
	    $nl = ",\n";
	}
    }
    for my $i (1..$#supers) {
	if ($posn < $base{$supers[$i]}) {
	    print CURFILE ($nl.("  " x $indent)."{ NULL }");
	    $posn = $base{$supers[$i]};
	    $nl = ",\n";
	}
	print CURFILE ($nl.("  " x $indent)."{\n");
	$nl = gen_generic_vtab_contents ($if, $supers[$i], $indent + 1, $impl);
	print CURFILE "\n";
	print CURFILE (("  " x $indent)."}");
	$nl = ",\n";
    }
    return $nl;
}

sub gen_generic_vtabs {
    my ($impl) = @_;
    my $nl = '';
    foreach my $if (toposort (keys %ifs)) {
	my %if = %{$ifs{$if}};
	$short = veryshort ($if);
	my $gname = gdome_name ($if);
	print CURFILE "const ${gname}Vtab gdome_${impl}_${short}_vtab = {\n";
	$posn = 0;
	$nl = gen_generic_vtab_contents ($if, $if, 1, $impl);
	print CURFILE "\n";
	print CURFILE "};\n";
	print CURFILE "\n";
    }
    if (@subclasses) {
	print CURFILE "/* Vtables for subclassed implementations */\n";
	print CURFILE "\n";
    }
    foreach my $sc (@subclasses) {
	my ($super, $sub) = @$sc;
	my %if = %{$ifs{$super}};
	my $gname = gdome_name ($super);
	$short = veryshort ($super);
	print CURFILE "/* ${super}:${sub} */\n";
	print CURFILE "const ${gname}Vtab gdome_${impl}_${sub}_${short}_vtab = {\n";
	$posn = 0;
	$nl = gen_generic_vtab_contents ($super, $super, 1, $impl, $sub);
	print CURFILE "\n";
	print CURFILE "};\n";
	print CURFILE "\n";
    }
}

sub gen_generic_vtab_decls {
    my ($impl) = @_;
    my $blank;
#    print CURFILE "/* Vtab decls */\n";
#    print CURFILE "\n";
    foreach my $if (toposort (keys %ifs)) {
	my %if = %{$ifs{$if}};
	$short = veryshort ($if);
	my $gname = gdome_name ($if);
	print CURFILE "const ${gname}Vtab gdome_${impl}_${short}_vtab;\n";
	$blank = "\n";
    }
    print CURFILE $blank;
    $blank = '';
}

if (@ARGV < 1) {
    die "Usage: ./ifgen.pl DOM.idl\n";
}
$idl = $ARGV[0];
parse_idl ($idl);
#dump_ifs ();


# beg of gdome.h
open (CURFILE, ">gdome.h");

print CURFILE "/* ----- gdome.h ----- */\n";
print CURFILE << 'EOF';
#ifndef GDOME_H
#define GDOME_H

#define GDOME_REFCOUNT

typedef struct _GdomeDOMString GdomeDOMString;
typedef int GdomeBoolean;
typedef unsigned short GdomeException;

struct _GdomeDOMString {
  void (*unref) (GdomeDOMString *self);
  char *str;
};

EOF

gen_typedefs ();
gen_consts ();
gen_objs ();
get_cmethods ();
gen_vtabs ();
print CURFILE << 'EOF';
void gdome_str_unref (GdomeDOMString *self);

EOF

gen_invoker_headers ();

print CURFILE "#endif /* GDOME_H */";
close (CURFILE);
# end of gdome.h

# beg of gdome.c
open (CURFILE, ">gdome.c");
print CURFILE "/* ----- gdome.c ----- */\n";
print CURFILE << 'EOF';
#include "gdome.h"

void gdome_str_unref (GdomeDOMString *self)
{
  self->unref (self);
}

EOF

gen_invokers ();

close (CURFILE);
# end of gdome.c

open (CURFILE, ">gdome-xml.c");
print CURFILE "/* ----- gdome-xml.c ----- */\n";
print CURFILE "/* This file was automatically generated by ifgen.pl from
   the idl file $idl and the input file gdome-xml.c.in.
*/

";
print CURFILE << 'EOF';
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include "gdome.h"
#include <tree.h>
#include <parser.h>
#include <xmlmemory.h>
#include "gdome-xml.h"
#include "gdome-xml-util.h"

EOF

$impl = 'xml';
get_classes ($impl);
gen_priv_typedefs ($impl);
gen_priv_objs ($impl);
gen_generic_vtab_decls ($impl);
gen_skeletons ($impl);
gen_generic_vtabs ($impl);

foreach $meth (keys %implemented) {
    if (!$used{$meth}) {
	print "Warning: unused method $meth\n";
    }
}

if (0) {
    foreach $if (keys %ifs) {
	my %if = %{$ifs{$if}};
	@cmethods = @{$if{'cmethods'}};
	$nc = @cmethods;
	print "interface $if has $nc methods\n";
    }
}


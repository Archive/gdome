/* Raph's DOM.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

#include <stdlib.h>
#include "rxml.h" /* for the allocation contexts, etc. */
#include "rdom.h"
#include "rdom-private.h"

/* Convenience functions for invoking DOM methods */

RDOM_Type
RDOM_get_nodeType (RDOM_Node *node)
{
  return node->dom->klass->get_type (node);
}

RDOM_Node *
RDOM_firstChild (RDOM_Node *node, RDOM_Node *result)
{
  return node->dom->klass->firstChild (node, result);
}

RDOM_Node *
RDOM_lastChild (RDOM_Node *node, RDOM_Node *result)
{
  return node->dom->klass->lastChild (node, result);
}

RDOM_Node *
RDOM_parentNode (RDOM_Node *node, RDOM_Node *result)
{
  return node->dom->klass->parentNode (node, result);
}

RDOM_Node *
RDOM_nextSibling (RDOM_Node *node, RDOM_Node *result)
{
  return node->dom->klass->nextSibling (node, result);
}

RDOM_Node *
RDOM_previousSibling (RDOM_Node *node, RDOM_Node *result)
{
  return node->dom->klass->previousSibling (node, result);
}

RDOM_NamedNodeMap *
RDOM_get_attributes (RDOM_Node *node, RDOM_NamedNodeMap *result)
{
  return node->dom->klass->get_attributes (node, result);
}

void
RDOM_appendChild (RDOM_Node *node, RDOM_Node *child)
{
  node->dom->klass->appendChild (node, child);
}

void
RDOM_add_listener (RDOM_Node *node, RDOM_Listener *listener)
{
  node->dom->klass->add_listener (node, listener);
}

void
RDOM_remove_listener (RDOM_Node *node, RDOM_Listener *listener)
{
  node->dom->klass->remove_listener (node, listener);
}

RDOM_Element *
RDOM_createElement (RDOM_Document *document, const char *tagName,
		    RDOM_Element *result)
{
  return document->dom->klass->createElement (document, tagName, result);
}

RDOM_Text *
RDOM_createTextNode (RDOM_Document *document,
		     const char *data,
		     RDOM_Text *result)
{
  return document->dom->klass->createTextNode (document, data, result);
}

char *
RDOM_get_tagName (RDOM_Element *element)
{
  return element->dom->klass->get_tagName (element);
}

char *
RDOM_getAttribute (RDOM_Element *element, const char *name)
{
  return element->dom->klass->getAttribute (element, name);
}

void
RDOM_setAttribute (RDOM_Element *element, const char *name, const char *value)
{
  element->dom->klass->setAttribute (element, name, value);
}

char *
RDOM_attr_get_name (RDOM_Attr *attr)
{
  return attr->dom->klass->attr_get_name (attr);
}

char *
RDOM_attr_get_value (RDOM_Attr *attr)
{
  return attr->dom->klass->attr_get_value (attr);
}

int
RDOM_nnm_get_length (RDOM_NamedNodeMap *nnm)
{
  return nnm->dom->klass->nnm_get_length (nnm);
}

RDOM_Node *
RDOM_getNamedItem (RDOM_NamedNodeMap *nnm, const char *name, RDOM_Node *result)
{
  return nnm->dom->klass->getNamedItem (nnm, name, result);
}

RDOM_Node *
RDOM_nnm_item (RDOM_NamedNodeMap *nnm, int index, RDOM_Node *result)
{
  return nnm->dom->klass->nnm_item (nnm, index, result);
}

char *
RDOM_cd_get_data (RDOM_Text *text)
{
  return text->dom->klass->cd_get_data (text);
}

void
RDOM_release_string (RDOM_Node *node, char *string)
{
  node->dom->klass->release_string (node, string);
}

/* Implementation of the methods */

static RDOM_Type
RDOM_N_get_type (RDOM_Node *node)
{
  RDOM_N *n = (RDOM_N *)node->node.p;

  return n->type;
}

static RDOM_Node *
RDOM_N_firstChild (RDOM_Node *node, RDOM_Node *result)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_N *child = n->first_child;

  if (child == NULL)
    return NULL;
  result->dom = node->dom;
  result->node.p = child;
  return result;
}

static RDOM_Node *
RDOM_N_lastChild (RDOM_Node *node, RDOM_Node *result)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_N *child;
  RDOM_N *next;

  child = n->first_child;
  if (child == NULL)
    return NULL;
  while ((next = child->next_sibling) != NULL)
    child = next;
  result->dom = node->dom;
  result->node.p = child;
  return result;
}

RDOM_Node *
RDOM_N_parentNode (RDOM_Node *node, RDOM_Node *result)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_N *parent = n->parent;

  if (parent == NULL)
    return NULL;
  result->dom = node->dom;
  result->node.p = parent;
  return result;
}

static RDOM_Node *
RDOM_N_nextSibling (RDOM_Node *node, RDOM_Node *result)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_N *sib = n->next_sibling;

  if (sib == NULL)
    return NULL;
  result->dom = node->dom;
  result->node.p = sib;
  return result;
}

static RDOM_Node *
RDOM_N_previousSibling (RDOM_Node *node, RDOM_Node *result)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_N *sib;
  RDOM_N *parent = n->parent;
  RDOM_N *next;

  sib = parent->first_child;
  if (sib == n)
    return NULL;
  while ((next = sib->next_sibling) != n)
    sib = next;
  result->dom = node->dom;
  result->node.p = sib;
  return result;
}

static RDOM_NamedNodeMap *
RDOM_N_get_attributes (RDOM_Node *node, RDOM_NamedNodeMap *result)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_E *e = (RDOM_E *)n;

  if (n->type != RDOM_ELEMENT_NODE)
    return NULL;
  result->dom = node->dom;
  result->nnm.p = &e->attributes;
  return result;
}

/* currently, this assumes the node is unattached */
static void
RDOM_N_appendChild (RDOM_Node *node, RDOM_Node *child)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_N *cn = (RDOM_N *)child->node.p;
  RDOM_N *sib;
  RDOM_N *next;
  RDOM_Listener *l;
  RDOM_ListenerList *ll, *llp;

  ll = n->listeners;
  for (llp = ll; llp != NULL; llp = llp->next)
    {
      l = ll->listener;
      l->insert (l, node, child, RDOM_Before);
    }
  sib = n->first_child;
  if (sib == NULL)
    {
      n->first_child = cn;
    }
  else
    {
      while ((next = sib->next_sibling) != NULL)
	sib = next;
      sib->next_sibling = cn;
    }
  cn->parent = n;
  for (llp = ll; llp != NULL; llp = llp->next)
    {
      l = ll->listener;
      l->insert (l, node, child, RDOM_After);
    }
}

static void
RDOM_N_add_listener (RDOM_Node *node, RDOM_Listener *listener)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_ListenerList *ll;
  RDOM_Pr *pr = (RDOM_Pr *)node->dom;

  ll = RXML_new (pr->context, RDOM_ListenerList, 1);
  ll->listener = listener;
  ll->next = n->listeners;
  n->listeners = ll;
}

static void
RDOM_N_remove_listener (RDOM_Node *node, RDOM_Listener *listener)
{
  RDOM_N *n = (RDOM_N *)node->node.p;
  RDOM_ListenerList *ll, *next;
  RDOM_Pr *pr = (RDOM_Pr *)node->dom;

  ll = n->listeners;
  if (ll->listener == listener)
    {
      n->listeners = ll->next;
    }
  else
    {
      while ((next = ll->next)->listener != listener)
	ll = next;
      ll->next = next->next;
      ll = next;
    }
  RXML_free (pr->context, ll);
}

RDOM_Element *
RDOM_D_createElement (RDOM_Document *document, const char *tagName,
		      RDOM_Element *result)
{
  RDOM_Pr *pr = (RDOM_Pr *)document->dom;
  RXML_Context *context = pr->context;
  RDOM_E *e;

  e = RXML_new (context, RDOM_E, 1);
  e->node.listeners = NULL;
  e->node.type = RDOM_ELEMENT_NODE;
  e->node.parent = NULL;
  e->node.first_child = NULL;
  e->node.next_sibling = NULL;
  e->tagName = RXML_strdup (context, tagName);
  e->attributes = NULL;

  result->dom = (RDOM *)pr;
  result->node.p = e;
  return result;
}

RDOM_Text *
RDOM_D_createTextNode (RDOM_Document *document, const char *data,
		       RDOM_Element *result)
{
  RDOM_Pr *pr = (RDOM_Pr *)document->dom;
  RXML_Context *context = pr->context;
  RDOM_T *t;

  t = RXML_new (context, RDOM_T, 1);
  t->node.listeners = NULL;
  t->node.type = RDOM_TEXT_NODE;
  t->node.parent = NULL;
  t->node.first_child = NULL;
  t->node.next_sibling = NULL;
  t->data = RXML_strdup (context, data);

  result->dom = (RDOM *)pr;
  result->node.p = t;
  return result;
}

static char *
RDOM_E_get_tagName (RDOM_Element *element)
{
  RDOM_E *e = (RDOM_E *)element->node.p;

  if (e->node.type != RDOM_ELEMENT_NODE)
    /* todo: warning */
    return NULL;
  return e->tagName;
}

static char *
RDOM_E_getAttribute (RDOM_Element *element, const char *name)
{
  RDOM_E *e = (RDOM_E *)element->node.p;
  RDOM_Alist *al;
  RDOM_A *a;
  RDOM_T *t;

  for (al = e->attributes; al != NULL; al = al->next)
    {
      a = al->attr;
      if (!strcmp (name, a->name))
	{
	  t = (RDOM_T *)a->node.first_child;
	  /* assume t is a Text node */
	  return t->data;
	}
    }
  return NULL;
}

static void
RDOM_E_setAttribute (RDOM_Element *element, const char *name, const char *value)
{
  RDOM_E *e = (RDOM_E *)element->node.p;
  RDOM_Pr *pr = (RDOM_Pr *)element->dom;
  RXML_Context *context = pr->context;
  RDOM_Alist *al;
  RDOM_A *a;
  RDOM_T *t;

  for (al = e->attributes; al != NULL; al = al->next)
    {
      a = al->attr;
      if (!strcmp (name, a->name))
	{
	  t = (RDOM_T *)a->node.first_child;
	  /* assume t is a Text node - probably should check */
	  /* also, we destroy the old value, otherwise we will leak
	     memory. Bah! */
	  RXML_free (context, t->data);
	  t->data = RXML_strdup (context, value);
	  return;
	}
    }
  /* ok, create a new attribute node */
  a = RXML_new (context, RDOM_A, 1);
  t = RXML_new (context, RDOM_T, 1);

  a->node.type = RDOM_ATTRIBUTE_NODE;
  a->node.listeners = NULL;
  a->node.parent = NULL;
  a->node.first_child = &t->node;
  a->node.next_sibling = NULL;
  a->name = RXML_strdup (context, name);

  t->node.type = RDOM_TEXT_NODE;
  t->node.listeners = NULL;
  t->node.parent = &a->node;
  t->node.first_child = NULL;
  t->node.next_sibling = NULL;
  t->data = RXML_strdup (context, value);

  /* insert new attribute into attribute list */
  al = RXML_new (context, RDOM_Alist, 1);
  al->attr = a;
  al->next = e->attributes;
  e->attributes = al;
}

static char *
RDOM_A_get_name (RDOM_Attr *attr)
{
  RDOM_A *a = (RDOM_A *)(attr->node.p);

  return a->name;
}

static char *
RDOM_A_get_value (RDOM_Attr *attr)
{
  RDOM_A *a = (RDOM_A *)(attr->node.p);
  RDOM_T *t = (RDOM_T *)a->node.first_child;

  return t->data;
}

static int
RDOM_NNM_get_length (RDOM_NamedNodeMap *nnm)
{
  RDOM_NNM *map = (RDOM_NNM *)(nnm->nnm.p);
  RDOM_Alist *al = *map;
  int i;

  for (i = 0; al != NULL; i++)
    al = al->next;
  return i;
}

static RDOM_Node *
RDOM_NNM_getNamedItem (RDOM_NamedNodeMap *nnm, const char *name, RDOM_Node *result)
{
  RDOM_NNM *map = (RDOM_NNM *)(nnm->nnm.p);
  RDOM_Alist *al = *map;
  RDOM_A *a;
  int i;

  for (i = 0; al != NULL; i++)
    {
      a = al->attr;
      if (!strcmp (a->name, name))
	{
	  result->dom = nnm->dom;
	  result->node.p = (RDOM_Node *)a;
	return result;
	}
      al = al->next;
    }
  return NULL;
}

static RDOM_Node *
RDOM_NNM_item (RDOM_NamedNodeMap *nnm, int index, RDOM_Node *result)
{
  RDOM_NNM *map = (RDOM_NNM *)(nnm->nnm.p);
  RDOM_Alist *al = *map;
  RDOM_A *a;
  int i;

  for (i = 0; i < index && al != NULL; i++)
    al = al->next;
  if (al != NULL)
    {
      a = al->attr;
      result->dom = nnm->dom;
      result->node.p = (RDOM_Node *)a;
      return result;
    }
  else
    return NULL;
}

static char *
RDOM_CD_get_data (RDOM_CharData *char_data)
{
  RDOM_CD *cd = (RDOM_CD *)char_data->node.p;

  if (cd->node.type != RDOM_TEXT_NODE /* todo or comment node */)
    /* todo: warning */
    return NULL;
  return cd->data;
}

static void
RDOM_N_release_string (RDOM_Node *node, char *string)
{
  /* the string is allocated in the node, rather than copied */
}

const RDOM_Class RDOM_N_Class = {
  RDOM_N_get_type,
  RDOM_N_firstChild,
  RDOM_N_lastChild,
  RDOM_N_parentNode,
  RDOM_N_nextSibling,
  RDOM_N_previousSibling,
  RDOM_N_get_attributes,
  RDOM_N_appendChild,
  RDOM_N_add_listener,
  RDOM_N_remove_listener,

  RDOM_D_createElement,
  RDOM_D_createTextNode,

  RDOM_E_get_tagName,
  RDOM_E_getAttribute,
  RDOM_E_setAttribute,

  RDOM_A_get_name,
  RDOM_A_get_value,

  RDOM_NNM_get_length,
  RDOM_NNM_getNamedItem,
  RDOM_NNM_item,

  RDOM_CD_get_data,

  RDOM_N_release_string
};

RDOM_Document *
RDOM_create (RXML_Context *context, RDOM_Node *result)
{
  RDOM_Pr *pr;
  RDOM_D *d;

  pr = RXML_new (context, RDOM_Pr, 1);
  pr->dom.klass = (RDOM_Class *)&RDOM_N_Class;
  pr->context = context;
  d = RXML_new (context, RDOM_D, 1);
  d->node.listeners = NULL;
  d->node.type = RDOM_DOCUMENT_NODE;
  d->node.parent = NULL;
  d->node.first_child = NULL;
  d->node.next_sibling = NULL;

  result->dom = (RDOM *)pr;
  result->node.p = d;
  return result;
}

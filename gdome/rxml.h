/* Raph's XML parser.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

/* todo: C++ and #ifdef wrapping */

typedef int RXML_i32;
typedef unsigned int RXML_u32;

/* Allocation contexts */

typedef struct _RXML_Context RXML_Context;

struct _RXML_Context {
  void *(*alloc) (RXML_Context *self, int n_bytes);
  void (*free) (RXML_Context *self, void *p);
  void (*free_context) (RXML_Context *self);
};

RXML_Context *RXML_context_default (void);

/* Helpful macros for using the alocation contexts */

#define RXML_new(context, type, n) ((type *)(context->alloc (context, sizeof(type) * n)))

#define RXML_free(context, p) context->free (context, p)

#define RXML_expand(context, p, type, n) (p = (type *)RXML_expand_data(context, p, sizeof(type) * ((n) <<= 1)))

void *RXML_expand_data(RXML_Context *context, void *p, int size);

/* Declarations for basic, low-level stream parser */

typedef struct _RXML_ByteWriter RXML_ByteWriter;
typedef struct _RXML_XbitWriter RXML_XbitWriter;
typedef struct _RXML_Tag RXML_Tag;
typedef struct _RXML_Attr RXML_Attr;

typedef struct _RXML_Error RXML_Error;

struct _RXML_Error {
  char *error;
  int pos;
  int line_no;
  void (*free) (RXML_Error *self);
};

struct _RXML_ByteWriter {
  RXML_Error *(*write) (RXML_ByteWriter *self, char *buf, int size);
  RXML_Error *(*close) (RXML_ByteWriter *self);
};

struct _RXML_XbitWriter {
  RXML_Error *(*start_tag) (RXML_XbitWriter *self, RXML_Tag *tag);
  RXML_Error *(*end_tag) (RXML_XbitWriter *self, RXML_Tag *tag);
  RXML_Error *(*text) (RXML_XbitWriter *self, char *buf);
  /* other stuff, like pi's, maybe entities, etc. */
  RXML_Error *(*close) (RXML_XbitWriter *self);
};

/* Corresponds closely to XML 1.0 [40] STag */
struct _RXML_Tag {
  char *name; /* UTF-8 encoding */
  int n_attrs;
  RXML_Attr *attrs;
};

/* Corresponds to XML 1.0 [41] Attribute */
struct _RXML_Attr {
  char *name; /* UTF-8 encoding */
  char *value; /* UTF-8 encoding, entities replaced */
};

/* XML parser */

/* Assumes utf8 encoding of byte stream */
RXML_ByteWriter *RXML_parse_xml_utf8 (RXML_Context *context,
				      RXML_XbitWriter *xw);

/* This one will convert arbitrary encodings to utf8.
   Autodetect if encoding == NULL */
RXML_ByteWriter *RXML_parse_xml (RXML_Context *context,
				 RXML_XbitWriter *dst, const char *encoding);

/* Declarations for parsing into a simple XML tree */

typedef struct _RXML_Content RXML_Content;
typedef struct _RXML_ContentBit RXML_ContentBit;
typedef struct _RXML_Element RXML_Element;
typedef struct _RXML_Prolog RXML_Prolog;
typedef struct _RXML_Document RXML_Document;

typedef enum {
  RXML_ELEMENT,
  RXML_CHARDATA
} RXML_ContentType;

struct _RXML_ContentBit {
  RXML_ContentType type;
  union {
    RXML_Element *element;
    char *CharData; /* UTF-8 encoding, entities replaced */
    /* more: pi's, comments */
  } content;
};

/* Corresponds to XML 1.0 [43] content */
struct _RXML_Content {
  int n_bits;
  RXML_ContentBit *bits;
};

/* Corresponds to XML 1.0 [39] Element */
struct _RXML_Element {
  RXML_Tag tag;
  RXML_Content children;
};

/* Corresponds to XML 1.0 [22] prolog */
struct _RXML_Prolog {
  /* need DTD stuff here */
  int dummy; /* just to avoid empty structure pedantic warning */
};

/* Corresponds to XML 1.0 [1] document */
struct _RXML_Document {
  RXML_Prolog *prolog;
  RXML_Element *element;
};

typedef struct _RXML_XbitWriterTree RXML_XbitWriterTree;

struct _RXML_XbitWriterTree {
  RXML_XbitWriter writer;
  RXML_Document *(*get_doc) (RXML_XbitWriterTree *self);
  void (*free) (RXML_XbitWriterTree *self);
};

RXML_XbitWriterTree *RXML_tree_from_xbit (RXML_Context *context);

/* Declarations for outputting a document tree as XML, reader form */

typedef struct _RXML_ByteReader RXML_ByteReader;

struct _RXML_ByteReader {
  int (*read) (RXML_ByteReader *self, char *buf, int size);
};

/* Usage:

   RXML_Document *document;
   RXML_ByteReader *reader;
   int bytes_read;
   char buffer[BUF_SIZE];

   // document is valid and non-null
   reader = RXML_bytes_from_tree (context, document);
   for (;;)
     {
       bytes_read = reader->read (reader, buffer, sizeof(buffer));
       if (bytes_read == 0)
         break;
       // output bytes_read bytes to output
     }

   // reader has been freed
   // document may now be freed

*/

RXML_ByteReader *RXML_bytes_from_tree (RXML_Context *context,
				       RXML_Document *document);

/* Declarations for outputting XML, writer form */

RXML_XbitWriter *RXML_bytes_from_xbit (RXML_Context *context,
				       RXML_ByteWriter *bw);

void RXML_write_document (RXML_Document *document, RXML_XbitWriter *xw);

char *RXML_strdup (RXML_Context *context, const char *str);

/* Raph's DOM.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

/* This is the private representation used for an implementation of the
   DOM. Applications access these data structures at their own risk.
*/

typedef struct _RDOM_N RDOM_N;
typedef struct _RDOM_D RDOM_D;
typedef struct _RDOM_E RDOM_E;
typedef struct _RDOM_CD RDOM_CD;
typedef struct _RDOM_CD RDOM_T;
typedef struct _RDOM_ListenerList RDOM_ListenerList;
typedef struct _RDOM_A RDOM_A;
typedef struct _RDOM_Alist RDOM_Alist;
typedef struct _RDOM_Alist *RDOM_NNM;
typedef struct _RDOM_Pr RDOM_Pr;

struct _RDOM_ListenerList {
  RDOM_Listener *listener;
  RDOM_ListenerList *next;
};

/* The implementation of a Node.

   This structure doesn't allow foreign DOM implementations of subtrees */
struct _RDOM_N {
  RDOM_Type type;
  RDOM_ListenerList *listeners;
  RDOM_N *parent;
  RDOM_N *first_child;
  RDOM_N *next_sibling;
};

/* Implementation of a Document. */

struct _RDOM_D {
  RDOM_N node;
  /* maybe DocumentType and DOMImplementation */
};

/* Implementation of an Element. */

struct _RDOM_E {
  RDOM_N node;
  char *tagName;
  RDOM_Alist *attributes;
};

/* Implementation of a CharacterData node. */

struct _RDOM_CD {
  RDOM_N node;
  char *data;
};

/* Implementation of an Attr. Typically, attribute values will be
   represented as a Text child. This sucks as far as memory usage
   is concerned, but makes implementation of the full CORBA DOM
   simpler. */

struct _RDOM_A {
  RDOM_N node; /* could roll our own, which would save a few pointers */
  char *name;
  /* todo: specified */
};

/* Implementation of a Named Node Map. This is a simple linked list of
   Attr nodes. */

struct _RDOM_Alist {
  RDOM_A *attr;
  RDOM_Alist *next;
};

struct _RDOM_Pr {
  RDOM dom;
  RXML_Context *context;
};


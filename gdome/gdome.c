/* ----- gdome.c ----- */
#include "gdome.h"

void gdome_str_unref (GdomeDOMString *self)
{
  self->unref (self);
}

void
gdome_n_ref (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->ref (self, exc);
}

void
gdome_n_unref (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->unref (self, exc);
}

void *
gdome_n_query_interface (GdomeNode *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

GdomeDOMString *
gdome_n_nodeName (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->nodeName (self, exc);
}

GdomeDOMString *
gdome_n_nodeValue (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->nodeValue (self, exc);
}

void
gdome_n_set_nodeValue (GdomeNode *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_nodeValue (self, nodeValue, exc);
}

unsigned short
gdome_n_nodeType (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->nodeType (self, exc);
}

GdomeNode *
gdome_n_parentNode (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->parentNode (self, exc);
}

GdomeNodeList *
gdome_n_childNodes (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->childNodes (self, exc);
}

GdomeNode *
gdome_n_firstChild (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->firstChild (self, exc);
}

GdomeNode *
gdome_n_lastChild (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->lastChild (self, exc);
}

GdomeNode *
gdome_n_previousSibling (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->previousSibling (self, exc);
}

GdomeNode *
gdome_n_nextSibling (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->nextSibling (self, exc);
}

GdomeNamedNodeMap *
gdome_n_attributes (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->attributes (self, exc);
}

GdomeDocument *
gdome_n_ownerDocument (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->ownerDocument (self, exc);
}

GdomeNode *
gdome_n_insertBefore (GdomeNode *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->insertBefore (self, newChild, refChild, exc);
}

GdomeNode *
gdome_n_replaceChild (GdomeNode *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->replaceChild (self, newChild, oldChild, exc);
}

GdomeNode *
gdome_n_removeChild (GdomeNode *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->removeChild (self, oldChild, exc);
}

GdomeNode *
gdome_n_appendChild (GdomeNode *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->appendChild (self, newChild, exc);
}

GdomeBoolean
gdome_n_hasChildNodes (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->hasChildNodes (self, exc);
}

GdomeNode *
gdome_n_cloneNode (GdomeNode *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->cloneNode (self, deep, exc);
}

void
gdome_n_normalize (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->normalize (self, exc);
}

GdomeBoolean
gdome_n_supports (GdomeNode *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->supports (self, feature, version, exc);
}

GdomeDOMString *
gdome_n_namespaceURI (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->namespaceURI (self, exc);
}

GdomeDOMString *
gdome_n_prefix (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->prefix (self, exc);
}

void
gdome_n_set_prefix (GdomeNode *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_prefix (self, prefix, exc);
}

GdomeDOMString *
gdome_n_localName (GdomeNode *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->localName (self, exc);
}

void
gdome_n_addEventListener (GdomeNode *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->addEventListener (self, type, listener, useCapture, exc);
}

void
gdome_n_removeEventListener (GdomeNode *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->removeEventListener (self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_n_dispatchEvent (GdomeNode *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->dispatchEvent (self, evt, exc);
}

GdomeDOMString *
gdome_a_name (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->name (self, exc);
}

GdomeBoolean
gdome_a_specified (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->specified (self, exc);
}

GdomeDOMString *
gdome_a_value (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->value (self, exc);
}

void
gdome_a_set_value (GdomeAttr *self, GdomeDOMString *value, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_value (self, value, exc);
}

/* gen_invoker_super GdomeAttr a Attr: Node ()*/
void
gdome_a_ref (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_a_unref (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_a_query_interface (GdomeAttr *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_a_nodeName (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_a_nodeValue (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_a_set_nodeValue (GdomeAttr *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_a_nodeType (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_parentNode (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_a_childNodes (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_firstChild (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_lastChild (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_previousSibling (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_nextSibling (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_a_attributes (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_a_ownerDocument (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_insertBefore (GdomeAttr *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_a_replaceChild (GdomeAttr *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_a_removeChild (GdomeAttr *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_a_appendChild (GdomeAttr *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_a_hasChildNodes (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_a_cloneNode (GdomeAttr *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_a_normalize (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_a_supports (GdomeAttr *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_a_namespaceURI (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_a_prefix (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_a_set_prefix (GdomeAttr *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_a_localName (GdomeAttr *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_a_addEventListener (GdomeAttr *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_a_removeEventListener (GdomeAttr *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_a_dispatchEvent (GdomeAttr *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

GdomeDOMString *
gdome_cd_data (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->data (self, exc);
}

void
gdome_cd_set_data (GdomeCharacterData *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_data (self, data, exc);
}

unsigned long
gdome_cd_length (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->length (self, exc);
}

GdomeDOMString *
gdome_cd_substringData (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->substringData (self, offset, count, exc);
}

void
gdome_cd_appendData (GdomeCharacterData *self, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->appendData (self, arg, exc);
}

void
gdome_cd_insertData (GdomeCharacterData *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->insertData (self, offset, arg, exc);
}

void
gdome_cd_deleteData (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  self->vtab->deleteData (self, offset, count, exc);
}

void
gdome_cd_replaceData (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->replaceData (self, offset, count, arg, exc);
}

/* gen_invoker_super GdomeCharacterData cd CharacterData: Node ()*/
void
gdome_cd_ref (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_cd_unref (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_cd_query_interface (GdomeCharacterData *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_cd_nodeName (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_cd_nodeValue (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_cd_set_nodeValue (GdomeCharacterData *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_cd_nodeType (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_parentNode (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_cd_childNodes (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_firstChild (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_lastChild (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_previousSibling (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_nextSibling (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_cd_attributes (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_cd_ownerDocument (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_insertBefore (GdomeCharacterData *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_cd_replaceChild (GdomeCharacterData *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_cd_removeChild (GdomeCharacterData *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_cd_appendChild (GdomeCharacterData *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_cd_hasChildNodes (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cd_cloneNode (GdomeCharacterData *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_cd_normalize (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_cd_supports (GdomeCharacterData *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_cd_namespaceURI (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_cd_prefix (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_cd_set_prefix (GdomeCharacterData *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_cd_localName (GdomeCharacterData *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_cd_addEventListener (GdomeCharacterData *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_cd_removeEventListener (GdomeCharacterData *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_cd_dispatchEvent (GdomeCharacterData *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

GdomeText *
gdome_t_splitText (GdomeText *self, unsigned long offset, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->splitText (self, offset, exc);
}

/* gen_invoker_super GdomeText t Text: CharacterData ()*/
GdomeDOMString *
gdome_t_data (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.data ((GdomeCharacterData *)self, exc);
}

void
gdome_t_set_data (GdomeText *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_data ((GdomeCharacterData *)self, data, exc);
}

unsigned long
gdome_t_length (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.length ((GdomeCharacterData *)self, exc);
}

GdomeDOMString *
gdome_t_substringData (GdomeText *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.substringData ((GdomeCharacterData *)self, offset, count, exc);
}

void
gdome_t_appendData (GdomeText *self, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.appendData ((GdomeCharacterData *)self, arg, exc);
}

void
gdome_t_insertData (GdomeText *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.insertData ((GdomeCharacterData *)self, offset, arg, exc);
}

void
gdome_t_deleteData (GdomeText *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.deleteData ((GdomeCharacterData *)self, offset, count, exc);
}

void
gdome_t_replaceData (GdomeText *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.replaceData ((GdomeCharacterData *)self, offset, count, arg, exc);
}

/* gen_invoker_super GdomeText t CharacterData: Node (super.)*/
void
gdome_t_ref (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.ref ((GdomeNode *)self, exc);
}

void
gdome_t_unref (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.unref ((GdomeNode *)self, exc);
}

void *
gdome_t_query_interface (GdomeText *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_t_nodeName (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_t_nodeValue (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_t_set_nodeValue (GdomeText *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_t_nodeType (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_parentNode (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_t_childNodes (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_firstChild (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_lastChild (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_previousSibling (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_nextSibling (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_t_attributes (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_t_ownerDocument (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_insertBefore (GdomeText *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_t_replaceChild (GdomeText *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_t_removeChild (GdomeText *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_t_appendChild (GdomeText *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_t_hasChildNodes (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_t_cloneNode (GdomeText *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_t_normalize (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_t_supports (GdomeText *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_t_namespaceURI (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_t_prefix (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.prefix ((GdomeNode *)self, exc);
}

void
gdome_t_set_prefix (GdomeText *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_t_localName (GdomeText *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.localName ((GdomeNode *)self, exc);
}

void
gdome_t_addEventListener (GdomeText *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_t_removeEventListener (GdomeText *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_t_dispatchEvent (GdomeText *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

/* gen_invoker_super GdomeCDATASection cds CDATASection: Text ()*/
GdomeText *
gdome_cds_splitText (GdomeCDATASection *self, unsigned long offset, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.splitText ((GdomeText *)self, offset, exc);
}

/* gen_invoker_super GdomeCDATASection cds Text: CharacterData (super.)*/
GdomeDOMString *
gdome_cds_data (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.data ((GdomeCharacterData *)self, exc);
}

void
gdome_cds_set_data (GdomeCDATASection *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_data ((GdomeCharacterData *)self, data, exc);
}

unsigned long
gdome_cds_length (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.length ((GdomeCharacterData *)self, exc);
}

GdomeDOMString *
gdome_cds_substringData (GdomeCDATASection *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.substringData ((GdomeCharacterData *)self, offset, count, exc);
}

void
gdome_cds_appendData (GdomeCDATASection *self, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.appendData ((GdomeCharacterData *)self, arg, exc);
}

void
gdome_cds_insertData (GdomeCDATASection *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.insertData ((GdomeCharacterData *)self, offset, arg, exc);
}

void
gdome_cds_deleteData (GdomeCDATASection *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.deleteData ((GdomeCharacterData *)self, offset, count, exc);
}

void
gdome_cds_replaceData (GdomeCDATASection *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.replaceData ((GdomeCharacterData *)self, offset, count, arg, exc);
}

/* gen_invoker_super GdomeCDATASection cds CharacterData: Node (super.super.)*/
void
gdome_cds_ref (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.ref ((GdomeNode *)self, exc);
}

void
gdome_cds_unref (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.unref ((GdomeNode *)self, exc);
}

void *
gdome_cds_query_interface (GdomeCDATASection *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_cds_nodeName (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_cds_nodeValue (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_cds_set_nodeValue (GdomeCDATASection *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_cds_nodeType (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_parentNode (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_cds_childNodes (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_firstChild (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_lastChild (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_previousSibling (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_nextSibling (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_cds_attributes (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_cds_ownerDocument (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_insertBefore (GdomeCDATASection *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_cds_replaceChild (GdomeCDATASection *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_cds_removeChild (GdomeCDATASection *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_cds_appendChild (GdomeCDATASection *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_cds_hasChildNodes (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_cds_cloneNode (GdomeCDATASection *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_cds_normalize (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_cds_supports (GdomeCDATASection *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_cds_namespaceURI (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_cds_prefix (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.prefix ((GdomeNode *)self, exc);
}

void
gdome_cds_set_prefix (GdomeCDATASection *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_cds_localName (GdomeCDATASection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.localName ((GdomeNode *)self, exc);
}

void
gdome_cds_addEventListener (GdomeCDATASection *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_cds_removeEventListener (GdomeCDATASection *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_cds_dispatchEvent (GdomeCDATASection *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

/* gen_invoker_super GdomeComment c Comment: CharacterData ()*/
GdomeDOMString *
gdome_c_data (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.data ((GdomeCharacterData *)self, exc);
}

void
gdome_c_set_data (GdomeComment *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_data ((GdomeCharacterData *)self, data, exc);
}

unsigned long
gdome_c_length (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.length ((GdomeCharacterData *)self, exc);
}

GdomeDOMString *
gdome_c_substringData (GdomeComment *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.substringData ((GdomeCharacterData *)self, offset, count, exc);
}

void
gdome_c_appendData (GdomeComment *self, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.appendData ((GdomeCharacterData *)self, arg, exc);
}

void
gdome_c_insertData (GdomeComment *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.insertData ((GdomeCharacterData *)self, offset, arg, exc);
}

void
gdome_c_deleteData (GdomeComment *self, unsigned long offset, unsigned long count, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.deleteData ((GdomeCharacterData *)self, offset, count, exc);
}

void
gdome_c_replaceData (GdomeComment *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.replaceData ((GdomeCharacterData *)self, offset, count, arg, exc);
}

/* gen_invoker_super GdomeComment c CharacterData: Node (super.)*/
void
gdome_c_ref (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.ref ((GdomeNode *)self, exc);
}

void
gdome_c_unref (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.unref ((GdomeNode *)self, exc);
}

void *
gdome_c_query_interface (GdomeComment *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_c_nodeName (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_c_nodeValue (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_c_set_nodeValue (GdomeComment *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_c_nodeType (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_parentNode (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_c_childNodes (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_firstChild (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_lastChild (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_previousSibling (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_nextSibling (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_c_attributes (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_c_ownerDocument (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_insertBefore (GdomeComment *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_c_replaceChild (GdomeComment *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_c_removeChild (GdomeComment *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_c_appendChild (GdomeComment *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_c_hasChildNodes (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_c_cloneNode (GdomeComment *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_c_normalize (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_c_supports (GdomeComment *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_c_namespaceURI (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_c_prefix (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.prefix ((GdomeNode *)self, exc);
}

void
gdome_c_set_prefix (GdomeComment *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_c_localName (GdomeComment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.localName ((GdomeNode *)self, exc);
}

void
gdome_c_addEventListener (GdomeComment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_c_removeEventListener (GdomeComment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_c_dispatchEvent (GdomeComment *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

void
gdome_DOMImplementation_ref (GdomeDOMImplementation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->ref (self, exc);
}

void
gdome_DOMImplementation_unref (GdomeDOMImplementation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->unref (self, exc);
}

void *
gdome_DOMImplementation_query_interface (GdomeDOMImplementation *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

GdomeBoolean
gdome_DOMImplementation_hasFeature (GdomeDOMImplementation *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->hasFeature (self, feature, version, exc);
}

GdomeDocumentType *
gdome_DOMImplementation_createDocumentType (GdomeDOMImplementation *self, GdomeDOMString *qualifiedName, GdomeDOMString *publicId, GdomeDOMString *systemId, GdomeDOMString *internalSubset, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createDocumentType (self, qualifiedName, publicId, systemId, internalSubset, exc);
}

GdomeDocument *
gdome_DOMImplementation_createDocument (GdomeDOMImplementation *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeDocumentType *doctype, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createDocument (self, namespaceURI, qualifiedName, doctype, exc);
}

GdomeDocumentType *
gdome_doc_doctype (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->doctype (self, exc);
}

GdomeDOMImplementation *
gdome_doc_implementation (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->implementation (self, exc);
}

GdomeElement *
gdome_doc_documentElement (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->documentElement (self, exc);
}

GdomeElement *
gdome_doc_createElement (GdomeDocument *self, GdomeDOMString *tagName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createElement (self, tagName, exc);
}

GdomeDocumentFragment *
gdome_doc_createDocumentFragment (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createDocumentFragment (self, exc);
}

GdomeText *
gdome_doc_createTextNode (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createTextNode (self, data, exc);
}

GdomeComment *
gdome_doc_createComment (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createComment (self, data, exc);
}

GdomeCDATASection *
gdome_doc_createCDATASection (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createCDATASection (self, data, exc);
}

GdomeProcessingInstruction *
gdome_doc_createProcessingInstruction (GdomeDocument *self, GdomeDOMString *target, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createProcessingInstruction (self, target, data, exc);
}

GdomeAttr *
gdome_doc_createAttribute (GdomeDocument *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createAttribute (self, name, exc);
}

GdomeEntityReference *
gdome_doc_createEntityReference (GdomeDocument *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createEntityReference (self, name, exc);
}

GdomeNodeList *
gdome_doc_getElementsByTagName (GdomeDocument *self, GdomeDOMString *tagname, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getElementsByTagName (self, tagname, exc);
}

GdomeNode *
gdome_doc_importNode (GdomeDocument *self, GdomeNode *importedNode, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->importNode (self, importedNode, deep, exc);
}

GdomeElement *
gdome_doc_createElementNS (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createElementNS (self, namespaceURI, qualifiedName, exc);
}

GdomeAttr *
gdome_doc_createAttrNS (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createAttrNS (self, namespaceURI, qualifiedName, exc);
}

GdomeNodeList *
gdome_doc_getElementsByTagNameNS (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getElementsByTagNameNS (self, namespaceURI, qualifiedName, exc);
}

GdomeElement *
gdome_doc_getElementById (GdomeDocument *self, GdomeDOMString *elementId, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getElementById (self, elementId, exc);
}

/* gen_invoker_super GdomeDocument doc Document: Node ()*/
void
gdome_doc_ref (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_doc_unref (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_doc_query_interface (GdomeDocument *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_doc_nodeName (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_doc_nodeValue (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_doc_set_nodeValue (GdomeDocument *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_doc_nodeType (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_parentNode (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_doc_childNodes (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_firstChild (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_lastChild (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_previousSibling (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_nextSibling (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_doc_attributes (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_doc_ownerDocument (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_insertBefore (GdomeDocument *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_doc_replaceChild (GdomeDocument *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_doc_removeChild (GdomeDocument *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_doc_appendChild (GdomeDocument *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_doc_hasChildNodes (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_doc_cloneNode (GdomeDocument *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_doc_normalize (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_doc_supports (GdomeDocument *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_doc_namespaceURI (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_doc_prefix (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_doc_set_prefix (GdomeDocument *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_doc_localName (GdomeDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_doc_addEventListener (GdomeDocument *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_doc_removeEventListener (GdomeDocument *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_doc_dispatchEvent (GdomeDocument *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

/* gen_invoker_super GdomeDocumentFragment df DocumentFragment: Node ()*/
void
gdome_df_ref (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_df_unref (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_df_query_interface (GdomeDocumentFragment *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_df_nodeName (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_df_nodeValue (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_df_set_nodeValue (GdomeDocumentFragment *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_df_nodeType (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_parentNode (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_df_childNodes (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_firstChild (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_lastChild (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_previousSibling (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_nextSibling (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_df_attributes (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_df_ownerDocument (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_insertBefore (GdomeDocumentFragment *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_df_replaceChild (GdomeDocumentFragment *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_df_removeChild (GdomeDocumentFragment *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_df_appendChild (GdomeDocumentFragment *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_df_hasChildNodes (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_df_cloneNode (GdomeDocumentFragment *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_df_normalize (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_df_supports (GdomeDocumentFragment *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_df_namespaceURI (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_df_prefix (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_df_set_prefix (GdomeDocumentFragment *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_df_localName (GdomeDocumentFragment *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_df_addEventListener (GdomeDocumentFragment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_df_removeEventListener (GdomeDocumentFragment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_df_dispatchEvent (GdomeDocumentFragment *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

GdomeDOMString *
gdome_dt_name (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->name (self, exc);
}

GdomeNamedNodeMap *
gdome_dt_entities (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->entities (self, exc);
}

GdomeNamedNodeMap *
gdome_dt_notations (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->notations (self, exc);
}

GdomeDOMString *
gdome_dt_publicId (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->publicId (self, exc);
}

GdomeDOMString *
gdome_dt_systemId (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->systemId (self, exc);
}

GdomeDOMString *
gdome_dt_internalSubset (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->internalSubset (self, exc);
}

/* gen_invoker_super GdomeDocumentType dt DocumentType: Node ()*/
void
gdome_dt_ref (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_dt_unref (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_dt_query_interface (GdomeDocumentType *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_dt_nodeName (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_dt_nodeValue (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_dt_set_nodeValue (GdomeDocumentType *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_dt_nodeType (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_parentNode (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_dt_childNodes (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_firstChild (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_lastChild (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_previousSibling (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_nextSibling (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_dt_attributes (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_dt_ownerDocument (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_insertBefore (GdomeDocumentType *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_dt_replaceChild (GdomeDocumentType *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_dt_removeChild (GdomeDocumentType *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_dt_appendChild (GdomeDocumentType *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_dt_hasChildNodes (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_dt_cloneNode (GdomeDocumentType *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_dt_normalize (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_dt_supports (GdomeDocumentType *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_dt_namespaceURI (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_dt_prefix (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_dt_set_prefix (GdomeDocumentType *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_dt_localName (GdomeDocumentType *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_dt_addEventListener (GdomeDocumentType *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_dt_removeEventListener (GdomeDocumentType *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_dt_dispatchEvent (GdomeDocumentType *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

GdomeDOMString *
gdome_el_tagName (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->tagName (self, exc);
}

GdomeDOMString *
gdome_el_getAttribute (GdomeElement *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getAttribute (self, name, exc);
}

void
gdome_el_setAttribute (GdomeElement *self, GdomeDOMString *name, GdomeDOMString *value, GdomeException *exc)
{
  *exc = 0;
  self->vtab->setAttribute (self, name, value, exc);
}

void
gdome_el_removeAttribute (GdomeElement *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  self->vtab->removeAttribute (self, name, exc);
}

GdomeAttr *
gdome_el_getAttributeNode (GdomeElement *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getAttributeNode (self, name, exc);
}

GdomeAttr *
gdome_el_setAttributeNode (GdomeElement *self, GdomeAttr *newAttr, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->setAttributeNode (self, newAttr, exc);
}

GdomeAttr *
gdome_el_removeAttributeNode (GdomeElement *self, GdomeAttr *oldAttr, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->removeAttributeNode (self, oldAttr, exc);
}

GdomeNodeList *
gdome_el_getElementsByTagName (GdomeElement *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getElementsByTagName (self, name, exc);
}

GdomeDOMString *
gdome_el_getAttributeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getAttributeNS (self, namespaceURI, localName, exc);
}

void
gdome_el_setAttributeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeDOMString *value, GdomeException *exc)
{
  *exc = 0;
  self->vtab->setAttributeNS (self, namespaceURI, localName, value, exc);
}

void
gdome_el_removeAttributeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc)
{
  *exc = 0;
  self->vtab->removeAttributeNS (self, namespaceURI, localName, exc);
}

GdomeAttr *
gdome_el_getAttributeNodeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getAttributeNodeNS (self, namespaceURI, localName, exc);
}

GdomeAttr *
gdome_el_setAttributeNodeNS (GdomeElement *self, GdomeAttr *newAttr, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->setAttributeNodeNS (self, newAttr, exc);
}

GdomeNodeList *
gdome_el_getElementsByTagNameNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getElementsByTagNameNS (self, namespaceURI, localName, exc);
}

/* gen_invoker_super GdomeElement el Element: Node ()*/
void
gdome_el_ref (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_el_unref (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_el_query_interface (GdomeElement *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_el_nodeName (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_el_nodeValue (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_el_set_nodeValue (GdomeElement *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_el_nodeType (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_parentNode (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_el_childNodes (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_firstChild (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_lastChild (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_previousSibling (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_nextSibling (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_el_attributes (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_el_ownerDocument (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_insertBefore (GdomeElement *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_el_replaceChild (GdomeElement *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_el_removeChild (GdomeElement *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_el_appendChild (GdomeElement *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_el_hasChildNodes (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_el_cloneNode (GdomeElement *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_el_normalize (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_el_supports (GdomeElement *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_el_namespaceURI (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_el_prefix (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_el_set_prefix (GdomeElement *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_el_localName (GdomeElement *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_el_addEventListener (GdomeElement *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_el_removeEventListener (GdomeElement *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_el_dispatchEvent (GdomeElement *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

GdomeDOMString *
gdome_ent_publicId (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->publicId (self, exc);
}

GdomeDOMString *
gdome_ent_systemId (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->systemId (self, exc);
}

GdomeDOMString *
gdome_ent_notationName (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->notationName (self, exc);
}

/* gen_invoker_super GdomeEntity ent Entity: Node ()*/
void
gdome_ent_ref (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_ent_unref (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_ent_query_interface (GdomeEntity *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_ent_nodeName (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_ent_nodeValue (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_ent_set_nodeValue (GdomeEntity *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_ent_nodeType (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_parentNode (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_ent_childNodes (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_firstChild (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_lastChild (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_previousSibling (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_nextSibling (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_ent_attributes (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_ent_ownerDocument (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_insertBefore (GdomeEntity *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_ent_replaceChild (GdomeEntity *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_ent_removeChild (GdomeEntity *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_ent_appendChild (GdomeEntity *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_ent_hasChildNodes (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_ent_cloneNode (GdomeEntity *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_ent_normalize (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_ent_supports (GdomeEntity *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_ent_namespaceURI (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_ent_prefix (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_ent_set_prefix (GdomeEntity *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_ent_localName (GdomeEntity *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_ent_addEventListener (GdomeEntity *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_ent_removeEventListener (GdomeEntity *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_ent_dispatchEvent (GdomeEntity *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

/* gen_invoker_super GdomeEntityReference er EntityReference: Node ()*/
void
gdome_er_ref (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_er_unref (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_er_query_interface (GdomeEntityReference *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_er_nodeName (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_er_nodeValue (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_er_set_nodeValue (GdomeEntityReference *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_er_nodeType (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_parentNode (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_er_childNodes (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_firstChild (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_lastChild (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_previousSibling (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_nextSibling (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_er_attributes (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_er_ownerDocument (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_insertBefore (GdomeEntityReference *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_er_replaceChild (GdomeEntityReference *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_er_removeChild (GdomeEntityReference *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_er_appendChild (GdomeEntityReference *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_er_hasChildNodes (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_er_cloneNode (GdomeEntityReference *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_er_normalize (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_er_supports (GdomeEntityReference *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_er_namespaceURI (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_er_prefix (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_er_set_prefix (GdomeEntityReference *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_er_localName (GdomeEntityReference *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_er_addEventListener (GdomeEntityReference *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_er_removeEventListener (GdomeEntityReference *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_er_dispatchEvent (GdomeEntityReference *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

void
gdome_evnt_ref (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->ref (self, exc);
}

void
gdome_evnt_unref (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->unref (self, exc);
}

void *
gdome_evnt_query_interface (GdomeEvent *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

GdomeDOMString *
gdome_evnt_type (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->type (self, exc);
}

GdomeNode *
gdome_evnt_target (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->target (self, exc);
}

GdomeNode *
gdome_evnt_currentNode (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->currentNode (self, exc);
}

unsigned short
gdome_evnt_eventPhase (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->eventPhase (self, exc);
}

GdomeBoolean
gdome_evnt_bubbles (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->bubbles (self, exc);
}

GdomeBoolean
gdome_evnt_cancelable (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->cancelable (self, exc);
}

void
gdome_evnt_stopPropagation (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->stopPropagation (self, exc);
}

void
gdome_evnt_preventDefault (GdomeEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->preventDefault (self, exc);
}

void
gdome_evnt_initEvent (GdomeEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->initEvent (self, eventTypeArg, canBubbleArg, cancelableArg, exc);
}

void
gdome_evntl_ref (GdomeEventListener *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->ref (self, exc);
}

void
gdome_evntl_unref (GdomeEventListener *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->unref (self, exc);
}

void *
gdome_evntl_query_interface (GdomeEventListener *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

void
gdome_evntl_handleEvent (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc)
{
  *exc = 0;
  self->vtab->handleEvent (self, event, exc);
}

GdomeNode *
gdome_mevnt_relatedNode (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->relatedNode (self, exc);
}

GdomeDOMString *
gdome_mevnt_prevValue (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->prevValue (self, exc);
}

GdomeDOMString *
gdome_mevnt_newValue (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->newValue (self, exc);
}

GdomeDOMString *
gdome_mevnt_attrName (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->attrName (self, exc);
}

void
gdome_mevnt_initMutationEvent (GdomeMutationEvent *self, GdomeDOMString *typeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeNode *relatedNodeArg, GdomeDOMString *prevValueArg, GdomeDOMString *newValueArg, GdomeDOMString *attrNameArg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->initMutationEvent (self, typeArg, canBubbleArg, cancelableArg, relatedNodeArg, prevValueArg, newValueArg, attrNameArg, exc);
}

/* gen_invoker_super GdomeMutationEvent mevnt MutationEvent: Event ()*/
void
gdome_mevnt_ref (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeEvent *)self, exc);
}

void
gdome_mevnt_unref (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeEvent *)self, exc);
}

void *
gdome_mevnt_query_interface (GdomeMutationEvent *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeEvent *)self, interface, exc);
}

GdomeDOMString *
gdome_mevnt_type (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.type ((GdomeEvent *)self, exc);
}

GdomeNode *
gdome_mevnt_target (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.target ((GdomeEvent *)self, exc);
}

GdomeNode *
gdome_mevnt_currentNode (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.currentNode ((GdomeEvent *)self, exc);
}

unsigned short
gdome_mevnt_eventPhase (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.eventPhase ((GdomeEvent *)self, exc);
}

GdomeBoolean
gdome_mevnt_bubbles (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.bubbles ((GdomeEvent *)self, exc);
}

GdomeBoolean
gdome_mevnt_cancelable (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cancelable ((GdomeEvent *)self, exc);
}

void
gdome_mevnt_stopPropagation (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.stopPropagation ((GdomeEvent *)self, exc);
}

void
gdome_mevnt_preventDefault (GdomeMutationEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.preventDefault ((GdomeEvent *)self, exc);
}

void
gdome_mevnt_initEvent (GdomeMutationEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.initEvent ((GdomeEvent *)self, eventTypeArg, canBubbleArg, cancelableArg, exc);
}

void
gdome_nnm_ref (GdomeNamedNodeMap *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->ref (self, exc);
}

void
gdome_nnm_unref (GdomeNamedNodeMap *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->unref (self, exc);
}

void *
gdome_nnm_query_interface (GdomeNamedNodeMap *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

GdomeNode *
gdome_nnm_getNamedItem (GdomeNamedNodeMap *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getNamedItem (self, name, exc);
}

GdomeNode *
gdome_nnm_setNamedItem (GdomeNamedNodeMap *self, GdomeNode *arg, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->setNamedItem (self, arg, exc);
}

GdomeNode *
gdome_nnm_removeNamedItem (GdomeNamedNodeMap *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->removeNamedItem (self, name, exc);
}

GdomeNode *
gdome_nnm_item (GdomeNamedNodeMap *self, unsigned long index, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->item (self, index, exc);
}

unsigned long
gdome_nnm_length (GdomeNamedNodeMap *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->length (self, exc);
}

void
gdome_nl_ref (GdomeNodeList *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->ref (self, exc);
}

void
gdome_nl_unref (GdomeNodeList *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->unref (self, exc);
}

void *
gdome_nl_query_interface (GdomeNodeList *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

GdomeNode *
gdome_nl_item (GdomeNodeList *self, unsigned long index, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->item (self, index, exc);
}

unsigned long
gdome_nl_length (GdomeNodeList *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->length (self, exc);
}

GdomeDOMString *
gdome_not_publicId (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->publicId (self, exc);
}

GdomeDOMString *
gdome_not_systemId (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->systemId (self, exc);
}

/* gen_invoker_super GdomeNotation not Notation: Node ()*/
void
gdome_not_ref (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_not_unref (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_not_query_interface (GdomeNotation *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_not_nodeName (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_not_nodeValue (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_not_set_nodeValue (GdomeNotation *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_not_nodeType (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_parentNode (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_not_childNodes (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_firstChild (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_lastChild (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_previousSibling (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_nextSibling (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_not_attributes (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_not_ownerDocument (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_insertBefore (GdomeNotation *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_not_replaceChild (GdomeNotation *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_not_removeChild (GdomeNotation *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_not_appendChild (GdomeNotation *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_not_hasChildNodes (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_not_cloneNode (GdomeNotation *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_not_normalize (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_not_supports (GdomeNotation *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_not_namespaceURI (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_not_prefix (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_not_set_prefix (GdomeNotation *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_not_localName (GdomeNotation *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_not_addEventListener (GdomeNotation *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_not_removeEventListener (GdomeNotation *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_not_dispatchEvent (GdomeNotation *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

GdomeDOMString *
gdome_pi_target (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->target (self, exc);
}

GdomeDOMString *
gdome_pi_data (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->data (self, exc);
}

void
gdome_pi_set_data (GdomeProcessingInstruction *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_data (self, data, exc);
}

/* gen_invoker_super GdomeProcessingInstruction pi ProcessingInstruction: Node ()*/
void
gdome_pi_ref (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeNode *)self, exc);
}

void
gdome_pi_unref (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeNode *)self, exc);
}

void *
gdome_pi_query_interface (GdomeProcessingInstruction *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_pi_nodeName (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_pi_nodeValue (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_pi_set_nodeValue (GdomeProcessingInstruction *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_pi_nodeType (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_parentNode (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_pi_childNodes (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_firstChild (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_lastChild (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_previousSibling (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_nextSibling (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_pi_attributes (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_pi_ownerDocument (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_insertBefore (GdomeProcessingInstruction *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_pi_replaceChild (GdomeProcessingInstruction *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_pi_removeChild (GdomeProcessingInstruction *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_pi_appendChild (GdomeProcessingInstruction *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_pi_hasChildNodes (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_pi_cloneNode (GdomeProcessingInstruction *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_pi_normalize (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_pi_supports (GdomeProcessingInstruction *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.supports ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_pi_namespaceURI (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_pi_prefix (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.prefix ((GdomeNode *)self, exc);
}

void
gdome_pi_set_prefix (GdomeProcessingInstruction *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_pi_localName (GdomeProcessingInstruction *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.localName ((GdomeNode *)self, exc);
}

void
gdome_pi_addEventListener (GdomeProcessingInstruction *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_pi_removeEventListener (GdomeProcessingInstruction *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_pi_dispatchEvent (GdomeProcessingInstruction *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

long
gdome_uievnt_screenX (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->screenX (self, exc);
}

void
gdome_uievnt_set_screenX (GdomeUIEvent *self, long screenX, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_screenX (self, screenX, exc);
}

long
gdome_uievnt_screenY (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->screenY (self, exc);
}

void
gdome_uievnt_set_screenY (GdomeUIEvent *self, long screenY, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_screenY (self, screenY, exc);
}

long
gdome_uievnt_clientX (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->clientX (self, exc);
}

void
gdome_uievnt_set_clientX (GdomeUIEvent *self, long clientX, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_clientX (self, clientX, exc);
}

long
gdome_uievnt_clientY (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->clientY (self, exc);
}

void
gdome_uievnt_set_clientY (GdomeUIEvent *self, long clientY, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_clientY (self, clientY, exc);
}

GdomeBoolean
gdome_uievnt_altKey (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->altKey (self, exc);
}

void
gdome_uievnt_set_altKey (GdomeUIEvent *self, GdomeBoolean altKey, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_altKey (self, altKey, exc);
}

GdomeBoolean
gdome_uievnt_ctrlKey (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->ctrlKey (self, exc);
}

void
gdome_uievnt_set_ctrlKey (GdomeUIEvent *self, GdomeBoolean ctrlKey, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_ctrlKey (self, ctrlKey, exc);
}

GdomeBoolean
gdome_uievnt_shiftKey (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->shiftKey (self, exc);
}

void
gdome_uievnt_set_shiftKey (GdomeUIEvent *self, GdomeBoolean shiftKey, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_shiftKey (self, shiftKey, exc);
}

unsigned long
gdome_uievnt_keyCode (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->keyCode (self, exc);
}

void
gdome_uievnt_set_keyCode (GdomeUIEvent *self, unsigned long keyCode, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_keyCode (self, keyCode, exc);
}

unsigned long
gdome_uievnt_charCode (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->charCode (self, exc);
}

void
gdome_uievnt_set_charCode (GdomeUIEvent *self, unsigned long charCode, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_charCode (self, charCode, exc);
}

unsigned short
gdome_uievnt_button (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->button (self, exc);
}

void
gdome_uievnt_set_button (GdomeUIEvent *self, unsigned short button, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_button (self, button, exc);
}

/* gen_invoker_super GdomeUIEvent uievnt UIEvent: Event ()*/
void
gdome_uievnt_ref (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeEvent *)self, exc);
}

void
gdome_uievnt_unref (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeEvent *)self, exc);
}

void *
gdome_uievnt_query_interface (GdomeUIEvent *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeEvent *)self, interface, exc);
}

GdomeDOMString *
gdome_uievnt_type (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.type ((GdomeEvent *)self, exc);
}

GdomeNode *
gdome_uievnt_target (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.target ((GdomeEvent *)self, exc);
}

GdomeNode *
gdome_uievnt_currentNode (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.currentNode ((GdomeEvent *)self, exc);
}

unsigned short
gdome_uievnt_eventPhase (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.eventPhase ((GdomeEvent *)self, exc);
}

GdomeBoolean
gdome_uievnt_bubbles (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.bubbles ((GdomeEvent *)self, exc);
}

GdomeBoolean
gdome_uievnt_cancelable (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.cancelable ((GdomeEvent *)self, exc);
}

void
gdome_uievnt_stopPropagation (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.stopPropagation ((GdomeEvent *)self, exc);
}

void
gdome_uievnt_preventDefault (GdomeUIEvent *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.preventDefault ((GdomeEvent *)self, exc);
}

void
gdome_uievnt_initEvent (GdomeUIEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.initEvent ((GdomeEvent *)self, eventTypeArg, canBubbleArg, cancelableArg, exc);
}


/* Raph's XML parser.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

#include <stdlib.h>
#include <string.h>
#include "rxml.h"
#include "rxml-private.h"
#include "rxml-encodings.h"

static void *
RXML_context_default_alloc (RXML_Context *self, int n_bytes)
{
  return malloc (n_bytes);
  /* todo: check for NULL */
}

static void
RXML_context_default_free (RXML_Context *self, void *p)
{
  free (p);
}

static void
RXML_context_default_free_context (RXML_Context *self)
{
  free (self);
}

RXML_Context *
RXML_context_default (void)
{
  RXML_Context *context;

  context = (RXML_Context *)malloc (sizeof(RXML_Context));
  context->alloc = RXML_context_default_alloc;
  context->free = RXML_context_default_free;
  context->free_context = RXML_context_default_free_context;

  return context;
}

void *
RXML_expand_data (RXML_Context *context, void *p, int size)
{
  void *new;

  new = context->alloc (context, size);
  memcpy (new, p, size >> 1);
  RXML_free (context, p);
  return new;
}

typedef struct _RXML_ByteWriterXP RXML_ByteWriterXP;
typedef struct _RXML_Entities RXML_Entities;
typedef struct _RXML_Entity RXML_Entity;

struct _RXML_ByteWriterXP {
  RXML_ByteWriter writer;

  RXML_Context *context;

  RXML_XbitWriter *xw;

  char *buf;
  int buf_size;
  int rd_ix, wr_ix;

  int pos;
  int line_no;

  RXML_Tag *tag_stack;
  int tag_sp, tag_sp_max;

  RXML_Entities *entities;
};

struct _RXML_Entities {
  int n_entities;
  RXML_Entity *entities;
};

struct _RXML_Entity {
  char *name;
  char *replacement;
};

static const RXML_Entity RXML_def_ent[5] = {
  {"amp", "&#38;"},
  {"lt", "&#60;"},
  {"gt", ">"},
  {"apos", "'"},
  {"quot", "\""}
};

static const RXML_Entities RXML_default_entities =
{ 5,
  (RXML_Entity *)&RXML_def_ent,
};

/* Entity reference is given as "&amp;". Return value is replacement
   text or NULL.

   Precondition: first character of entref is &, last is ;

   XML 1.0 [68] EntityRef
*/
static char *
RXML_lookup_entity (const RXML_Entities *entities, const char *entref)
{
  int i, j;
  int n_entities = entities->n_entities;
  char first = entref[1];

  for (i = 0; i < n_entities; i++)
    {
      char *name = entities->entities[i].name;
      if (name[0] == first)
	{
	  char c;
	  for (j = 1; (c = entref[j + 1]) != ';'; j++)
	    if (c != name[j])
	      break;
	  if (c == ';' && name[j] == '\0')
	    return entities->entities[i].replacement;
	}
    }
  return NULL;
}

/* Entity reference is given as "&#60;" or "&#x3c;". Places UTF-8 encoding
   of character referenced in buf, returns length of UTF-8 string.

   Returns 0 in case of error.

   XML 1.0 [66] CharRef */

static int
RXML_parse_charref (char *buf, const char *charref)
{
  int ch = 0;
  int i;
  char c;

  c = charref[2];
  if (c == 'x')
    {
      for (i = 3; (c = charref[i]) != ';'; i++)
	{
	  if (c >= '0' && c <= '9')
	    ch = (ch << 4) + c - '0';
	  else if (c >= 'a' && c <= 'f')
	    ch = (ch << 4) + c + 10 - 'a';
	  else if (c >= 'A' && c <= 'F')
	    ch = (ch << 4) + c + 10 - 'A';
	  else
	    return 0;
	}
    }
  else
    {
      for (i = 2; (c = charref[i]) != ';'; i++)
	{
	  if (c >= '0' && c <= '9')
	    ch = (ch * 10) + c - '0';
	  else
	    return 0;
	}
    }

  /* XML 1.0 [2] Char, as WFC on [66] CharRef */
  if ((ch < 0x20 && !(ch == 0x9 || ch == 0xA || ch == 0xD)) ||
      (ch > 0xD800 && (ch < 0xE000 || ch > 0x10FFFF)))
    return 0;

  /* ok, have parsed numeric chacter value, store in UTF-8 */
  if (ch < 0x80)
    {
      buf[0] = ch;
      return 1;
    }
  else if (ch < 0x800)
    {
      buf[0] = 0xc0 | (ch >> 6);
      buf[1] = 0x80 | (ch & 0x3f);
      return 2;
    }
  else if (ch < 0x10000)
    {
      buf[0] = 0xe0 | (ch >> 12);
      buf[1] = 0x80 | ((ch >> 6) & 0x3f);
      buf[2] = 0x80 | (ch & 0x3f);
      return 3;
    }
  else if (ch < 0x200000)
    {
      buf[0] = 0xf0 | (ch >> 18);
      buf[1] = 0x80 | ((ch >> 12) & 0x3f);
      buf[2] = 0x80 | ((ch >> 6) & 0x3f);
      buf[3] = 0x80 | (ch & 0x3f);
      return 4;
    }
  else if (ch < 0x4000000)
    {
      buf[0] = 0xf8 | (ch >> 24);
      buf[1] = 0x80 | ((ch >> 18) & 0x3f);
      buf[2] = 0x80 | ((ch >> 12) & 0x3f);
      buf[3] = 0x80 | ((ch >> 6) & 0x3f);
      buf[4] = 0x80 | (ch & 0x3f);
      return 5;
    }
  else if (ch < 0x80000000)
    {
      buf[0] = 0xfc | (ch >> 30);
      buf[1] = 0x80 | ((ch >> 24) & 0x3f);
      buf[2] = 0x80 | ((ch >> 18) & 0x3f);
      buf[3] = 0x80 | ((ch >> 12) & 0x3f);
      buf[4] = 0x80 | ((ch >> 6) & 0x3f);
      buf[5] = 0x80 | (ch & 0x3f);
    }
  return 0;
}

/* Return a newly allocated string. The buffer input must not contain
   a partial reference (i.e. "&am").

   todo: this needs to be changed to recursively process general
   entities.
*/
static char *
RXML_replace_entities (RXML_Context *context, RXML_Entities *entities,
		       const char *buf, int size)
{
  char *result;
  int result_size;
  int i, j;
  char *rbuf;
  int rbuf_size;

  result_size = size + 1;
  if (result_size < 6)
    result_size = 6;
  result = RXML_new (context, char, result_size);

  rbuf = (char *)buf;
  rbuf_size = 0;

  j = 0;
  for (i = 0; i < size; i++)
    {
      char c = rbuf[i];
      if (c == '&')
	{
	  /* XML 1.0 [67] Reference */
	  i++;
	  if (i == size)
	    return result;
	  c = rbuf[i];
	  if (c == '#')
	    {
	      /* XML 1.0 [66] CharRef */
	      char ch[7];
	      int ch_len;

	      ch_len = RXML_parse_charref (ch, rbuf + i - 1);
	      if (j + ch_len > result_size)
		RXML_expand (context, result, char, result_size);
	      memcpy (result + j, ch, ch_len);
	      j += ch_len;
	      while (i < size && rbuf[i] != ';') i++;
	    }
	  else
	    {
	      /* XML 1.0 [68] EntityRef */
	      char *replacement;
	      replacement = RXML_lookup_entity (entities, rbuf + i - 1);
	      if (replacement != NULL)
		{
		  int repl_len = strlen (replacement);
		  /* ok, we need to stick the replacement text in the
		     rbuf buffer, prepended to the remainder of the
		     input string. */
		  while (i < size && rbuf[i] != ';') i++;
		  i++;
		  if (rbuf == buf)
		    {
		      /* allocate a new rbuf from scratch */
		      int rbuf_size;
		      rbuf_size = repl_len + size - i;
		      if (rbuf_size < 6)
			rbuf_size = 6;
		      rbuf = RXML_new (context, char, rbuf_size);
		      memcpy (rbuf, replacement, repl_len);
		      memcpy (rbuf + repl_len, buf + i, size - i);
		      i = -1;
		    }
		  else if (repl_len < i)
		    {
		      /* store it in place in the existing rbuf */
		      memcpy (rbuf + i - repl_len, replacement, repl_len);
		      i -= repl_len + 1;
		    }
		  else
		    {
		      /* uggh, we have to reallocate the rbuf */
		      exit (1);
		    }
		}
	      else
		i--;
	    }
	}
      else
	{
	  if (j == result_size)
	    RXML_expand (context, result, char, result_size);
	  result[j++] = c;
	}
    }
  if (j == result_size)
    RXML_expand (context, result, char, result_size);
  result[j++] = '\0';

  if (rbuf != buf)
    RXML_free (context, rbuf);
  return result;
}

static RXML_Error *
RXML_parse_xml_utf8_write (RXML_ByteWriter *self, char *buf, int size)
{
  RXML_ByteWriterXP *bw;
  int buf_n;
  RXML_Context *context;
  char *ibuf;
  int rd_ix, wr_ix;
  int beg_rd_ix;
  RXML_Tag *tag;
  RXML_XbitWriter *xw;
  int i;

  bw = (RXML_ByteWriterXP *)self;
  context = bw->context;
  xw = bw->xw;

  rd_ix = bw->rd_ix;
  wr_ix = bw->wr_ix;

  /* transfer the input buffer to the internal buffer */
  if (wr_ix + size >= bw->buf_size)
    {
      buf_n = wr_ix - rd_ix;
      if (rd_ix >= (bw->buf_size >> 1) &&
	  buf_n + size <= bw->buf_size)
	{
	  if (buf_n)
	    memcpy (bw->buf, bw->buf + rd_ix, buf_n);
	}
      else
	{
	  /* need to reallocate */
	  char *new_buf;
	  int new_buf_size;

	  new_buf_size = bw->buf_size << 1;
	  while (new_buf_size < buf_n + size)
	    new_buf_size <<= 1;
	  new_buf = RXML_new (context, char, new_buf_size);
	  if (buf_n)
	    memcpy (new_buf, bw->buf + rd_ix, buf_n);
	  RXML_free (context, bw->buf);
	  bw->buf = new_buf;
	  bw->buf_size = new_buf_size;
	}
      /* either way, existing data in buffer goes to the left */
      wr_ix = buf_n;
      rd_ix = 0;
    }
  /* invariant: bw->wr_ix + size <= bw->buf_size */
  ibuf = bw->buf;
  memcpy (ibuf + bw->wr_ix, buf, size);
  wr_ix += size;
  bw->wr_ix = wr_ix;

  beg_rd_ix = rd_ix;

  /* ok, now everything is in ibuf */
  while (rd_ix < wr_ix)
    {
      if (ibuf[rd_ix] == '<')
	{
	  int scout;

	  for (scout = rd_ix + 1; scout < wr_ix; scout++)
	    if (ibuf[scout] == '>')
	      break;
	  if (scout == wr_ix)
	    /* tag in buffer is incomplete */
	    break;
	  /* tag in buffer is complete */
	  if (ibuf[rd_ix + 1] == '?')
	    {
	      /* it's a pi, skip for now, todo: handle pi's */
	      rd_ix = scout + 1;
	    }
	  else if (ibuf[rd_ix + 1] == '!')
	    {
	      /* it's probably a declaration, maybe CDATA */
	      /* todo: handle this */
	      rd_ix = scout + 1;
	    }
	  else if (ibuf[rd_ix + 1] == '/')
	    {
	      /* end tag */
	      /* todo: check end tag matches tag on stack */
	      if (bw->tag_sp > 0)
		{
		  bw->tag_sp--;
		  tag = &bw->tag_stack[bw->tag_sp];
		  xw->end_tag (xw, tag);
		}
	      /* todo: else error */
	      rd_ix = scout + 1;
	    }
	  else
	    {
	      /* vanilla tag, parse it */
	      RXML_Attr *attr;
	      int n_attr, n_attr_max;

	      /* expand tag stack if necessary */
	      if (bw->tag_sp == bw->tag_sp_max)
		RXML_expand (context, bw->tag_stack, RXML_Tag,
			     bw->tag_sp_max);
	      tag = &bw->tag_stack[bw->tag_sp];
	      rd_ix++; /* skip over initial '<' */
	      i = rd_ix;
	      while (ibuf[i] != '>' && ibuf[i] != '/' && ibuf[i] > ' ') i++;
	      tag->name = RXML_new (context, char, 1 + i - rd_ix);
	      memcpy (tag->name, ibuf + rd_ix, i - rd_ix);
	      tag->name[i - rd_ix] = '\0';

	      /* skip over S */
	      while (ibuf[i] <= ' ') i++;
	      rd_ix = i;

	      /* we'll want to split out the attribute parser, because
		 it's used for a bunch of other things */
	      n_attr_max = 4;
	      attr = RXML_new (context, RXML_Attr, n_attr_max);
	      n_attr = 0;
	      while (ibuf[rd_ix] != '>' && ibuf[rd_ix] != '/')
		{
		  char quot;

		  if (n_attr == n_attr_max)
		    RXML_expand (context, attr, RXML_Attr, n_attr_max);
		  i = rd_ix;
		  while (ibuf[i] != '>' && ibuf[i] != '=' && ibuf[i] > ' ')
		    i++;
		  attr[n_attr].name = RXML_new (context, char, 1 + i - rd_ix);
		  memcpy (attr[n_attr].name, ibuf + rd_ix, i - rd_ix);
		  attr[n_attr].name[i - rd_ix] = '\0';
		  /* skip over S */
		  while (ibuf[i] <= ' ') i++;
		  if (ibuf[i] == '=')
		    i++;
		  /* todo: else cause an error */
		  /* skip over S */
		  while (ibuf[i] <= ' ') i++;
		  quot = ibuf[i];
		  if (quot == '\'' || quot == '"')
		    i++;
		  else
		    quot = '\''; /* stub so it won't go past '>' below */
		  /* todo: else cause an error */
		  rd_ix = i;
		  while (ibuf[i] != '>' && ibuf[i] != quot) i++;
		  /* handle entity references in value */
		  attr[n_attr].value = RXML_replace_entities (context,
							      bw->entities,
							      ibuf + rd_ix,
							      i - rd_ix);
		  if (ibuf[i] == quot)
		    i++;
		  n_attr++;

		  /* skip over S */
		  while (ibuf[i] <= ' ') i++;
		  rd_ix = i;
		}
	      tag->n_attrs = n_attr;
	      if (n_attr > 0)
		tag->attrs = attr;
	      else
		RXML_free (context, attr);

	      xw->start_tag (xw, tag);
	      if (ibuf[rd_ix] == '/')
		xw->end_tag (xw, tag);
	      else
		bw->tag_sp++;
	      while (ibuf[rd_ix] != '>') rd_ix++;
	      rd_ix++;
	    }
	} else if (ibuf[rd_ix] == '&') {
	  int scout;

	  for (scout = rd_ix + 1; scout < wr_ix; scout++)
	    if (ibuf[scout] == ';')
	      break;
	  if (scout == wr_ix)
	    /* reference is incomplete */
	    break;
	  if (ibuf[rd_ix + 1] == '#')
	    {
	      char ch[7];
	      int ch_len;

	      ch_len = RXML_parse_charref (ch, ibuf + rd_ix);
	      ch[ch_len] = '\0';
	      xw->text (xw, ch);
	      rd_ix = scout + 1;
	    }
	  else
	    {
	      char *replacement;
	      int repl_len;

	      replacement = RXML_lookup_entity (bw->entities, ibuf + rd_ix);
	      if (replacement != NULL)
		{
		  repl_len = strlen (replacement);
		  if (scout + 1 > repl_len)
		    {
		      /* common case, just write the replacement in the
			 buffer, replacing the reference */
		      rd_ix = scout + 1 - repl_len;
		      memcpy (ibuf + rd_ix, replacement, repl_len);
		    }
		  else
		    {
		      /* uggh, need to allocate more room at the front
			 of the buffer to accomodate the replacement. */
		      int space_needed = wr_ix + repl_len - (scout + 1);
		      if (space_needed > bw->buf_size)
			{
			  char *new_buf;
			  int new_buf_size;

			  new_buf_size = bw->buf_size << 1;
			  while (new_buf_size < space_needed)
			    new_buf_size <<= 1;
			  new_buf = RXML_new (context, char, new_buf_size);
			  if (space_needed > repl_len)
			    memcpy (new_buf + repl_len, ibuf + scout + 1,
				    space_needed - repl_len);
			  RXML_free (context, ibuf);
			  ibuf = new_buf;
			  bw->buf = new_buf;
			  bw->buf_size = new_buf_size;
			}
		      else
			{
			  if (space_needed > repl_len)
			    memmove (ibuf + repl_len, ibuf + scout + 1,
				     space_needed - repl_len);
			}
		      rd_ix = 0;
		      memcpy (ibuf, replacement, repl_len);
		    }
		}
	      else
		/* error, entity not found, just skip the & */
		rd_ix++;
	    }
	} else {
	  /* not '<' or '&', must be text */
	  char *obuf;
	  char c;

	  /* go as far as we can, which means to the next < or to the
	     next reference */
	  i = rd_ix;
	  while (i < wr_ix && (c = ibuf[i]) != '<' && c != '&')
	    i++;

	  /* todo optimization: keep this buffer around */
	  obuf = RXML_new (context, char, 1 + i - rd_ix);
	  memcpy (obuf, ibuf + rd_ix, i - rd_ix);
	  obuf[i - rd_ix] = '\0';
	  xw->text (xw, obuf);
	  RXML_free (context, obuf);
	  rd_ix = i;
	}
    }

  bw->rd_ix = rd_ix;
  bw->pos += rd_ix - beg_rd_ix;

  return NULL;
}

static RXML_Error *
RXML_parse_xml_utf8_close (RXML_ByteWriter *self)
{
  RXML_ByteWriterXP *bw = (RXML_ByteWriterXP *)self;
  RXML_XbitWriter *xw = bw->xw;

  xw->close (xw);

  return NULL;
}

RXML_ByteWriter *
RXML_parse_xml_utf8 (RXML_Context *context, RXML_XbitWriter *xw)
{
  RXML_ByteWriterXP *bw;

  bw = RXML_new (context, RXML_ByteWriterXP, 1);

  bw->writer.write = RXML_parse_xml_utf8_write;
  bw->writer.close = RXML_parse_xml_utf8_close;

  bw->context = context;

  bw->xw = xw;

  bw->buf_size = 1024;
  bw->buf = RXML_new (context, char, bw->buf_size);
  bw->rd_ix = 0;
  bw->wr_ix = 0;

  bw->pos = 0;
  bw->line_no = 1;

  bw->tag_sp_max = 16;
  bw->tag_stack = RXML_new (context, RXML_Tag, bw->tag_sp_max);
  bw->tag_sp = 0;

  bw->entities = (RXML_Entities *)&RXML_default_entities;

  return (RXML_ByteWriter *)bw;
}

RXML_ByteWriter *
RXML_parse_xml (RXML_Context *context,
		RXML_XbitWriter *dst, const char *encoding)
{
  RXML_ByteWriter *bw;

  bw = RXML_parse_xml_utf8 (context, dst);
  return RXML_encoding_decode (context, bw, encoding);
  /* todo: check NULL and destroy bw if so */
}

typedef struct _RXML_XbitWriterTreePriv RXML_XbitWriterTreePriv;

struct _RXML_XbitWriterTreePriv {
  RXML_XbitWriterTree tree;

  RXML_Context *context;

  RXML_Element *el_stack;
  int el_sp, el_sp_max;
};

static RXML_Error *
RXML_tree_from_xbit_start_tag (RXML_XbitWriter *self, RXML_Tag *tag)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = (RXML_XbitWriterTreePriv *)self;

  return NULL;
}

static RXML_Error *
RXML_tree_from_xbit_end_tag (RXML_XbitWriter *self, RXML_Tag *tag)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = (RXML_XbitWriterTreePriv *)self;

  return NULL;
}

static RXML_Error *
RXML_tree_from_xbit_text (RXML_XbitWriter *self, char *buf)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = (RXML_XbitWriterTreePriv *)self;

  return NULL;
}

static RXML_Error *
RXML_tree_from_xbit_close (RXML_XbitWriter *self)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = (RXML_XbitWriterTreePriv *)self;

  return NULL;
}

static RXML_Document *
RXML_tree_from_xbit_get_doc (RXML_XbitWriterTree *self)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = (RXML_XbitWriterTreePriv *)self;

  return NULL;
}

static void
RXML_tree_from_xbit_free (RXML_XbitWriterTree *self)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = (RXML_XbitWriterTreePriv *)self;
}


RXML_XbitWriterTree *
RXML_tree_from_xbit (RXML_Context *context)
{
  RXML_XbitWriterTreePriv *xwt;

  xwt = RXML_new (context, RXML_XbitWriterTreePriv, 1);

  xwt->tree.writer.start_tag = RXML_tree_from_xbit_start_tag;
  xwt->tree.writer.end_tag = RXML_tree_from_xbit_end_tag;
  xwt->tree.writer.text = RXML_tree_from_xbit_text;
  xwt->tree.writer.close = RXML_tree_from_xbit_close;

  xwt->tree.get_doc = RXML_tree_from_xbit_get_doc;
  xwt->tree.free = RXML_tree_from_xbit_free;

  xwt->context = context;

  xwt->el_sp_max = 16;
  xwt->el_stack = RXML_new (context, RXML_Element, xwt->el_sp_max);
  xwt->el_sp = 0;

  return (RXML_XbitWriterTree *)xwt;
}

typedef struct _RXML_XbitWriterBytes RXML_XbitWriterBytes;

struct _RXML_XbitWriterBytes {
  RXML_XbitWriter writer;

  RXML_Context *context;
  RXML_ByteWriter *bw;
};

/* Ack! Having to do all our own string buffer stuff is a bit painful */

static char *
RXML_charbuf_ensure (RXML_Context *context,
		     char **pbuf,
		     int *pbuf_size,
		     int wr_ix,
		     int new)
{
  if (wr_ix + new > *pbuf_size)
    RXML_expand (context, *pbuf, char, *pbuf_size);
  return *pbuf + wr_ix;
}

static void
RXML_append_string (RXML_Context *context,
		    char **pbuf,
		    int *pbuf_size,
		    int *pwr_ix,
		    const char *str)
{
  int len;
  char *p;

  len = strlen (str);

  p = RXML_charbuf_ensure (context, pbuf, pbuf_size, *pwr_ix, len);
  memcpy (p, str, len);
  *pwr_ix += len;
}

static void
RXML_append_expand (RXML_Context *context,
		    char **pbuf,
		    int *pbuf_size,
		    int *pwr_ix,
		    const char *str)
{
  int len;
  int i;
  char *p;
  char c;

  len = 0;
  for (i = 0; (c = str[i]) != '\0'; i++)
    switch (c)
      {
      case '<':
      case '>':
	len += 3;
	break;
      case '&':
	len += 4;
	break;
      case '"':
	len += 5;
	break;
      }
  len += i;

  p = RXML_charbuf_ensure (context, pbuf, pbuf_size, *pwr_ix, len);
  *pwr_ix += len;

  /* could special-case len == i case here (it would be a memcpy) */
  for (i = 0; (c = str[i]) != '\0'; i++)
    switch (c)
      {
      case '<':
	*p++ = '&';
	*p++ = 'l';
	*p++ = 't';
	*p++ = ';';
	break;
      case '>':
	*p++ = '&';
	*p++ = 'g';
	*p++ = 't';
	*p++ = ';';
	break;
      case '&':
	*p++ = '&';
	*p++ = 'a';
	*p++ = 'm';
	*p++ = 'p';
	*p++ = ';';
	break;
      case '"':
	*p++ = '&';
	*p++ = 'q';
	*p++ = 'u';
	*p++ = 'o';
	*p++ = 't';
	*p++ = ';';
	break;
      default:
	*p++ = c;
	break;
      }
}

static RXML_Error *
RXML_bytes_from_xbit_start_tag (RXML_XbitWriter *self, RXML_Tag *tag)
{
  RXML_XbitWriterBytes *xwt;
  RXML_Context *context;
  RXML_ByteWriter *bw;
  char *buf;
  int buf_size;
  int wr_ix;
  int i;

  xwt = (RXML_XbitWriterBytes *)self;
  context = xwt->context;
  bw = xwt->bw;

  buf_size = 1024;
  buf = RXML_new (context, char, buf_size);
  wr_ix = 0;

  RXML_append_string (context, &buf, &buf_size, &wr_ix, "<");
  RXML_append_string (context, &buf, &buf_size, &wr_ix, tag->name);

  for (i = 0; i < tag->n_attrs; i++)
    {
      RXML_Attr *attr = &tag->attrs[i];
      RXML_append_string (context, &buf, &buf_size, &wr_ix, " ");
      RXML_append_string (context, &buf, &buf_size, &wr_ix, attr->name);
      RXML_append_string (context, &buf, &buf_size, &wr_ix, "=\"");
      RXML_append_expand (context, &buf, &buf_size, &wr_ix, attr->value);
      RXML_append_string (context, &buf, &buf_size, &wr_ix, "\"");
    }

  RXML_append_string (context, &buf, &buf_size, &wr_ix, ">");

  bw->write (bw, buf, wr_ix);
  /* todo: handle errors */

  RXML_free (context, buf);
  
  return NULL;
}

static RXML_Error *
RXML_bytes_from_xbit_end_tag (RXML_XbitWriter *self, RXML_Tag *tag)
{
  RXML_XbitWriterBytes *xwt;
  RXML_Context *context;
  RXML_ByteWriter *bw;
  char *buf;
  int buf_size;
  int wr_ix;

  xwt = (RXML_XbitWriterBytes *)self;
  context = xwt->context;
  bw = xwt->bw;

  buf_size = 1024;
  buf = RXML_new (context, char, buf_size);
  wr_ix = 0;

  RXML_append_string (context, &buf, &buf_size, &wr_ix, "</");
  RXML_append_string (context, &buf, &buf_size, &wr_ix, tag->name);
  RXML_append_string (context, &buf, &buf_size, &wr_ix, ">");

  bw->write (bw, buf, wr_ix);
  /* todo: handle errors */

  RXML_free (context, buf);
  
  return NULL;
}

static RXML_Error *
RXML_bytes_from_xbit_text (RXML_XbitWriter *self, char *text)
{
  RXML_XbitWriterBytes *xwt;
  RXML_Context *context;
  RXML_ByteWriter *bw;
  char *buf;
  int buf_size;
  int wr_ix;

  xwt = (RXML_XbitWriterBytes *)self;
  context = xwt->context;
  bw = xwt->bw;

  buf_size = 1024;
  buf = RXML_new (context, char, buf_size);
  wr_ix = 0;

  RXML_append_expand (context, &buf, &buf_size, &wr_ix, text);

  bw->write (bw, buf, wr_ix);
  /* todo: handle errors */

  RXML_free (context, buf);
  
  return NULL;
}

static RXML_Error *
RXML_bytes_from_xbit_close (RXML_XbitWriter *self)
{
  RXML_XbitWriterBytes *xwt;
  RXML_ByteWriter *bw;

  xwt = (RXML_XbitWriterBytes *)self;
  bw = xwt->bw;

  bw->write (bw, "\n", 1);
  bw->close (bw);
  /* todo: handle errors */

  return NULL;
}

RXML_XbitWriter *
RXML_bytes_from_xbit (RXML_Context *context,
		      RXML_ByteWriter *bw)
{
  RXML_XbitWriterBytes *xw;

  xw = RXML_new (context, RXML_XbitWriterBytes, 1);

  xw->writer.start_tag = RXML_bytes_from_xbit_start_tag;
  xw->writer.end_tag = RXML_bytes_from_xbit_end_tag;
  xw->writer.text = RXML_bytes_from_xbit_text;
  xw->writer.close = RXML_bytes_from_xbit_close;

  xw->context = context;
  xw->bw = bw;

  return (RXML_XbitWriter *)xw;
}

static void
RXML_write_document_element (RXML_Element *e, RXML_XbitWriter *xw)
{
  int i;

  xw->start_tag (xw, &e->tag);
  for (i = 0; i < e->children.n_bits; i++)
    {
      RXML_ContentBit *bit = &e->children.bits[i];
      if (bit->type == RXML_ELEMENT)
	RXML_write_document_element (bit->content.element, xw);
      else if (bit->type == RXML_CHARDATA)
	xw->text (xw, bit->content.CharData);
      /* todo: write pi's etc */
    }
  xw->end_tag (xw, &e->tag);
}

void
RXML_write_document (RXML_Document *document, RXML_XbitWriter *xw)
{
  /* todo: write prolog */
  RXML_write_document_element (document->element, xw);
  xw->close (xw);
}

/* A simple utility function */

char *
RXML_strdup (RXML_Context *context, const char *str)
{
  int len;
  char *result;

  len = strlen (str) + 1;
  result = RXML_new (context, char, len);
  memcpy (result, str, len);
  return result;
}

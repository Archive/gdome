#include "gdome-util.h"

void gdome_evntt_ref (GdomeEventTarget *self, GdomeException *exc)
{
  gdome_n_ref ((GdomeNode *)self, exc);
}

void gdome_evntt_unref (GdomeEventTarget *self, GdomeException *exc)
{
  gdome_n_unref ((GdomeNode *)self, exc);
}

void * gdome_evntt_query_interface (GdomeEventTarget *self, const char *interface, GdomeException *exc)
{
  return gdome_n_query_interface ((GdomeNode *)self, interface, exc);
}

void gdome_evntt_addEventListener (GdomeEventTarget *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  gdome_n_addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void gdome_evntt_removeEventListener (GdomeEventTarget *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  gdome_n_removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean gdome_evntt_dispatchEvent (GdomeEventTarget *self, GdomeEvent *evt, GdomeException *exc)
{
  return gdome_n_dispatchEvent ((GdomeNode *)self, evt, exc);
}



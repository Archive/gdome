typedef struct _GdomeEventPriv GdomeEventPriv;
typedef struct _GdomeMutationEventPriv GdomeMutationEventPriv;
typedef struct _GdomeListenerList GdomeListenerList;
typedef struct _GdomeEventListenerPriv GdomeEventListenerPriv;

struct _GdomeEventListenerPriv {
  void (*callback) (GdomeEventListener *self, 
		    GdomeEvent *event, 
		    GdomeException *exc);
  void *priv;

};

struct _GdomeEventPriv {
  char *type;
  GdomeNode *target;
  xmlNode *currentNode;
  int eventPhase;
  GdomeBoolean bubbles;
  GdomeBoolean cancelable;

  GdomeBoolean propagation_stopped;
  GdomeBoolean default_prevented;
};

struct _GdomeMutationEventPriv {
  GdomeEventPriv ev;

  GdomeNode *relatedNode;
  GdomeDOMString *prevValue;
  GdomeDOMString *newValue;
  GdomeDOMString *attrName;
};

struct _GdomeListenerList {
  GdomeListenerList *next;
  char *type;
  GdomeEventListener *listener;
  GdomeBoolean useCapture;
};

GdomeDocument *gdome_xml_from_document (xmlDoc *doc);

/* conveniance function */
GdomeEventListener * 
gdome_xml_evntl_new (void (*callback) (GdomeEventListener *self, 
					GdomeEvent *event, 
					GdomeException *exc), 
		     void *priv);


void * gdome_xml_evntl_get_priv (GdomeEventListener *self);

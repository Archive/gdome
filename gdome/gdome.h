/* ----- gdome.h ----- */
#ifndef GDOME_H
#define GDOME_H

#define GDOME_REFCOUNT

typedef struct _GdomeDOMString GdomeDOMString;
typedef int GdomeBoolean;
typedef unsigned short GdomeException;

struct _GdomeDOMString {
  void (*unref) (GdomeDOMString *self);
  char *str;
};

typedef struct _GdomeNode GdomeNode;
typedef struct _GdomeAttr GdomeAttr;
typedef struct _GdomeCharacterData GdomeCharacterData;
typedef struct _GdomeText GdomeText;
typedef struct _GdomeCDATASection GdomeCDATASection;
typedef struct _GdomeComment GdomeComment;
typedef struct _GdomeDOMImplementation GdomeDOMImplementation;
typedef struct _GdomeDocument GdomeDocument;
typedef struct _GdomeDocumentFragment GdomeDocumentFragment;
typedef struct _GdomeDocumentType GdomeDocumentType;
typedef struct _GdomeElement GdomeElement;
typedef struct _GdomeEntity GdomeEntity;
typedef struct _GdomeEntityReference GdomeEntityReference;
typedef struct _GdomeEvent GdomeEvent;
typedef struct _GdomeEventListener GdomeEventListener;
typedef struct _GdomeMutationEvent GdomeMutationEvent;
typedef struct _GdomeNamedNodeMap GdomeNamedNodeMap;
typedef struct _GdomeNodeList GdomeNodeList;
typedef struct _GdomeNotation GdomeNotation;
typedef struct _GdomeProcessingInstruction GdomeProcessingInstruction;
typedef struct _GdomeUIEvent GdomeUIEvent;

typedef struct _GdomeNodeVtab GdomeNodeVtab;
typedef struct _GdomeAttrVtab GdomeAttrVtab;
typedef struct _GdomeCharacterDataVtab GdomeCharacterDataVtab;
typedef struct _GdomeTextVtab GdomeTextVtab;
typedef struct _GdomeCDATASectionVtab GdomeCDATASectionVtab;
typedef struct _GdomeCommentVtab GdomeCommentVtab;
typedef struct _GdomeDOMImplementationVtab GdomeDOMImplementationVtab;
typedef struct _GdomeDocumentVtab GdomeDocumentVtab;
typedef struct _GdomeDocumentFragmentVtab GdomeDocumentFragmentVtab;
typedef struct _GdomeDocumentTypeVtab GdomeDocumentTypeVtab;
typedef struct _GdomeElementVtab GdomeElementVtab;
typedef struct _GdomeEntityVtab GdomeEntityVtab;
typedef struct _GdomeEntityReferenceVtab GdomeEntityReferenceVtab;
typedef struct _GdomeEventVtab GdomeEventVtab;
typedef struct _GdomeEventListenerVtab GdomeEventListenerVtab;
typedef struct _GdomeMutationEventVtab GdomeMutationEventVtab;
typedef struct _GdomeNamedNodeMapVtab GdomeNamedNodeMapVtab;
typedef struct _GdomeNodeListVtab GdomeNodeListVtab;
typedef struct _GdomeNotationVtab GdomeNotationVtab;
typedef struct _GdomeProcessingInstructionVtab GdomeProcessingInstructionVtab;
typedef struct _GdomeUIEventVtab GdomeUIEventVtab;

#define GDOME_ELEMENT_NODE 1
#define GDOME_ATTRIBUTE_NODE 2
#define GDOME_TEXT_NODE 3
#define GDOME_CDATA_SECTION_NODE 4
#define GDOME_ENTITY_REFERENCE_NODE 5
#define GDOME_ENTITY_NODE 6
#define GDOME_PROCESSING_INSTRUCTION_NODE 7
#define GDOME_COMMENT_NODE 8
#define GDOME_DOCUMENT_NODE 9
#define GDOME_DOCUMENT_TYPE_NODE 10
#define GDOME_DOCUMENT_FRAGMENT_NODE 11
#define GDOME_NOTATION_NODE 12

#define GDOME_CAPTURING_PHASE 1
#define GDOME_AT_TARGET 2
#define GDOME_BUBBLING_PHASE 3

struct _GdomeNode {
  const GdomeNodeVtab *vtab;
};

struct _GdomeAttr {
  const GdomeAttrVtab *vtab;
};

struct _GdomeCharacterData {
  const GdomeCharacterDataVtab *vtab;
};

struct _GdomeText {
  const GdomeTextVtab *vtab;
};

struct _GdomeCDATASection {
  const GdomeCDATASectionVtab *vtab;
};

struct _GdomeComment {
  const GdomeCommentVtab *vtab;
};

struct _GdomeDOMImplementation {
  const GdomeDOMImplementationVtab *vtab;
};

struct _GdomeDocument {
  const GdomeDocumentVtab *vtab;
};

struct _GdomeDocumentFragment {
  const GdomeDocumentFragmentVtab *vtab;
};

struct _GdomeDocumentType {
  const GdomeDocumentTypeVtab *vtab;
};

struct _GdomeElement {
  const GdomeElementVtab *vtab;
};

struct _GdomeEntity {
  const GdomeEntityVtab *vtab;
};

struct _GdomeEntityReference {
  const GdomeEntityReferenceVtab *vtab;
};

struct _GdomeEvent {
  const GdomeEventVtab *vtab;
};

struct _GdomeEventListener {
  const GdomeEventListenerVtab *vtab;
};

struct _GdomeMutationEvent {
  const GdomeMutationEventVtab *vtab;
};

struct _GdomeNamedNodeMap {
  const GdomeNamedNodeMapVtab *vtab;
};

struct _GdomeNodeList {
  const GdomeNodeListVtab *vtab;
};

struct _GdomeNotation {
  const GdomeNotationVtab *vtab;
};

struct _GdomeProcessingInstruction {
  const GdomeProcessingInstructionVtab *vtab;
};

struct _GdomeUIEvent {
  const GdomeUIEventVtab *vtab;
};


struct _GdomeNodeVtab {
  void (*ref) (GdomeNode *self, GdomeException *exc);
  void (*unref) (GdomeNode *self, GdomeException *exc);
  void * (*query_interface) (GdomeNode *self, const char *interface, GdomeException *exc);
  GdomeDOMString *(*nodeName) (GdomeNode *self, GdomeException *exc);
  GdomeDOMString *(*nodeValue) (GdomeNode *self, GdomeException *exc);
  void (*set_nodeValue) (GdomeNode *self, GdomeDOMString *nodeValue, GdomeException *exc);
  unsigned short (*nodeType) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*parentNode) (GdomeNode *self, GdomeException *exc);
  GdomeNodeList *(*childNodes) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*firstChild) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*lastChild) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*previousSibling) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*nextSibling) (GdomeNode *self, GdomeException *exc);
  GdomeNamedNodeMap *(*attributes) (GdomeNode *self, GdomeException *exc);
  GdomeDocument *(*ownerDocument) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*insertBefore) (GdomeNode *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
  GdomeNode *(*replaceChild) (GdomeNode *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
  GdomeNode *(*removeChild) (GdomeNode *self, GdomeNode *oldChild, GdomeException *exc);
  GdomeNode *(*appendChild) (GdomeNode *self, GdomeNode *newChild, GdomeException *exc);
  GdomeBoolean (*hasChildNodes) (GdomeNode *self, GdomeException *exc);
  GdomeNode *(*cloneNode) (GdomeNode *self, GdomeBoolean deep, GdomeException *exc);
  void (*normalize) (GdomeNode *self, GdomeException *exc);
  GdomeBoolean (*supports) (GdomeNode *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
  GdomeDOMString *(*namespaceURI) (GdomeNode *self, GdomeException *exc);
  GdomeDOMString *(*prefix) (GdomeNode *self, GdomeException *exc);
  void (*set_prefix) (GdomeNode *self, GdomeDOMString *prefix, GdomeException *exc);
  GdomeDOMString *(*localName) (GdomeNode *self, GdomeException *exc);
  void (*addEventListener) (GdomeNode *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
  void (*removeEventListener) (GdomeNode *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
  GdomeBoolean (*dispatchEvent) (GdomeNode *self, GdomeEvent *evt, GdomeException *exc);
};

struct _GdomeAttrVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*name) (GdomeAttr *self, GdomeException *exc);
  GdomeBoolean (*specified) (GdomeAttr *self, GdomeException *exc);
  GdomeDOMString *(*value) (GdomeAttr *self, GdomeException *exc);
  void (*set_value) (GdomeAttr *self, GdomeDOMString *value, GdomeException *exc);
};

struct _GdomeCharacterDataVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*data) (GdomeCharacterData *self, GdomeException *exc);
  void (*set_data) (GdomeCharacterData *self, GdomeDOMString *data, GdomeException *exc);
  unsigned long (*length) (GdomeCharacterData *self, GdomeException *exc);
  GdomeDOMString *(*substringData) (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeException *exc);
  void (*appendData) (GdomeCharacterData *self, GdomeDOMString *arg, GdomeException *exc);
  void (*insertData) (GdomeCharacterData *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc);
  void (*deleteData) (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeException *exc);
  void (*replaceData) (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc);
};

struct _GdomeTextVtab {
  GdomeCharacterDataVtab super;
  GdomeText *(*splitText) (GdomeText *self, unsigned long offset, GdomeException *exc);
};

struct _GdomeCDATASectionVtab {
  GdomeTextVtab super;
};

struct _GdomeCommentVtab {
  GdomeCharacterDataVtab super;
};

struct _GdomeDOMImplementationVtab {
  void (*ref) (GdomeDOMImplementation *self, GdomeException *exc);
  void (*unref) (GdomeDOMImplementation *self, GdomeException *exc);
  void * (*query_interface) (GdomeDOMImplementation *self, const char *interface, GdomeException *exc);
  GdomeBoolean (*hasFeature) (GdomeDOMImplementation *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
  GdomeDocumentType *(*createDocumentType) (GdomeDOMImplementation *self, GdomeDOMString *qualifiedName, GdomeDOMString *publicId, GdomeDOMString *systemId, GdomeDOMString *internalSubset, GdomeException *exc);
  GdomeDocument *(*createDocument) (GdomeDOMImplementation *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeDocumentType *doctype, GdomeException *exc);
};

struct _GdomeDocumentVtab {
  GdomeNodeVtab super;
  GdomeDocumentType *(*doctype) (GdomeDocument *self, GdomeException *exc);
  GdomeDOMImplementation *(*implementation) (GdomeDocument *self, GdomeException *exc);
  GdomeElement *(*documentElement) (GdomeDocument *self, GdomeException *exc);
  GdomeElement *(*createElement) (GdomeDocument *self, GdomeDOMString *tagName, GdomeException *exc);
  GdomeDocumentFragment *(*createDocumentFragment) (GdomeDocument *self, GdomeException *exc);
  GdomeText *(*createTextNode) (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc);
  GdomeComment *(*createComment) (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc);
  GdomeCDATASection *(*createCDATASection) (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc);
  GdomeProcessingInstruction *(*createProcessingInstruction) (GdomeDocument *self, GdomeDOMString *target, GdomeDOMString *data, GdomeException *exc);
  GdomeAttr *(*createAttribute) (GdomeDocument *self, GdomeDOMString *name, GdomeException *exc);
  GdomeEntityReference *(*createEntityReference) (GdomeDocument *self, GdomeDOMString *name, GdomeException *exc);
  GdomeNodeList *(*getElementsByTagName) (GdomeDocument *self, GdomeDOMString *tagname, GdomeException *exc);
  GdomeNode *(*importNode) (GdomeDocument *self, GdomeNode *importedNode, GdomeBoolean deep, GdomeException *exc);
  GdomeElement *(*createElementNS) (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc);
  GdomeAttr *(*createAttrNS) (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc);
  GdomeNodeList *(*getElementsByTagNameNS) (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc);
  GdomeElement *(*getElementById) (GdomeDocument *self, GdomeDOMString *elementId, GdomeException *exc);
};

struct _GdomeDocumentFragmentVtab {
  GdomeNodeVtab super;
};

struct _GdomeDocumentTypeVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*name) (GdomeDocumentType *self, GdomeException *exc);
  GdomeNamedNodeMap *(*entities) (GdomeDocumentType *self, GdomeException *exc);
  GdomeNamedNodeMap *(*notations) (GdomeDocumentType *self, GdomeException *exc);
  GdomeDOMString *(*publicId) (GdomeDocumentType *self, GdomeException *exc);
  GdomeDOMString *(*systemId) (GdomeDocumentType *self, GdomeException *exc);
  GdomeDOMString *(*internalSubset) (GdomeDocumentType *self, GdomeException *exc);
};

struct _GdomeElementVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*tagName) (GdomeElement *self, GdomeException *exc);
  GdomeDOMString *(*getAttribute) (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
  void (*setAttribute) (GdomeElement *self, GdomeDOMString *name, GdomeDOMString *value, GdomeException *exc);
  void (*removeAttribute) (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
  GdomeAttr *(*getAttributeNode) (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
  GdomeAttr *(*setAttributeNode) (GdomeElement *self, GdomeAttr *newAttr, GdomeException *exc);
  GdomeAttr *(*removeAttributeNode) (GdomeElement *self, GdomeAttr *oldAttr, GdomeException *exc);
  GdomeNodeList *(*getElementsByTagName) (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
  GdomeDOMString *(*getAttributeNS) (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
  void (*setAttributeNS) (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeDOMString *value, GdomeException *exc);
  void (*removeAttributeNS) (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
  GdomeAttr *(*getAttributeNodeNS) (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
  GdomeAttr *(*setAttributeNodeNS) (GdomeElement *self, GdomeAttr *newAttr, GdomeException *exc);
  GdomeNodeList *(*getElementsByTagNameNS) (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
};

struct _GdomeEntityVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*publicId) (GdomeEntity *self, GdomeException *exc);
  GdomeDOMString *(*systemId) (GdomeEntity *self, GdomeException *exc);
  GdomeDOMString *(*notationName) (GdomeEntity *self, GdomeException *exc);
};

struct _GdomeEntityReferenceVtab {
  GdomeNodeVtab super;
};

struct _GdomeEventVtab {
  void (*ref) (GdomeEvent *self, GdomeException *exc);
  void (*unref) (GdomeEvent *self, GdomeException *exc);
  void * (*query_interface) (GdomeEvent *self, const char *interface, GdomeException *exc);
  GdomeDOMString *(*type) (GdomeEvent *self, GdomeException *exc);
  GdomeNode *(*target) (GdomeEvent *self, GdomeException *exc);
  GdomeNode *(*currentNode) (GdomeEvent *self, GdomeException *exc);
  unsigned short (*eventPhase) (GdomeEvent *self, GdomeException *exc);
  GdomeBoolean (*bubbles) (GdomeEvent *self, GdomeException *exc);
  GdomeBoolean (*cancelable) (GdomeEvent *self, GdomeException *exc);
  void (*stopPropagation) (GdomeEvent *self, GdomeException *exc);
  void (*preventDefault) (GdomeEvent *self, GdomeException *exc);
  void (*initEvent) (GdomeEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc);
};

struct _GdomeEventListenerVtab {
  void (*ref) (GdomeEventListener *self, GdomeException *exc);
  void (*unref) (GdomeEventListener *self, GdomeException *exc);
  void * (*query_interface) (GdomeEventListener *self, const char *interface, GdomeException *exc);
  void (*handleEvent) (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc);
};

struct _GdomeMutationEventVtab {
  GdomeEventVtab super;
  GdomeNode *(*relatedNode) (GdomeMutationEvent *self, GdomeException *exc);
  GdomeDOMString *(*prevValue) (GdomeMutationEvent *self, GdomeException *exc);
  GdomeDOMString *(*newValue) (GdomeMutationEvent *self, GdomeException *exc);
  GdomeDOMString *(*attrName) (GdomeMutationEvent *self, GdomeException *exc);
  void (*initMutationEvent) (GdomeMutationEvent *self, GdomeDOMString *typeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeNode *relatedNodeArg, GdomeDOMString *prevValueArg, GdomeDOMString *newValueArg, GdomeDOMString *attrNameArg, GdomeException *exc);
};

struct _GdomeNamedNodeMapVtab {
  void (*ref) (GdomeNamedNodeMap *self, GdomeException *exc);
  void (*unref) (GdomeNamedNodeMap *self, GdomeException *exc);
  void * (*query_interface) (GdomeNamedNodeMap *self, const char *interface, GdomeException *exc);
  GdomeNode *(*getNamedItem) (GdomeNamedNodeMap *self, GdomeDOMString *name, GdomeException *exc);
  GdomeNode *(*setNamedItem) (GdomeNamedNodeMap *self, GdomeNode *arg, GdomeException *exc);
  GdomeNode *(*removeNamedItem) (GdomeNamedNodeMap *self, GdomeDOMString *name, GdomeException *exc);
  GdomeNode *(*item) (GdomeNamedNodeMap *self, unsigned long index, GdomeException *exc);
  unsigned long (*length) (GdomeNamedNodeMap *self, GdomeException *exc);
};

struct _GdomeNodeListVtab {
  void (*ref) (GdomeNodeList *self, GdomeException *exc);
  void (*unref) (GdomeNodeList *self, GdomeException *exc);
  void * (*query_interface) (GdomeNodeList *self, const char *interface, GdomeException *exc);
  GdomeNode *(*item) (GdomeNodeList *self, unsigned long index, GdomeException *exc);
  unsigned long (*length) (GdomeNodeList *self, GdomeException *exc);
};

struct _GdomeNotationVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*publicId) (GdomeNotation *self, GdomeException *exc);
  GdomeDOMString *(*systemId) (GdomeNotation *self, GdomeException *exc);
};

struct _GdomeProcessingInstructionVtab {
  GdomeNodeVtab super;
  GdomeDOMString *(*target) (GdomeProcessingInstruction *self, GdomeException *exc);
  GdomeDOMString *(*data) (GdomeProcessingInstruction *self, GdomeException *exc);
  void (*set_data) (GdomeProcessingInstruction *self, GdomeDOMString *data, GdomeException *exc);
};

struct _GdomeUIEventVtab {
  GdomeEventVtab super;
  long (*screenX) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_screenX) (GdomeUIEvent *self, long screenX, GdomeException *exc);
  long (*screenY) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_screenY) (GdomeUIEvent *self, long screenY, GdomeException *exc);
  long (*clientX) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_clientX) (GdomeUIEvent *self, long clientX, GdomeException *exc);
  long (*clientY) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_clientY) (GdomeUIEvent *self, long clientY, GdomeException *exc);
  GdomeBoolean (*altKey) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_altKey) (GdomeUIEvent *self, GdomeBoolean altKey, GdomeException *exc);
  GdomeBoolean (*ctrlKey) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_ctrlKey) (GdomeUIEvent *self, GdomeBoolean ctrlKey, GdomeException *exc);
  GdomeBoolean (*shiftKey) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_shiftKey) (GdomeUIEvent *self, GdomeBoolean shiftKey, GdomeException *exc);
  unsigned long (*keyCode) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_keyCode) (GdomeUIEvent *self, unsigned long keyCode, GdomeException *exc);
  unsigned long (*charCode) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_charCode) (GdomeUIEvent *self, unsigned long charCode, GdomeException *exc);
  unsigned short (*button) (GdomeUIEvent *self, GdomeException *exc);
  void (*set_button) (GdomeUIEvent *self, unsigned short button, GdomeException *exc);
};

void gdome_str_unref (GdomeDOMString *self);

void gdome_n_ref (GdomeNode *self, GdomeException *exc);
void gdome_n_unref (GdomeNode *self, GdomeException *exc);
void * gdome_n_query_interface (GdomeNode *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_n_nodeName (GdomeNode *self, GdomeException *exc);
GdomeDOMString *gdome_n_nodeValue (GdomeNode *self, GdomeException *exc);
void gdome_n_set_nodeValue (GdomeNode *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_n_nodeType (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_parentNode (GdomeNode *self, GdomeException *exc);
GdomeNodeList *gdome_n_childNodes (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_firstChild (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_lastChild (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_previousSibling (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_nextSibling (GdomeNode *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_n_attributes (GdomeNode *self, GdomeException *exc);
GdomeDocument *gdome_n_ownerDocument (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_insertBefore (GdomeNode *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_n_replaceChild (GdomeNode *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_n_removeChild (GdomeNode *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_n_appendChild (GdomeNode *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_n_hasChildNodes (GdomeNode *self, GdomeException *exc);
GdomeNode *gdome_n_cloneNode (GdomeNode *self, GdomeBoolean deep, GdomeException *exc);
void gdome_n_normalize (GdomeNode *self, GdomeException *exc);
GdomeBoolean gdome_n_supports (GdomeNode *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_n_namespaceURI (GdomeNode *self, GdomeException *exc);
GdomeDOMString *gdome_n_prefix (GdomeNode *self, GdomeException *exc);
void gdome_n_set_prefix (GdomeNode *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_n_localName (GdomeNode *self, GdomeException *exc);
void gdome_n_addEventListener (GdomeNode *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_n_removeEventListener (GdomeNode *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_n_dispatchEvent (GdomeNode *self, GdomeEvent *evt, GdomeException *exc);

GdomeDOMString *gdome_a_name (GdomeAttr *self, GdomeException *exc);
GdomeBoolean gdome_a_specified (GdomeAttr *self, GdomeException *exc);
GdomeDOMString *gdome_a_value (GdomeAttr *self, GdomeException *exc);
void gdome_a_set_value (GdomeAttr *self, GdomeDOMString *value, GdomeException *exc);
void gdome_a_ref (GdomeAttr *self, GdomeException *exc);
void gdome_a_unref (GdomeAttr *self, GdomeException *exc);
void * gdome_a_query_interface (GdomeAttr *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_a_nodeName (GdomeAttr *self, GdomeException *exc);
GdomeDOMString *gdome_a_nodeValue (GdomeAttr *self, GdomeException *exc);
void gdome_a_set_nodeValue (GdomeAttr *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_a_nodeType (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_parentNode (GdomeAttr *self, GdomeException *exc);
GdomeNodeList *gdome_a_childNodes (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_firstChild (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_lastChild (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_previousSibling (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_nextSibling (GdomeAttr *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_a_attributes (GdomeAttr *self, GdomeException *exc);
GdomeDocument *gdome_a_ownerDocument (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_insertBefore (GdomeAttr *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_a_replaceChild (GdomeAttr *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_a_removeChild (GdomeAttr *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_a_appendChild (GdomeAttr *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_a_hasChildNodes (GdomeAttr *self, GdomeException *exc);
GdomeNode *gdome_a_cloneNode (GdomeAttr *self, GdomeBoolean deep, GdomeException *exc);
void gdome_a_normalize (GdomeAttr *self, GdomeException *exc);
GdomeBoolean gdome_a_supports (GdomeAttr *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_a_namespaceURI (GdomeAttr *self, GdomeException *exc);
GdomeDOMString *gdome_a_prefix (GdomeAttr *self, GdomeException *exc);
void gdome_a_set_prefix (GdomeAttr *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_a_localName (GdomeAttr *self, GdomeException *exc);
void gdome_a_addEventListener (GdomeAttr *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_a_removeEventListener (GdomeAttr *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_a_dispatchEvent (GdomeAttr *self, GdomeEvent *evt, GdomeException *exc);

GdomeDOMString *gdome_cd_data (GdomeCharacterData *self, GdomeException *exc);
void gdome_cd_set_data (GdomeCharacterData *self, GdomeDOMString *data, GdomeException *exc);
unsigned long gdome_cd_length (GdomeCharacterData *self, GdomeException *exc);
GdomeDOMString *gdome_cd_substringData (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_cd_appendData (GdomeCharacterData *self, GdomeDOMString *arg, GdomeException *exc);
void gdome_cd_insertData (GdomeCharacterData *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc);
void gdome_cd_deleteData (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_cd_replaceData (GdomeCharacterData *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc);
void gdome_cd_ref (GdomeCharacterData *self, GdomeException *exc);
void gdome_cd_unref (GdomeCharacterData *self, GdomeException *exc);
void * gdome_cd_query_interface (GdomeCharacterData *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_cd_nodeName (GdomeCharacterData *self, GdomeException *exc);
GdomeDOMString *gdome_cd_nodeValue (GdomeCharacterData *self, GdomeException *exc);
void gdome_cd_set_nodeValue (GdomeCharacterData *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_cd_nodeType (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_parentNode (GdomeCharacterData *self, GdomeException *exc);
GdomeNodeList *gdome_cd_childNodes (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_firstChild (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_lastChild (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_previousSibling (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_nextSibling (GdomeCharacterData *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_cd_attributes (GdomeCharacterData *self, GdomeException *exc);
GdomeDocument *gdome_cd_ownerDocument (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_insertBefore (GdomeCharacterData *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_cd_replaceChild (GdomeCharacterData *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_cd_removeChild (GdomeCharacterData *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_cd_appendChild (GdomeCharacterData *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_cd_hasChildNodes (GdomeCharacterData *self, GdomeException *exc);
GdomeNode *gdome_cd_cloneNode (GdomeCharacterData *self, GdomeBoolean deep, GdomeException *exc);
void gdome_cd_normalize (GdomeCharacterData *self, GdomeException *exc);
GdomeBoolean gdome_cd_supports (GdomeCharacterData *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_cd_namespaceURI (GdomeCharacterData *self, GdomeException *exc);
GdomeDOMString *gdome_cd_prefix (GdomeCharacterData *self, GdomeException *exc);
void gdome_cd_set_prefix (GdomeCharacterData *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_cd_localName (GdomeCharacterData *self, GdomeException *exc);
void gdome_cd_addEventListener (GdomeCharacterData *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_cd_removeEventListener (GdomeCharacterData *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_cd_dispatchEvent (GdomeCharacterData *self, GdomeEvent *evt, GdomeException *exc);

GdomeText *gdome_t_splitText (GdomeText *self, unsigned long offset, GdomeException *exc);
GdomeDOMString *gdome_t_data (GdomeText *self, GdomeException *exc);
void gdome_t_set_data (GdomeText *self, GdomeDOMString *data, GdomeException *exc);
unsigned long gdome_t_length (GdomeText *self, GdomeException *exc);
GdomeDOMString *gdome_t_substringData (GdomeText *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_t_appendData (GdomeText *self, GdomeDOMString *arg, GdomeException *exc);
void gdome_t_insertData (GdomeText *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc);
void gdome_t_deleteData (GdomeText *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_t_replaceData (GdomeText *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc);
void gdome_t_ref (GdomeText *self, GdomeException *exc);
void gdome_t_unref (GdomeText *self, GdomeException *exc);
void * gdome_t_query_interface (GdomeText *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_t_nodeName (GdomeText *self, GdomeException *exc);
GdomeDOMString *gdome_t_nodeValue (GdomeText *self, GdomeException *exc);
void gdome_t_set_nodeValue (GdomeText *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_t_nodeType (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_parentNode (GdomeText *self, GdomeException *exc);
GdomeNodeList *gdome_t_childNodes (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_firstChild (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_lastChild (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_previousSibling (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_nextSibling (GdomeText *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_t_attributes (GdomeText *self, GdomeException *exc);
GdomeDocument *gdome_t_ownerDocument (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_insertBefore (GdomeText *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_t_replaceChild (GdomeText *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_t_removeChild (GdomeText *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_t_appendChild (GdomeText *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_t_hasChildNodes (GdomeText *self, GdomeException *exc);
GdomeNode *gdome_t_cloneNode (GdomeText *self, GdomeBoolean deep, GdomeException *exc);
void gdome_t_normalize (GdomeText *self, GdomeException *exc);
GdomeBoolean gdome_t_supports (GdomeText *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_t_namespaceURI (GdomeText *self, GdomeException *exc);
GdomeDOMString *gdome_t_prefix (GdomeText *self, GdomeException *exc);
void gdome_t_set_prefix (GdomeText *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_t_localName (GdomeText *self, GdomeException *exc);
void gdome_t_addEventListener (GdomeText *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_t_removeEventListener (GdomeText *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_t_dispatchEvent (GdomeText *self, GdomeEvent *evt, GdomeException *exc);

GdomeText *gdome_cds_splitText (GdomeCDATASection *self, unsigned long offset, GdomeException *exc);
GdomeDOMString *gdome_cds_data (GdomeCDATASection *self, GdomeException *exc);
void gdome_cds_set_data (GdomeCDATASection *self, GdomeDOMString *data, GdomeException *exc);
unsigned long gdome_cds_length (GdomeCDATASection *self, GdomeException *exc);
GdomeDOMString *gdome_cds_substringData (GdomeCDATASection *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_cds_appendData (GdomeCDATASection *self, GdomeDOMString *arg, GdomeException *exc);
void gdome_cds_insertData (GdomeCDATASection *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc);
void gdome_cds_deleteData (GdomeCDATASection *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_cds_replaceData (GdomeCDATASection *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc);
void gdome_cds_ref (GdomeCDATASection *self, GdomeException *exc);
void gdome_cds_unref (GdomeCDATASection *self, GdomeException *exc);
void * gdome_cds_query_interface (GdomeCDATASection *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_cds_nodeName (GdomeCDATASection *self, GdomeException *exc);
GdomeDOMString *gdome_cds_nodeValue (GdomeCDATASection *self, GdomeException *exc);
void gdome_cds_set_nodeValue (GdomeCDATASection *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_cds_nodeType (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_parentNode (GdomeCDATASection *self, GdomeException *exc);
GdomeNodeList *gdome_cds_childNodes (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_firstChild (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_lastChild (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_previousSibling (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_nextSibling (GdomeCDATASection *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_cds_attributes (GdomeCDATASection *self, GdomeException *exc);
GdomeDocument *gdome_cds_ownerDocument (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_insertBefore (GdomeCDATASection *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_cds_replaceChild (GdomeCDATASection *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_cds_removeChild (GdomeCDATASection *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_cds_appendChild (GdomeCDATASection *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_cds_hasChildNodes (GdomeCDATASection *self, GdomeException *exc);
GdomeNode *gdome_cds_cloneNode (GdomeCDATASection *self, GdomeBoolean deep, GdomeException *exc);
void gdome_cds_normalize (GdomeCDATASection *self, GdomeException *exc);
GdomeBoolean gdome_cds_supports (GdomeCDATASection *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_cds_namespaceURI (GdomeCDATASection *self, GdomeException *exc);
GdomeDOMString *gdome_cds_prefix (GdomeCDATASection *self, GdomeException *exc);
void gdome_cds_set_prefix (GdomeCDATASection *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_cds_localName (GdomeCDATASection *self, GdomeException *exc);
void gdome_cds_addEventListener (GdomeCDATASection *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_cds_removeEventListener (GdomeCDATASection *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_cds_dispatchEvent (GdomeCDATASection *self, GdomeEvent *evt, GdomeException *exc);
GdomeDOMString *gdome_c_data (GdomeComment *self, GdomeException *exc);
void gdome_c_set_data (GdomeComment *self, GdomeDOMString *data, GdomeException *exc);
unsigned long gdome_c_length (GdomeComment *self, GdomeException *exc);
GdomeDOMString *gdome_c_substringData (GdomeComment *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_c_appendData (GdomeComment *self, GdomeDOMString *arg, GdomeException *exc);
void gdome_c_insertData (GdomeComment *self, unsigned long offset, GdomeDOMString *arg, GdomeException *exc);
void gdome_c_deleteData (GdomeComment *self, unsigned long offset, unsigned long count, GdomeException *exc);
void gdome_c_replaceData (GdomeComment *self, unsigned long offset, unsigned long count, GdomeDOMString *arg, GdomeException *exc);
void gdome_c_ref (GdomeComment *self, GdomeException *exc);
void gdome_c_unref (GdomeComment *self, GdomeException *exc);
void * gdome_c_query_interface (GdomeComment *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_c_nodeName (GdomeComment *self, GdomeException *exc);
GdomeDOMString *gdome_c_nodeValue (GdomeComment *self, GdomeException *exc);
void gdome_c_set_nodeValue (GdomeComment *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_c_nodeType (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_parentNode (GdomeComment *self, GdomeException *exc);
GdomeNodeList *gdome_c_childNodes (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_firstChild (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_lastChild (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_previousSibling (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_nextSibling (GdomeComment *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_c_attributes (GdomeComment *self, GdomeException *exc);
GdomeDocument *gdome_c_ownerDocument (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_insertBefore (GdomeComment *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_c_replaceChild (GdomeComment *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_c_removeChild (GdomeComment *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_c_appendChild (GdomeComment *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_c_hasChildNodes (GdomeComment *self, GdomeException *exc);
GdomeNode *gdome_c_cloneNode (GdomeComment *self, GdomeBoolean deep, GdomeException *exc);
void gdome_c_normalize (GdomeComment *self, GdomeException *exc);
GdomeBoolean gdome_c_supports (GdomeComment *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_c_namespaceURI (GdomeComment *self, GdomeException *exc);
GdomeDOMString *gdome_c_prefix (GdomeComment *self, GdomeException *exc);
void gdome_c_set_prefix (GdomeComment *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_c_localName (GdomeComment *self, GdomeException *exc);
void gdome_c_addEventListener (GdomeComment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_c_removeEventListener (GdomeComment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_c_dispatchEvent (GdomeComment *self, GdomeEvent *evt, GdomeException *exc);
void gdome_DOMImplementation_ref (GdomeDOMImplementation *self, GdomeException *exc);
void gdome_DOMImplementation_unref (GdomeDOMImplementation *self, GdomeException *exc);
void * gdome_DOMImplementation_query_interface (GdomeDOMImplementation *self, const char *interface, GdomeException *exc);
GdomeBoolean gdome_DOMImplementation_hasFeature (GdomeDOMImplementation *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDocumentType *gdome_DOMImplementation_createDocumentType (GdomeDOMImplementation *self, GdomeDOMString *qualifiedName, GdomeDOMString *publicId, GdomeDOMString *systemId, GdomeDOMString *internalSubset, GdomeException *exc);
GdomeDocument *gdome_DOMImplementation_createDocument (GdomeDOMImplementation *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeDocumentType *doctype, GdomeException *exc);

GdomeDocumentType *gdome_doc_doctype (GdomeDocument *self, GdomeException *exc);
GdomeDOMImplementation *gdome_doc_implementation (GdomeDocument *self, GdomeException *exc);
GdomeElement *gdome_doc_documentElement (GdomeDocument *self, GdomeException *exc);
GdomeElement *gdome_doc_createElement (GdomeDocument *self, GdomeDOMString *tagName, GdomeException *exc);
GdomeDocumentFragment *gdome_doc_createDocumentFragment (GdomeDocument *self, GdomeException *exc);
GdomeText *gdome_doc_createTextNode (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc);
GdomeComment *gdome_doc_createComment (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc);
GdomeCDATASection *gdome_doc_createCDATASection (GdomeDocument *self, GdomeDOMString *data, GdomeException *exc);
GdomeProcessingInstruction *gdome_doc_createProcessingInstruction (GdomeDocument *self, GdomeDOMString *target, GdomeDOMString *data, GdomeException *exc);
GdomeAttr *gdome_doc_createAttribute (GdomeDocument *self, GdomeDOMString *name, GdomeException *exc);
GdomeEntityReference *gdome_doc_createEntityReference (GdomeDocument *self, GdomeDOMString *name, GdomeException *exc);
GdomeNodeList *gdome_doc_getElementsByTagName (GdomeDocument *self, GdomeDOMString *tagname, GdomeException *exc);
GdomeNode *gdome_doc_importNode (GdomeDocument *self, GdomeNode *importedNode, GdomeBoolean deep, GdomeException *exc);
GdomeElement *gdome_doc_createElementNS (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc);
GdomeAttr *gdome_doc_createAttrNS (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc);
GdomeNodeList *gdome_doc_getElementsByTagNameNS (GdomeDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc);
GdomeElement *gdome_doc_getElementById (GdomeDocument *self, GdomeDOMString *elementId, GdomeException *exc);
void gdome_doc_ref (GdomeDocument *self, GdomeException *exc);
void gdome_doc_unref (GdomeDocument *self, GdomeException *exc);
void * gdome_doc_query_interface (GdomeDocument *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_doc_nodeName (GdomeDocument *self, GdomeException *exc);
GdomeDOMString *gdome_doc_nodeValue (GdomeDocument *self, GdomeException *exc);
void gdome_doc_set_nodeValue (GdomeDocument *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_doc_nodeType (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_parentNode (GdomeDocument *self, GdomeException *exc);
GdomeNodeList *gdome_doc_childNodes (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_firstChild (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_lastChild (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_previousSibling (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_nextSibling (GdomeDocument *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_doc_attributes (GdomeDocument *self, GdomeException *exc);
GdomeDocument *gdome_doc_ownerDocument (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_insertBefore (GdomeDocument *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_doc_replaceChild (GdomeDocument *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_doc_removeChild (GdomeDocument *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_doc_appendChild (GdomeDocument *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_doc_hasChildNodes (GdomeDocument *self, GdomeException *exc);
GdomeNode *gdome_doc_cloneNode (GdomeDocument *self, GdomeBoolean deep, GdomeException *exc);
void gdome_doc_normalize (GdomeDocument *self, GdomeException *exc);
GdomeBoolean gdome_doc_supports (GdomeDocument *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_doc_namespaceURI (GdomeDocument *self, GdomeException *exc);
GdomeDOMString *gdome_doc_prefix (GdomeDocument *self, GdomeException *exc);
void gdome_doc_set_prefix (GdomeDocument *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_doc_localName (GdomeDocument *self, GdomeException *exc);
void gdome_doc_addEventListener (GdomeDocument *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_doc_removeEventListener (GdomeDocument *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_doc_dispatchEvent (GdomeDocument *self, GdomeEvent *evt, GdomeException *exc);

void gdome_df_ref (GdomeDocumentFragment *self, GdomeException *exc);
void gdome_df_unref (GdomeDocumentFragment *self, GdomeException *exc);
void * gdome_df_query_interface (GdomeDocumentFragment *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_df_nodeName (GdomeDocumentFragment *self, GdomeException *exc);
GdomeDOMString *gdome_df_nodeValue (GdomeDocumentFragment *self, GdomeException *exc);
void gdome_df_set_nodeValue (GdomeDocumentFragment *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_df_nodeType (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_parentNode (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNodeList *gdome_df_childNodes (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_firstChild (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_lastChild (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_previousSibling (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_nextSibling (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_df_attributes (GdomeDocumentFragment *self, GdomeException *exc);
GdomeDocument *gdome_df_ownerDocument (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_insertBefore (GdomeDocumentFragment *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_df_replaceChild (GdomeDocumentFragment *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_df_removeChild (GdomeDocumentFragment *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_df_appendChild (GdomeDocumentFragment *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_df_hasChildNodes (GdomeDocumentFragment *self, GdomeException *exc);
GdomeNode *gdome_df_cloneNode (GdomeDocumentFragment *self, GdomeBoolean deep, GdomeException *exc);
void gdome_df_normalize (GdomeDocumentFragment *self, GdomeException *exc);
GdomeBoolean gdome_df_supports (GdomeDocumentFragment *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_df_namespaceURI (GdomeDocumentFragment *self, GdomeException *exc);
GdomeDOMString *gdome_df_prefix (GdomeDocumentFragment *self, GdomeException *exc);
void gdome_df_set_prefix (GdomeDocumentFragment *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_df_localName (GdomeDocumentFragment *self, GdomeException *exc);
void gdome_df_addEventListener (GdomeDocumentFragment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_df_removeEventListener (GdomeDocumentFragment *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_df_dispatchEvent (GdomeDocumentFragment *self, GdomeEvent *evt, GdomeException *exc);
GdomeDOMString *gdome_dt_name (GdomeDocumentType *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_dt_entities (GdomeDocumentType *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_dt_notations (GdomeDocumentType *self, GdomeException *exc);
GdomeDOMString *gdome_dt_publicId (GdomeDocumentType *self, GdomeException *exc);
GdomeDOMString *gdome_dt_systemId (GdomeDocumentType *self, GdomeException *exc);
GdomeDOMString *gdome_dt_internalSubset (GdomeDocumentType *self, GdomeException *exc);
void gdome_dt_ref (GdomeDocumentType *self, GdomeException *exc);
void gdome_dt_unref (GdomeDocumentType *self, GdomeException *exc);
void * gdome_dt_query_interface (GdomeDocumentType *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_dt_nodeName (GdomeDocumentType *self, GdomeException *exc);
GdomeDOMString *gdome_dt_nodeValue (GdomeDocumentType *self, GdomeException *exc);
void gdome_dt_set_nodeValue (GdomeDocumentType *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_dt_nodeType (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_parentNode (GdomeDocumentType *self, GdomeException *exc);
GdomeNodeList *gdome_dt_childNodes (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_firstChild (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_lastChild (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_previousSibling (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_nextSibling (GdomeDocumentType *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_dt_attributes (GdomeDocumentType *self, GdomeException *exc);
GdomeDocument *gdome_dt_ownerDocument (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_insertBefore (GdomeDocumentType *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_dt_replaceChild (GdomeDocumentType *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_dt_removeChild (GdomeDocumentType *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_dt_appendChild (GdomeDocumentType *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_dt_hasChildNodes (GdomeDocumentType *self, GdomeException *exc);
GdomeNode *gdome_dt_cloneNode (GdomeDocumentType *self, GdomeBoolean deep, GdomeException *exc);
void gdome_dt_normalize (GdomeDocumentType *self, GdomeException *exc);
GdomeBoolean gdome_dt_supports (GdomeDocumentType *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_dt_namespaceURI (GdomeDocumentType *self, GdomeException *exc);
GdomeDOMString *gdome_dt_prefix (GdomeDocumentType *self, GdomeException *exc);
void gdome_dt_set_prefix (GdomeDocumentType *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_dt_localName (GdomeDocumentType *self, GdomeException *exc);
void gdome_dt_addEventListener (GdomeDocumentType *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_dt_removeEventListener (GdomeDocumentType *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_dt_dispatchEvent (GdomeDocumentType *self, GdomeEvent *evt, GdomeException *exc);

GdomeDOMString *gdome_el_tagName (GdomeElement *self, GdomeException *exc);
GdomeDOMString *gdome_el_getAttribute (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
void gdome_el_setAttribute (GdomeElement *self, GdomeDOMString *name, GdomeDOMString *value, GdomeException *exc);
void gdome_el_removeAttribute (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
GdomeAttr *gdome_el_getAttributeNode (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
GdomeAttr *gdome_el_setAttributeNode (GdomeElement *self, GdomeAttr *newAttr, GdomeException *exc);
GdomeAttr *gdome_el_removeAttributeNode (GdomeElement *self, GdomeAttr *oldAttr, GdomeException *exc);
GdomeNodeList *gdome_el_getElementsByTagName (GdomeElement *self, GdomeDOMString *name, GdomeException *exc);
GdomeDOMString *gdome_el_getAttributeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
void gdome_el_setAttributeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeDOMString *value, GdomeException *exc);
void gdome_el_removeAttributeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
GdomeAttr *gdome_el_getAttributeNodeNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
GdomeAttr *gdome_el_setAttributeNodeNS (GdomeElement *self, GdomeAttr *newAttr, GdomeException *exc);
GdomeNodeList *gdome_el_getElementsByTagNameNS (GdomeElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
void gdome_el_ref (GdomeElement *self, GdomeException *exc);
void gdome_el_unref (GdomeElement *self, GdomeException *exc);
void * gdome_el_query_interface (GdomeElement *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_el_nodeName (GdomeElement *self, GdomeException *exc);
GdomeDOMString *gdome_el_nodeValue (GdomeElement *self, GdomeException *exc);
void gdome_el_set_nodeValue (GdomeElement *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_el_nodeType (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_parentNode (GdomeElement *self, GdomeException *exc);
GdomeNodeList *gdome_el_childNodes (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_firstChild (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_lastChild (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_previousSibling (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_nextSibling (GdomeElement *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_el_attributes (GdomeElement *self, GdomeException *exc);
GdomeDocument *gdome_el_ownerDocument (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_insertBefore (GdomeElement *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_el_replaceChild (GdomeElement *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_el_removeChild (GdomeElement *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_el_appendChild (GdomeElement *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_el_hasChildNodes (GdomeElement *self, GdomeException *exc);
GdomeNode *gdome_el_cloneNode (GdomeElement *self, GdomeBoolean deep, GdomeException *exc);
void gdome_el_normalize (GdomeElement *self, GdomeException *exc);
GdomeBoolean gdome_el_supports (GdomeElement *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_el_namespaceURI (GdomeElement *self, GdomeException *exc);
GdomeDOMString *gdome_el_prefix (GdomeElement *self, GdomeException *exc);
void gdome_el_set_prefix (GdomeElement *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_el_localName (GdomeElement *self, GdomeException *exc);
void gdome_el_addEventListener (GdomeElement *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_el_removeEventListener (GdomeElement *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_el_dispatchEvent (GdomeElement *self, GdomeEvent *evt, GdomeException *exc);

GdomeDOMString *gdome_ent_publicId (GdomeEntity *self, GdomeException *exc);
GdomeDOMString *gdome_ent_systemId (GdomeEntity *self, GdomeException *exc);
GdomeDOMString *gdome_ent_notationName (GdomeEntity *self, GdomeException *exc);
void gdome_ent_ref (GdomeEntity *self, GdomeException *exc);
void gdome_ent_unref (GdomeEntity *self, GdomeException *exc);
void * gdome_ent_query_interface (GdomeEntity *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_ent_nodeName (GdomeEntity *self, GdomeException *exc);
GdomeDOMString *gdome_ent_nodeValue (GdomeEntity *self, GdomeException *exc);
void gdome_ent_set_nodeValue (GdomeEntity *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_ent_nodeType (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_parentNode (GdomeEntity *self, GdomeException *exc);
GdomeNodeList *gdome_ent_childNodes (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_firstChild (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_lastChild (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_previousSibling (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_nextSibling (GdomeEntity *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_ent_attributes (GdomeEntity *self, GdomeException *exc);
GdomeDocument *gdome_ent_ownerDocument (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_insertBefore (GdomeEntity *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_ent_replaceChild (GdomeEntity *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_ent_removeChild (GdomeEntity *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_ent_appendChild (GdomeEntity *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_ent_hasChildNodes (GdomeEntity *self, GdomeException *exc);
GdomeNode *gdome_ent_cloneNode (GdomeEntity *self, GdomeBoolean deep, GdomeException *exc);
void gdome_ent_normalize (GdomeEntity *self, GdomeException *exc);
GdomeBoolean gdome_ent_supports (GdomeEntity *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_ent_namespaceURI (GdomeEntity *self, GdomeException *exc);
GdomeDOMString *gdome_ent_prefix (GdomeEntity *self, GdomeException *exc);
void gdome_ent_set_prefix (GdomeEntity *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_ent_localName (GdomeEntity *self, GdomeException *exc);
void gdome_ent_addEventListener (GdomeEntity *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_ent_removeEventListener (GdomeEntity *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_ent_dispatchEvent (GdomeEntity *self, GdomeEvent *evt, GdomeException *exc);

void gdome_er_ref (GdomeEntityReference *self, GdomeException *exc);
void gdome_er_unref (GdomeEntityReference *self, GdomeException *exc);
void * gdome_er_query_interface (GdomeEntityReference *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_er_nodeName (GdomeEntityReference *self, GdomeException *exc);
GdomeDOMString *gdome_er_nodeValue (GdomeEntityReference *self, GdomeException *exc);
void gdome_er_set_nodeValue (GdomeEntityReference *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_er_nodeType (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_parentNode (GdomeEntityReference *self, GdomeException *exc);
GdomeNodeList *gdome_er_childNodes (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_firstChild (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_lastChild (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_previousSibling (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_nextSibling (GdomeEntityReference *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_er_attributes (GdomeEntityReference *self, GdomeException *exc);
GdomeDocument *gdome_er_ownerDocument (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_insertBefore (GdomeEntityReference *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_er_replaceChild (GdomeEntityReference *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_er_removeChild (GdomeEntityReference *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_er_appendChild (GdomeEntityReference *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_er_hasChildNodes (GdomeEntityReference *self, GdomeException *exc);
GdomeNode *gdome_er_cloneNode (GdomeEntityReference *self, GdomeBoolean deep, GdomeException *exc);
void gdome_er_normalize (GdomeEntityReference *self, GdomeException *exc);
GdomeBoolean gdome_er_supports (GdomeEntityReference *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_er_namespaceURI (GdomeEntityReference *self, GdomeException *exc);
GdomeDOMString *gdome_er_prefix (GdomeEntityReference *self, GdomeException *exc);
void gdome_er_set_prefix (GdomeEntityReference *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_er_localName (GdomeEntityReference *self, GdomeException *exc);
void gdome_er_addEventListener (GdomeEntityReference *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_er_removeEventListener (GdomeEntityReference *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_er_dispatchEvent (GdomeEntityReference *self, GdomeEvent *evt, GdomeException *exc);
void gdome_evnt_ref (GdomeEvent *self, GdomeException *exc);
void gdome_evnt_unref (GdomeEvent *self, GdomeException *exc);
void * gdome_evnt_query_interface (GdomeEvent *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_evnt_type (GdomeEvent *self, GdomeException *exc);
GdomeNode *gdome_evnt_target (GdomeEvent *self, GdomeException *exc);
GdomeNode *gdome_evnt_currentNode (GdomeEvent *self, GdomeException *exc);
unsigned short gdome_evnt_eventPhase (GdomeEvent *self, GdomeException *exc);
GdomeBoolean gdome_evnt_bubbles (GdomeEvent *self, GdomeException *exc);
GdomeBoolean gdome_evnt_cancelable (GdomeEvent *self, GdomeException *exc);
void gdome_evnt_stopPropagation (GdomeEvent *self, GdomeException *exc);
void gdome_evnt_preventDefault (GdomeEvent *self, GdomeException *exc);
void gdome_evnt_initEvent (GdomeEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc);

void gdome_evntl_ref (GdomeEventListener *self, GdomeException *exc);
void gdome_evntl_unref (GdomeEventListener *self, GdomeException *exc);
void * gdome_evntl_query_interface (GdomeEventListener *self, const char *interface, GdomeException *exc);
void gdome_evntl_handleEvent (GdomeEventListener *self, GdomeEvent *event, GdomeException *exc);

GdomeNode *gdome_mevnt_relatedNode (GdomeMutationEvent *self, GdomeException *exc);
GdomeDOMString *gdome_mevnt_prevValue (GdomeMutationEvent *self, GdomeException *exc);
GdomeDOMString *gdome_mevnt_newValue (GdomeMutationEvent *self, GdomeException *exc);
GdomeDOMString *gdome_mevnt_attrName (GdomeMutationEvent *self, GdomeException *exc);
void gdome_mevnt_initMutationEvent (GdomeMutationEvent *self, GdomeDOMString *typeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeNode *relatedNodeArg, GdomeDOMString *prevValueArg, GdomeDOMString *newValueArg, GdomeDOMString *attrNameArg, GdomeException *exc);
void gdome_mevnt_ref (GdomeMutationEvent *self, GdomeException *exc);
void gdome_mevnt_unref (GdomeMutationEvent *self, GdomeException *exc);
void * gdome_mevnt_query_interface (GdomeMutationEvent *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_mevnt_type (GdomeMutationEvent *self, GdomeException *exc);
GdomeNode *gdome_mevnt_target (GdomeMutationEvent *self, GdomeException *exc);
GdomeNode *gdome_mevnt_currentNode (GdomeMutationEvent *self, GdomeException *exc);
unsigned short gdome_mevnt_eventPhase (GdomeMutationEvent *self, GdomeException *exc);
GdomeBoolean gdome_mevnt_bubbles (GdomeMutationEvent *self, GdomeException *exc);
GdomeBoolean gdome_mevnt_cancelable (GdomeMutationEvent *self, GdomeException *exc);
void gdome_mevnt_stopPropagation (GdomeMutationEvent *self, GdomeException *exc);
void gdome_mevnt_preventDefault (GdomeMutationEvent *self, GdomeException *exc);
void gdome_mevnt_initEvent (GdomeMutationEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc);

void gdome_nnm_ref (GdomeNamedNodeMap *self, GdomeException *exc);
void gdome_nnm_unref (GdomeNamedNodeMap *self, GdomeException *exc);
void * gdome_nnm_query_interface (GdomeNamedNodeMap *self, const char *interface, GdomeException *exc);
GdomeNode *gdome_nnm_getNamedItem (GdomeNamedNodeMap *self, GdomeDOMString *name, GdomeException *exc);
GdomeNode *gdome_nnm_setNamedItem (GdomeNamedNodeMap *self, GdomeNode *arg, GdomeException *exc);
GdomeNode *gdome_nnm_removeNamedItem (GdomeNamedNodeMap *self, GdomeDOMString *name, GdomeException *exc);
GdomeNode *gdome_nnm_item (GdomeNamedNodeMap *self, unsigned long index, GdomeException *exc);
unsigned long gdome_nnm_length (GdomeNamedNodeMap *self, GdomeException *exc);

void gdome_nl_ref (GdomeNodeList *self, GdomeException *exc);
void gdome_nl_unref (GdomeNodeList *self, GdomeException *exc);
void * gdome_nl_query_interface (GdomeNodeList *self, const char *interface, GdomeException *exc);
GdomeNode *gdome_nl_item (GdomeNodeList *self, unsigned long index, GdomeException *exc);
unsigned long gdome_nl_length (GdomeNodeList *self, GdomeException *exc);

GdomeDOMString *gdome_not_publicId (GdomeNotation *self, GdomeException *exc);
GdomeDOMString *gdome_not_systemId (GdomeNotation *self, GdomeException *exc);
void gdome_not_ref (GdomeNotation *self, GdomeException *exc);
void gdome_not_unref (GdomeNotation *self, GdomeException *exc);
void * gdome_not_query_interface (GdomeNotation *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_not_nodeName (GdomeNotation *self, GdomeException *exc);
GdomeDOMString *gdome_not_nodeValue (GdomeNotation *self, GdomeException *exc);
void gdome_not_set_nodeValue (GdomeNotation *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_not_nodeType (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_parentNode (GdomeNotation *self, GdomeException *exc);
GdomeNodeList *gdome_not_childNodes (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_firstChild (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_lastChild (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_previousSibling (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_nextSibling (GdomeNotation *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_not_attributes (GdomeNotation *self, GdomeException *exc);
GdomeDocument *gdome_not_ownerDocument (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_insertBefore (GdomeNotation *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_not_replaceChild (GdomeNotation *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_not_removeChild (GdomeNotation *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_not_appendChild (GdomeNotation *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_not_hasChildNodes (GdomeNotation *self, GdomeException *exc);
GdomeNode *gdome_not_cloneNode (GdomeNotation *self, GdomeBoolean deep, GdomeException *exc);
void gdome_not_normalize (GdomeNotation *self, GdomeException *exc);
GdomeBoolean gdome_not_supports (GdomeNotation *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_not_namespaceURI (GdomeNotation *self, GdomeException *exc);
GdomeDOMString *gdome_not_prefix (GdomeNotation *self, GdomeException *exc);
void gdome_not_set_prefix (GdomeNotation *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_not_localName (GdomeNotation *self, GdomeException *exc);
void gdome_not_addEventListener (GdomeNotation *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_not_removeEventListener (GdomeNotation *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_not_dispatchEvent (GdomeNotation *self, GdomeEvent *evt, GdomeException *exc);

GdomeDOMString *gdome_pi_target (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeDOMString *gdome_pi_data (GdomeProcessingInstruction *self, GdomeException *exc);
void gdome_pi_set_data (GdomeProcessingInstruction *self, GdomeDOMString *data, GdomeException *exc);
void gdome_pi_ref (GdomeProcessingInstruction *self, GdomeException *exc);
void gdome_pi_unref (GdomeProcessingInstruction *self, GdomeException *exc);
void * gdome_pi_query_interface (GdomeProcessingInstruction *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_pi_nodeName (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeDOMString *gdome_pi_nodeValue (GdomeProcessingInstruction *self, GdomeException *exc);
void gdome_pi_set_nodeValue (GdomeProcessingInstruction *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_pi_nodeType (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_parentNode (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNodeList *gdome_pi_childNodes (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_firstChild (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_lastChild (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_previousSibling (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_nextSibling (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_pi_attributes (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeDocument *gdome_pi_ownerDocument (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_insertBefore (GdomeProcessingInstruction *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_pi_replaceChild (GdomeProcessingInstruction *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_pi_removeChild (GdomeProcessingInstruction *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_pi_appendChild (GdomeProcessingInstruction *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_pi_hasChildNodes (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeNode *gdome_pi_cloneNode (GdomeProcessingInstruction *self, GdomeBoolean deep, GdomeException *exc);
void gdome_pi_normalize (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeBoolean gdome_pi_supports (GdomeProcessingInstruction *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_pi_namespaceURI (GdomeProcessingInstruction *self, GdomeException *exc);
GdomeDOMString *gdome_pi_prefix (GdomeProcessingInstruction *self, GdomeException *exc);
void gdome_pi_set_prefix (GdomeProcessingInstruction *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_pi_localName (GdomeProcessingInstruction *self, GdomeException *exc);
void gdome_pi_addEventListener (GdomeProcessingInstruction *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_pi_removeEventListener (GdomeProcessingInstruction *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_pi_dispatchEvent (GdomeProcessingInstruction *self, GdomeEvent *evt, GdomeException *exc);

long gdome_uievnt_screenX (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_screenX (GdomeUIEvent *self, long screenX, GdomeException *exc);
long gdome_uievnt_screenY (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_screenY (GdomeUIEvent *self, long screenY, GdomeException *exc);
long gdome_uievnt_clientX (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_clientX (GdomeUIEvent *self, long clientX, GdomeException *exc);
long gdome_uievnt_clientY (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_clientY (GdomeUIEvent *self, long clientY, GdomeException *exc);
GdomeBoolean gdome_uievnt_altKey (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_altKey (GdomeUIEvent *self, GdomeBoolean altKey, GdomeException *exc);
GdomeBoolean gdome_uievnt_ctrlKey (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_ctrlKey (GdomeUIEvent *self, GdomeBoolean ctrlKey, GdomeException *exc);
GdomeBoolean gdome_uievnt_shiftKey (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_shiftKey (GdomeUIEvent *self, GdomeBoolean shiftKey, GdomeException *exc);
unsigned long gdome_uievnt_keyCode (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_keyCode (GdomeUIEvent *self, unsigned long keyCode, GdomeException *exc);
unsigned long gdome_uievnt_charCode (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_charCode (GdomeUIEvent *self, unsigned long charCode, GdomeException *exc);
unsigned short gdome_uievnt_button (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_set_button (GdomeUIEvent *self, unsigned short button, GdomeException *exc);
void gdome_uievnt_ref (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_unref (GdomeUIEvent *self, GdomeException *exc);
void * gdome_uievnt_query_interface (GdomeUIEvent *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_uievnt_type (GdomeUIEvent *self, GdomeException *exc);
GdomeNode *gdome_uievnt_target (GdomeUIEvent *self, GdomeException *exc);
GdomeNode *gdome_uievnt_currentNode (GdomeUIEvent *self, GdomeException *exc);
unsigned short gdome_uievnt_eventPhase (GdomeUIEvent *self, GdomeException *exc);
GdomeBoolean gdome_uievnt_bubbles (GdomeUIEvent *self, GdomeException *exc);
GdomeBoolean gdome_uievnt_cancelable (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_stopPropagation (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_preventDefault (GdomeUIEvent *self, GdomeException *exc);
void gdome_uievnt_initEvent (GdomeUIEvent *self, GdomeDOMString *eventTypeArg, GdomeBoolean canBubbleArg, GdomeBoolean cancelableArg, GdomeException *exc);

#endif /* GDOME_H */
/* This module provides for recognition and translation of character
   encodings.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so it can be used well nigh anywhere.
*/

typedef struct _RXML_Encoding RXML_Encoding;

struct _RXML_Encoding {
  char *name;
  RXML_ByteWriter *(*transform) (RXML_Context *context, RXML_ByteWriter *out);
};

extern RXML_Encoding RXML_encodings[];
/* actually, there's no way to tell the size of this array. ah well */

RXML_ByteWriter * RXML_encoding_decode (RXML_Context *context,
					RXML_ByteWriter *out,
					const char *encoding);

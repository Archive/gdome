/* casting macros */

#ifndef GDOME_UTIL_H
#define GDOME_UTIL_H

#include "gdome.h"

#define GDOME_EVNTT(evntt) (GdomeEventTarget *) evntt
#define GDOME_EVNTL(evntl) (GdomeEventListener *) evntl
#define GDOME_N(node) (GdomeNode *) node
#define GDOME_EL(el) (GdomeElement *) el
#define GDOME_A(attr) (GdomeAttr *) attr
#define GDOME_T(text) (GdomeText *) text
#define GDOME_NOT(not) (GdomeNot *) not
#define GDOME_ENT(ent) (GdomeEntity *) ent
#define GDOME_ER(er) (GdomeEntityReference *) er
#define GDOME_CDS(cds) (GdomeCDATASection *) cds
#define GDOME_DOC(doc) (GdomeDocument *) doc
#define GDOME_PI(pi) (GdomeProcessingInstruction *) pi
#define GDOME_CD(cd) (GdomeCharacterData *) cd

#define GDOME_EVNTL_CALLBACK(cb) cb

/* todo: make this autogened in gdome.h */
/*
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
#define GDOME_() (Gdome *)
*/
   

typedef GdomeNode GdomeEventTarget;


/* wrapper functions around gdome_n_*EventListener functions */
void gdome_evntt_ref (GdomeEventTarget *self, GdomeException *exc);

void gdome_evntt_unref (GdomeEventTarget *self, GdomeException *exc);

void * gdome_evntt_query_interface (GdomeEventTarget *self, const char *interface, GdomeException *exc);

void gdome_evntt_addEventListener (GdomeEventTarget *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);

void gdome_evntt_removeEventListener (GdomeEventTarget *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);

GdomeBoolean gdome_evntt_dispatchEvent (GdomeEventTarget *self, GdomeEvent *evt, GdomeException *exc);


#endif /* GDOME_UTIL_H */

/* This is a module to create an RDOM Document given a stream of XML bits.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

#include <stdlib.h>
#include "rxml.h"
#include "rdom.h"

typedef struct _RXML_XbitWriterRdom RXML_XbitWriterRdom;

struct _RXML_XbitWriterRdom {
  RXML_XbitWriter writer;

  RXML_Context *context;
  RDOM_Document document;
  /* maybe should maintain stack, as popping the stack might be
     quicker than traversing the parent link */
  RDOM_Node cursor;
};

static RXML_Error *
RXML_rdom_from_xbit_start_tag (RXML_XbitWriter *self, RXML_Tag *tag)
{
  RXML_XbitWriterRdom *xwr = (RXML_XbitWriterRdom *)self;
  RDOM_Element el;
  int i;

  RDOM_createElement (&xwr->document, tag->name, &el);
  for (i = 0; i < tag->n_attrs; i++)
    RDOM_setAttribute (&el, tag->attrs[i].name, tag->attrs[i].value);
  RDOM_appendChild (&xwr->cursor, &el);
  xwr->cursor = el; /* can we just assign the node? */
  return NULL;
}

static RXML_Error *
RXML_rdom_from_xbit_end_tag (RXML_XbitWriter *self, RXML_Tag *tag)
{
  RXML_XbitWriterRdom *xwr = (RXML_XbitWriterRdom *)self;

  RDOM_parentNode (&xwr->cursor, &xwr->cursor);
  return NULL;
}

static RXML_Error *
RXML_rdom_from_xbit_text (RXML_XbitWriter *self, char *buf)
{
  RXML_XbitWriterRdom *xwr = (RXML_XbitWriterRdom *)self;
  RDOM_Element el;

  RDOM_createTextNode (&xwr->document, buf, &el);
  RDOM_appendChild (&xwr->cursor, &el);
  return NULL;
}

static RXML_Error *
RXML_rdom_from_xbit_close (RXML_XbitWriter *self)
{
  return NULL;
}

RXML_XbitWriter *
RDOM_RXML_create_document (RXML_Context *context, RDOM_Document *result)
{
  RXML_XbitWriterRdom *xw;

  RDOM_create (context, result);
  xw = RXML_new (context, RXML_XbitWriterRdom, 1);

  xw->writer.start_tag = RXML_rdom_from_xbit_start_tag;
  xw->writer.end_tag = RXML_rdom_from_xbit_end_tag;
  xw->writer.text = RXML_rdom_from_xbit_text;
  xw->writer.close = RXML_rdom_from_xbit_close;

  xw->context = context;
  xw->document = *result;
  xw->cursor = *result;

  return (RXML_XbitWriter *)xw;
}

/* Raph's DOM.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

/* This is the public interface for the DOM. It should be possible
   to implement it in a number of different ways. The implementation
   in this module is simple but fairly memory-hungry. */

typedef struct _RDOM RDOM;
typedef struct _RDOM_Node RDOM_Node;
typedef struct _RDOM_Node RDOM_Element;
typedef struct _RDOM_Node RDOM_Attr;
typedef struct _RDOM_Node RDOM_CharData;
typedef struct _RDOM_Node RDOM_Text;
typedef struct _RDOM_Node RDOM_Document;
typedef struct _RDOM_Class RDOM_Class;
typedef struct _RDOM_Listener RDOM_Listener;

typedef struct _RDOM_NodeList RDOM_NodeList;

typedef struct _RDOM_NamedNodeMap RDOM_NamedNodeMap;

struct _RDOM_Node {
  RDOM *dom;
  union {
    int id;
    void *p;
  } node;
};

struct _RDOM_NodeList {
  RDOM *dom;
  union {
    int id;
    void *p;
  } nl;
};

struct _RDOM_NamedNodeMap {
  RDOM *dom;
  union {
    int id;
    void *p;
  } nnm;
};

/* should this be a reference or inclusion? It seems to me like there
   are going to be fairly few RDOM's, probably no more than one per
   document. By using a reference, we have one more indirection.

   Using the convenience functions insulates the client from this
   choice.
*/
struct _RDOM {
  RDOM_Class *klass;
};

typedef enum {
  RDOM_ELEMENT_NODE = 1,
  RDOM_ATTRIBUTE_NODE = 2,
  RDOM_TEXT_NODE = 3,

  RDOM_DOCUMENT_NODE = 9
} RDOM_Type;

struct _RDOM_Class {
  /* Node */

  RDOM_Type (*get_type) (RDOM_Node *node);

  /* traversal methods - these just return the result pointer,
     or NULL if no such node exists */
  RDOM_Node *(*firstChild) (RDOM_Node *node, RDOM_Node *result);
  RDOM_Node *(*lastChild) (RDOM_Node *node, RDOM_Node *result);
  RDOM_Node *(*parentNode) (RDOM_Node *node, RDOM_Node *result);
  RDOM_Node *(*nextSibling) (RDOM_Node *node, RDOM_Node *result);
  RDOM_Node *(*previousSibling) (RDOM_Node *node, RDOM_Node *result);

  RDOM_NamedNodeMap *(*get_attributes) (RDOM_Node *node, RDOM_NamedNodeMap *result);

  /* mutation methods */
  void (*appendChild) (RDOM_Node *node, RDOM_Node *child);

  /* listeners (an extension to the level 1 DOM) */
  void (*add_listener) (RDOM_Node *node, RDOM_Listener *listener);
  void (*remove_listener) (RDOM_Node *node, RDOM_Listener *listener);

  /* Document */

  RDOM_Element *(*createElement) (RDOM_Document *document,
				  const char *tagName,
				  RDOM_Element *result);

  RDOM_Text *(*createTextNode) (RDOM_Document *document,
				const char *data,
				RDOM_Text *result);

  /* Element */

  /* string returned is only guaranteed to be valid as long as it
     has not been destroyed in the document. it must be released
     by a ::release_string invocation */
  char *(*get_tagName) (RDOM_Element *el);
  char *(*getAttribute) (RDOM_Element *el, const char *name);
  void (*setAttribute) (RDOM_Element *el, const char *name, const char *value);

  /* Attr */
  char *(*attr_get_name) (RDOM_Attr *attr);
  char *(*attr_get_value) (RDOM_Attr *attr);

  /* NamedNodeMap */

  int (*nnm_get_length) (RDOM_NamedNodeMap *nnm);
  RDOM_Node *(*getNamedItem) (RDOM_NamedNodeMap *nnm, const char *name, RDOM_Node *result);
  RDOM_Node *(*nnm_item) (RDOM_NamedNodeMap *nnm, int index, RDOM_Node *result);

  /* CharacterData */

  char *(*cd_get_data) (RDOM_CharData *cd);

  /* release functions for memory management */
  void (*release_string) (RDOM_Node *node, char *string);
};

typedef enum {
  RDOM_Before,
  RDOM_After
} RDOM_When;

struct _RDOM_Listener {
  void (*insert) (RDOM_Listener *self, RDOM_Node *node, RDOM_Node *child,
		  RDOM_When when);
  void (*delete) (RDOM_Listener *self, RDOM_Node *node, RDOM_When when);
};

/* Convenience functions for invoking DOM methods */

RDOM_Type RDOM_get_nodeType (RDOM_Node *node);
RDOM_Node *RDOM_firstChild (RDOM_Node *node, RDOM_Node *result);
RDOM_Node *RDOM_lastChild (RDOM_Node *node, RDOM_Node *result);
RDOM_Node *RDOM_parentNode (RDOM_Node *node, RDOM_Node *result);
RDOM_Node *RDOM_nextSibling (RDOM_Node *node, RDOM_Node *result);
RDOM_Node *RDOM_previousSibling (RDOM_Node *node, RDOM_Node *result);
RDOM_NamedNodeMap *RDOM_get_attributes (RDOM_Node *node, RDOM_NamedNodeMap *result);
void RDOM_appendChild (RDOM_Node *node, RDOM_Node *result);

RDOM_Element *RDOM_createElement (RDOM_Document *document,
				  const char *tagName,
				  RDOM_Element *result);
RDOM_Text *RDOM_createTextNode (RDOM_Document *document,
				const char *data,
				RDOM_Text *result);

char *RDOM_get_tagName (RDOM_Element *element);
char *RDOM_getAttribute (RDOM_Element *element, const char *name);
void RDOM_setAttribute (RDOM_Element *element, const char *name, const char *value);

char *RDOM_attr_get_name (RDOM_Attr *attr);
char *RDOM_attr_get_value (RDOM_Attr *attr);

int RDOM_nnm_get_length (RDOM_NamedNodeMap *nnm);
RDOM_Node *RDOM_getNamedItem (RDOM_NamedNodeMap *nnm, const char *name, RDOM_Node *result);
RDOM_Node *RDOM_nnm_item (RDOM_NamedNodeMap *nnm, int index, RDOM_Node *result);

char *RDOM_cd_get_data (RDOM_CharData *cd);

/* Listeners. These don't correspond directly to the DOM spec. */
void RDOM_add_listener (RDOM_Node *node, RDOM_Listener *listener);
void RDOM_remove_listener (RDOM_Node *node, RDOM_Listener *listener);

void RDOM_release_string (RDOM_Node *node, char *string);

/* Create a document, default implementation */
RDOM_Document *RDOM_create (RXML_Context *context, RDOM_Node *result);


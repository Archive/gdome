#include <tree.h>

#define GDOME_XML_IS_N(node) \
((Gdome_xml_Node *) node)->n->type == XML_ELEMENT_NODE       ||  \
((Gdome_xml_Node *) node)->n->type == XML_TEXT_NODE          ||  \
((Gdome_xml_Node *) node)->n->type == XML_CDATA_SECTION_NODE ||  \
((Gdome_xml_Node *) node)->n->type == XML_ENTITY_REF_NODE    ||  \
((Gdome_xml_Node *) node)->n->type == XML_ENTITY_NODE        ||  \
((Gdome_xml_Node *) node)->n->type == XML_PI_NODE            ||  \
((Gdome_xml_Node *) node)->n->type == XML_COMMENT_NODE       ||  \
((Gdome_xml_Node *) node)->n->type == XML_ATTRIBUTE_NODE     ||  \
((Gdome_xml_Node *) node)->n->type == XML_NOTATION_NODE      ||  \
((Gdome_xml_Node *) node)->n->type == XML_DOCUMENT_TYPE_NODE ||  \
((Gdome_xml_Node *) node)->n->type == XML_DOCUMENT_FRAG_NODE ||  \
((Gdome_xml_Node *) node)->n->type == XML_DOCUMENT_NODE

#define GDOME_XML_IS_EVNTL(evntl) 1

#define GDOME_XML_IS_CD(cd) 1

#define GDOME_XML_IS_T(cd) 1

#define GDOME_XML_IS_EL(el) 1

#define GDOME_XML_IS_ENT(ent) 1

#define GDOME_XML_IS_PI(pi) 1

#define GDOME_XML_IS_EVNTL(evntl) 1

#define GDOME_XML_IS_EVNT(evnt) 1

#define GDOME_XML_IS_MEVNT(mevnt) 1

/*#define GDOME_IS_EL(node) \*/

/* This is a module to create an RDOM Document given a stream of XML bits.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so that this code can be used well-nigh
   anywhere.

*/

RXML_XbitWriter *RDOM_RXML_create_document (RXML_Context *context,
					    RDOM_Document *result);

/* This module provides for recognition and translation of character
   encodings.

   Copyright 1999 Raph Levien <raph@acm.org>

   IJG-style license, so it can be used well nigh anywhere.
*/

#include <string.h>
#include <ctype.h>
#include "rxml.h"
#include "rxml-encodings.h"

RXML_ByteWriter *
RXML_encoding_decode_utf8 (RXML_Context *context, RXML_ByteWriter *out)
{
/* maybe we should do utf-8 checking here */
  return out;
}

typedef struct _RXML_ByteWriterIso8859_1 RXML_ByteWriterIso8859_1;

struct _RXML_ByteWriterIso8859_1 {
  RXML_ByteWriter writer;

  RXML_Context *context;
  RXML_ByteWriter *out;

  char *buf;
  int buf_size;
};

static RXML_Error *
RXML_encoding_decode_iso8859_1_write (RXML_ByteWriter *self, char *buf, int size)
{
  RXML_ByteWriterIso8859_1 *bw = (RXML_ByteWriterIso8859_1 *)self;
  int i, j;
  char *obuf;
  RXML_u32 c4;

  if (bw->buf_size < (size << 1))
    {
      if (bw->buf_size == 0)
	bw->buf_size = 256;
      else
	RXML_free (bw->context, bw->buf);
      do
	bw->buf_size <<= 1;
      while (bw->buf_size < (size << 1));
      bw->buf = RXML_new (bw->context, char, bw->buf_size);
    }

  obuf = bw->buf;

  /* fast processing of ASCII prefix */
  for (i = 0; i < size - 3; i += 4)
    {
      c4 = *((RXML_u32 *)(buf + i));
      if ((c4 & 0x80808080) != 0)
	break;
      *((RXML_u32 *)(obuf + i)) = c4;
    }

  j = i;

  for (; i < size; i++)
    {
      char c;

      c = buf[i];
      if (c & 0x80)
	{
	  obuf[j++] = 0xc0 | ((unsigned char)c) >> 6;
	  obuf[j++] = 0xbf & c;
	}
      else
	obuf[j++] = c;
    }

  bw->out->write (bw->out, obuf, j);
  /* todo: error handling */

  return NULL;
}

static RXML_Error *
RXML_encoding_decode_iso8859_1_close (RXML_ByteWriter *self)
{
  RXML_ByteWriterIso8859_1 *bw = (RXML_ByteWriterIso8859_1 *)self;

  return bw->out->close (bw->out);
  /* todo: deallocation */
}

static RXML_ByteWriter *
RXML_encoding_decode_iso8859_1 (RXML_Context *context, RXML_ByteWriter *out)
{
  RXML_ByteWriterIso8859_1 *bw;

  bw = RXML_new (context, RXML_ByteWriterIso8859_1, 1);

  bw->writer.write = RXML_encoding_decode_iso8859_1_write;
  bw->writer.close = RXML_encoding_decode_iso8859_1_close;

  bw->context = context;

  bw->out = out;
  bw->buf_size = 0;
  bw->buf = NULL;

  return (RXML_ByteWriter *)bw;
}

RXML_Encoding RXML_encodings[] = {
  { "utf-8", RXML_encoding_decode_utf8 },
  { "iso-8859-1", RXML_encoding_decode_iso8859_1 }
};

int
RXML_strcasecmp (const char *s1, const char *s2)
{
  int i;
  char c1, c2;

  for (i = 0; (c1 = s1[i]) != '\0'; i++)
    {
      c1 = tolower (c1);
      c2 = s2[i];
      if (c2 == '\0')
	return 1;
      c2 = tolower (c2);
      if (c1 != c2)
	{
	  if (c1 > c2)
	    return 1;
	  else /* c1 < c2 */
	    return -1;
	}
    }
  if (s2[i] == '\0')
    return 0;
  else
    return -1;
}

/* Return NULL if error in finding encoding */
RXML_ByteWriter *
RXML_encoding_decode (RXML_Context *context,
		      RXML_ByteWriter *out, const char *encoding)
{
  int i;

  if (encoding == NULL)
    return out; /* todo: autodetect */

  for (i = 0; i < sizeof(RXML_encodings) / sizeof(RXML_Encoding); i++)
    if (!RXML_strcasecmp (RXML_encodings[i].name, encoding))
      return (RXML_encodings[i].transform) (context, out);

  return NULL;
}
